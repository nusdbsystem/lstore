#!/bin/bash

sh run_for_distr.sh 0 0 100000
sh run_for_distr.sh 0.01 0.01 22000
sh run_for_distr.sh 0.05 0.05 13000
sh run_for_distr.sh 0.1 0.1 7000
sh run_for_distr.sh 0.2 0.2 4500
sh run_for_distr.sh 0.3 0.3 3600
sh run_for_distr.sh 0.5 0.5 2600

exit 0
