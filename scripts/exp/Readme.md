# Instructions for running the experiment

1. Confirm the following files are in the same level of a folder: 

  * jar
  * nodes configuration
  * script of launching the master
  * script of launching the slaves
  * script of killing the slaves
  * scritp of running for a given distributed data percentage
  * script of batch execution

2. Check the jar version in the following scripts:

  * script of launching the master
  * script of launching the slaves

3. Modify the node configuration. 

4. Start the master at the corresponding host. 

5. Run the script of batch execution. 

6. Collect the results from the master host. 
