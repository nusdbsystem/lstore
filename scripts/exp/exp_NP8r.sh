#!/bin/bash

sh run_for_distr.sh 0 0 100000
sh run_for_distr.sh 0.01 0.01 13000
sh run_for_distr.sh 0.05 0.05 10000
sh run_for_distr.sh 0.1 0.1 6200
sh run_for_distr.sh 0.2 0.2 3900
sh run_for_distr.sh 0.3 0.3 2850
sh run_for_distr.sh 0.5 0.5 1950

exit 0
