#!/bin/bash

sh run_for_distr.sh 0 0 100000
sh run_for_distr.sh 0.01 0.01 20000
sh run_for_distr.sh 0.05 0.05 12000
sh run_for_distr.sh 0.1 0.1 7200
sh run_for_distr.sh 0.2 0.2 4500
sh run_for_distr.sh 0.3 0.3 3100
sh run_for_distr.sh 0.5 0.5 2100

exit 0
