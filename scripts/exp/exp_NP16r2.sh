#!/bin/bash

sh run_for_distr.sh 0 0 21900
sh run_for_distr.sh 0.01 0.01 9300
sh run_for_distr.sh 0.05 0.05 7700
sh run_for_distr.sh 0.1 0.1 5400
sh run_for_distr.sh 0.2 0.2 3600
sh run_for_distr.sh 0.3 0.3 2650
sh run_for_distr.sh 0.5 0.5 1850

exit 0
