#!/bin/bash

./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0 -dp 0 -f 4900 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0 -dp 0 -f 6500 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0 -dp 0 -f 8150 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0 -dp 0 -f 10000 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0 -dp 0 -f 11900 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0 -dp 0 -f 13750 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0 -dp 0 -f 15650 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0 -dp 0 -f 18750 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0 -dp 0 -f 21900 -p 32 && sleep 52 && ./kill_slaves.sh 

./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.1 -dp 0.1 -f 600 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.1 -dp 0.1 -f 800 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.1 -dp 0.1 -f 1000 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.1 -dp 0.1 -f 2100 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.1 -dp 0.1 -f 3150 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.1 -dp 0.1 -f 4200 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.1 -dp 0.1 -f 5250 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.1 -dp 0.1 -f 6300 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.1 -dp 0.1 -f 7350 -p 32 && sleep 52 && ./kill_slaves.sh 

./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.2 -dp 0.2 -f 300 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.2 -dp 0.2 -f 400 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.2 -dp 0.2 -f 500 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.2 -dp 0.2 -f 1200 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.2 -dp 0.2 -f 1850 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.2 -dp 0.2 -f 2500 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.2 -dp 0.2 -f 3200 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.2 -dp 0.2 -f 3850 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.2 -dp 0.2 -f 4450 -p 32 && sleep 52 && ./kill_slaves.sh 

./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.3 -dp 0.3 -f 200 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.3 -dp 0.3 -f 250 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.3 -dp 0.3 -f 300 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.3 -dp 0.3 -f 900 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.3 -dp 0.3 -f 1400 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.3 -dp 0.3 -f 1950 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.3 -dp 0.3 -f 2500 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.3 -dp 0.3 -f 3000 -p 32 && sleep 52 && ./kill_slaves.sh 
./launch_slaves.sh -w 3 -t 30 -ap 5000 -dn 0.3 -dp 0.3 -f 3500 -p 32 && sleep 52 && ./kill_slaves.sh 

exit 0

