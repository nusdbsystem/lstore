#!/bin/bash

sh run_for_distr.sh 0 0 100000
sh run_for_distr.sh 0.01 0.01 25000
sh run_for_distr.sh 0.05 0.05 10000
sh run_for_distr.sh 0.1 0.1 5900
sh run_for_distr.sh 0.2 0.2 3650
sh run_for_distr.sh 0.3 0.3 2300
sh run_for_distr.sh 0.5 0.5 1550

exit 0
