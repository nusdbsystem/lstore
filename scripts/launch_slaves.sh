#!/bin/bash
set -o nounset
#set -o errexit
#set -o pipefail

HOSTS_FILE="slaves"

WORK_DIR="/home/linqian/owner_txn"
OUTPUT_FILE="/data0/linqian/p1.txt"
#MASTER_FILE="/data0/linqian/master.txt"

JAR_VER="0.1n"
JAR_NAME="odtp"
JAR="${WORK_DIR}/${JAR_NAME}-assembly-${JAR_VER}.jar"

NODES_FILE="${WORK_DIR}/nodes"

USER_ARGS="$@"
#echo "Arguments: ${USER_ARGS}" >> $MASTER_FILE

SCRIPT="java -jar ${JAR} -c ${NODES_FILE} ${USER_ARGS} > ${OUTPUT_FILE} &"

SSH_OPTS="-o StrictHostKeyChecking=no"

HOSTS=$(cat ${HOSTS_FILE} | tr "\\n" " ")

printf "launch "
for HOST_NAME in ${HOSTS} ; do
    ssh ${SSH_OPTS} ${HOST_NAME} "${SCRIPT}"
    printf "."
done

echo "done"

exit 0
