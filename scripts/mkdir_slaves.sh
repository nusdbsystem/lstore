#!/bin/bash
set -o nounset
#set -o errexit
#set -o pipefail

HOSTS_FILE="slaves"

SCRIPT="mkdir /data0/linqian ; mkdir /data1/linqian"

SSH_OPTS="-o StrictHostKeyChecking=no"

HOSTS=$(cat ${HOSTS_FILE} | tr "\\n" " ")

for HOST_NAME in ${HOSTS} ; do
    ssh ${SSH_OPTS} ${HOST_NAME} "${SCRIPT}"
    echo "${HOST_NAME}"
    printf "."
done

echo "done"

exit 0
