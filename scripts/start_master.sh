#!/bin/bash
set -o nounset
set -o errexit
set -o pipefail

OUTPUT_DIR="/data0/linqian"

JAR_VER="0.1n"
JAR_NAME="odtp"
JAR="${JAR_NAME}-assembly-${JAR_VER}.jar"

#if [[ $# -ne 1 ]]; then 
#	echo "Usage: $0 <master_output_name>"
#	exit 1
#fi

#MASTER_OUTPUT="$1"

#if [[ -f ${OUTPUT_DIR}/${MASTER_OUTPUT} ]]; then
#	echo "File exists: ${OUTPUT_DIR}/${MASTER_OUTPUT}"
#	exit 2
#fi

#java -jar ${JAR} -m -c nodes -d ${OUTPUT_DIR} -o ${MASTER_OUTPUT} --overwrite
java -jar ${JAR} -m -c nodes -d ${OUTPUT_DIR}

exit 0
