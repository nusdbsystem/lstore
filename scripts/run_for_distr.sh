#!/bin/bash

EXEC_TIME="30"
STARTUP_TIME="22"
REST_INTERVAL="5"

LAUNCH_SCRIPT="launch_slaves.sh"
KILL_SCRIPT="kill_slaves.sh"

WHSE_PER_NODE="3"
ABORT_PENALTY="5000"

THREAD_POOL_SIZE="4 8 16 32 64 128"

if [[ $# -ne 3 ]]; then 
	echo "Usage: $0 <distr_neworder> <distr_payment> <feed_rate_per_node>"
	exit 1
fi

DISTR_NEWORDER="$1"
DISTR_PAYMENT="$2"
FEED="$3"

for NUM_THREADS in ${THREAD_POOL_SIZE}; do
    echo "${DISTR_NEWORDER} Distr. NewOrder + ${DISTR_PAYMENT} Distr. Payment, ${NUM_THREADS} Threads/Node, ${FEED} Txns/Sec/Node"

	sh ${LAUNCH_SCRIPT} \
	    -w ${WHSE_PER_NODE} -t ${EXEC_TIME} -ap ${ABORT_PENALTY} \
	    -dn ${DISTR_NEWORDER} -dp ${DISTR_PAYMENT} -f ${FEED} \
	    -p ${NUM_THREADS} \
	&& sleep $(($EXEC_TIME + $STARTUP_TIME)) \
	&& sh ${KILL_SCRIPT}

    sleep ${REST_INTERVAL}
    
    echo ""
done

exit 0
