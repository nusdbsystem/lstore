#!/bin/bash
set -o nounset
#set -o errexit
#set -o pipefail

HOSTS_FILE="slaves"

JAR_NAME="odtp"

SCRIPT="pkill -f java.*${JAR_NAME}.*"

SSH_OPTS="-o StrictHostKeyChecking=no"

HOSTS=$(cat ${HOSTS_FILE} | tr "\\n" " ")

printf "kill "
for HOST_NAME in ${HOSTS} ; do
    ssh ${SSH_OPTS} ${HOST_NAME} "${SCRIPT}"
    #echo "${SCRIPT}"
    printf "."
done

echo "done"

exit 0
