import AssemblyKeys._

assemblySettings

name := "odtp"

version := "0.1"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "commons-lang" % "commons-lang" % "2.6",
  "net.sourceforge.collections" % "collections-generic" % "4.01",
  "com.typesafe.akka" %% "akka-actor" % "2.3.11",
  "com.typesafe.akka" %% "akka-remote" % "2.3.11",
  "args4j" % "args4j" % "2.0.31", 
  "commons-io" % "commons-io" % "2.4", 
  "org.slf4j" % "slf4j-api" % "1.7.12", 
  "ch.qos.logback" % "logback-classic" % "1.1.3"
)

scalacOptions := Seq("-deprecation", "-feature")

// qlib.Logger level: SEVERE, WARNING, INFO, FINE, FINEST
scalacOptions ++= Seq("-Xelide-below", "INFO")

// assertion switch
scalacOptions += "-Xdisable-assertions"

javacOptions := Seq("-Xlint:unchecked")
