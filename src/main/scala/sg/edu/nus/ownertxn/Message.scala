package sg.edu.nus.ownertxn

import akka.actor.ActorRef

import OwnerMgr.AbortType.AbortType
import OwnerMgr.ReqContext
import Tuple.KeyType

object Message {
  // partition ready
  case class PartitionReady(partitionId: Int) {
    override def toString() = s"PartitionReady(id=$partitionId)"
  }

  // migration start
  case object MigrationStart {
    override def toString() = "MigrationStart"
  }

  // migration done
  case class MigrationDone(partitionId: Int) {
    override def toString() = s"MigrationDone(id=$partitionId)"
  }

  // execution start
  case object ExecStart {
    override def toString() = "ExecStart"
  }

  // runtime statistics
  case class RuntimeStat(partitionId: Int, stat: Map[String, String]) {
    override def toString() = s"RuntimeStat(partition-$partitionId, stat=$stat)"
  }

  // transaction issue
  case class TxnIssue(txnId: Long) {
    override def toString() = s"TxnIssue(txnId=$txnId)"
  }

  // transaction start
  case class TxnStart(txnId: Long, timestamp: Long) {
    override def toString() = s"TxnStart(txnId=$txnId, timestamp=$timestamp)"
  }

  // transaction commit
  case class TxnCommit(txnId: Long, runCount: Int) {
    override def toString() = s"TxnCommit(txnId=$txnId, runCount=$runCount)"
  }

  // transaction abort
  case class TxnAbort(key: KeyType, txnId: Long, abortType: AbortType) {
    override def toString() =
      s"TxnAbort(key=$key, txnId=$txnId, abortType=$abortType)"
  }

  // local thread asking for ownership
  case class AskForOwner(key: KeyType, txnId: Long) {
    override def toString() = s"AskForOwner(key=$key, txnId=$txnId)"
  }

  // ownership request (for write operation)
  case class OwnerReq(key: KeyType, txnId: Long) {
    override def toString() = s"OwnerReq(key=$key, txnId=$txnId)"
  }

  // ownership response
  case class OwnerRsp(key: KeyType, values: Tuple, txnId: Long,
                      fromPartitionId: Int) {
    override def toString() = s"OwnerRsp(key=$key, txnId=$txnId, values=$values)"
  }

  // ownership transfer request
  case class OwnerTransReq(key: KeyType, reqCtx: ReqContext) {
    override def toString() = s"OwnerTransReq(key=$key, $reqCtx)"
  }

  // ownership acknowledgement
  case class OwnerAck(key: KeyType, obtained: Boolean) {
    override def toString() = s"OwnerAck(key=$key, obtained=$obtained)"
  }
}
