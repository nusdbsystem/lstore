package sg.edu.nus.ownertxn

import akka.actor.{ ActorRef, ActorSelection }
import scala.collection.mutable.{ Map => MutMap, PriorityQueue => Heap }
import qlib.Logger._

import Message._
import Tuple.KeyType
import TxnSvrProfiler.{ markTxnAbort, markOutFlow }

object OwnerMgr {

  private[this] var txnSvrActor: TxnSvrActor = null
  private[this] var storeMgr: StorageMgr = null

  /** Initialize the ownership manager
   *  @param txnSvrActor the transaction server actor.
   */
  def initOwnerMgr(txnSvrActor: TxnSvrActor) {
    this.txnSvrActor = txnSvrActor
    storeMgr = txnSvrActor.txnSvr.storeMgr
  }

  object AbortType extends Enumeration {
    type AbortType = Value
    val ABORT_FROM_OL, ABORT_FROM_OTL = Value
  }
  import AbortType._

  object OwnerLockStat extends Enumeration {
    type OwnerLockStat = Value
    val O_NEW, O_LOCKED, O_UNLOCKED = Value
  }
  import OwnerLockStat._

  case class ReqContext(txnId: Long, src: ActorRef)
    extends Ordered[ReqContext] {

    def compare(that: ReqContext): Int = this.txnId compare that.txnId

    override def toString() = s"ReqContext(txnId=$txnId, src=$src)"
  }

  import Long.{ MaxValue => MaxTxnId }

  case class OwnerLockEntry(lockStat: OwnerLockStat,
                            currTxnId: Long,
                            pendingHeap: Heap[ReqContext])

  // map: key -> (lock, curr_txn_id, heap[tnx_id, request_source])
  val ownerLock = MutMap[Long, OwnerLockEntry]()

  /** Get the ownership lock state.
   *  @param key the key of the ownership tuple.
   *  @return the lock state.
   */
  def getOwnerLockStat(key: KeyType): OwnerLockStat = {
    if (ownerLock contains key) ownerLock(key).lockStat else O_UNLOCKED
  }

  /** Occupy the ownership lock.
   *  @param key the key of the ownership tuple.
   *  @param txnId the ID of the transaction that wants to occupy the lock.
   */
  def lockOwnerLock(key: KeyType, txnId: Long) {
    if (ownerLock contains key) {
      val olEntry = ownerLock(key)
      import olEntry.{ lockStat, pendingHeap }
      assert(lockStat == O_UNLOCKED)
      ownerLock(key) = OwnerLockEntry(O_LOCKED, txnId, pendingHeap)
    } else if (storeMgr.hasTuple(key)) { // for initial state
      ownerLock(key) = OwnerLockEntry(O_LOCKED, txnId, Heap())
    } else {
      debug(s"key=$key is not locally available")
      ownerLock(key) = OwnerLockEntry(O_NEW, txnId, Heap())
    }
  }

  /** Release occupation of the ownership lock.
   *  @param key the key of the ownership tuple.
   */
  def unlockOwnerLock(key: KeyType) {
    assert(ownerLock contains key)
    val olEntry = ownerLock(key)
    import olEntry.{ lockStat, pendingHeap }
    assert(lockStat != O_UNLOCKED)

    ownerLock(key) = OwnerLockEntry(O_UNLOCKED, MaxTxnId, pendingHeap)
  }

  /** Update an ownership lock entry.
   *  @param key the key of the ownership tuple.
   *  @param txnId the ID of the transaction that wants to occupy the lock.
   *  @param lockStat the lock state that will be updated to.
   */
  def updateOwnerLock(key: KeyType, txnId: Long, lockStat: OwnerLockStat) {
    assert(ownerLock contains key)
    val pendingHeap = ownerLock(key).pendingHeap
    ownerLock(key) = OwnerLockEntry(lockStat, txnId, pendingHeap)
  }

  /** Delete an ownership lock entry.
   *  @param key the key of the ownership tuple.
   */
  def deleteOwnerLock(key: KeyType) {
    assert(getOwnerLockStat(key) == O_UNLOCKED)
    ownerLock -= key
  }

  /** Add the ownership request to the pending heap.
   *  @param key the key of the ownership tuple.
   *  @param reqCtx the context of the ownership request.
   */
  def addOwnerLockPending(key: KeyType, reqCtx: ReqContext) {
    assert(ownerLock contains key)
    val olEntry = ownerLock(key)
    import olEntry.{ lockStat, currTxnId, pendingHeap }
    assert(lockStat != O_UNLOCKED)

    import reqCtx.{ txnId, src }
    if (txnId >= currTxnId) {
      if (txnSvrActor.txn contains txnId) {
        markTxnAbort(txnId)
        val t = txnSvrActor.txn(txnId)
        t.blockingRef ! false
        t.keysObtained foreach { txnSvrActor.release(_) }

        txnSvrActor.txn -= txnId
      } else {
        val msgTxnAbort = TxnAbort(key, txnId, ABORT_FROM_OL)
        src ! msgTxnAbort
        markOutFlow(msgTxnAbort)
      }
    } else {
      pendingHeap enqueue reqCtx
    }
  }

  /** Fetch a ownership request from the pending heap.
   *  @param key the key of the ownership tuple.
   *  @return an option of ownership request.
   */
  def fetchOwnerLockPending(key: KeyType): Option[ReqContext] = {
    if (ownerLock contains key) {
      val olEntry = ownerLock(key)
      import olEntry.{ lockStat, pendingHeap }
      assert(lockStat == O_UNLOCKED)

      if (pendingHeap.isEmpty) None else Some(pendingHeap.dequeue)
    } else {
      None
    }
  }

  object OwnerTransLockStat extends Enumeration {
    type OwnerTransLockStat = Value
    val OT_LOCKED, OT_UNLOCKED = Value
  }
  import OwnerTransLockStat._

  case class OwnerTransLockEntry(lockStat: OwnerTransLockStat,
                                 holder: ActorRef,
                                 currTxnId: Long,
                                 pendingHeap: Heap[ReqContext])

  // map: key -> (lock, holder, curr_txn_id, heap[txn_id, request_source])
  val ownerTransLock = MutMap[Long, OwnerTransLockEntry]()

  /** Get the ownership transfer lock state.
   *  @param key the key of the ownership tuple.
   *  @return the lock state.
   */
  def getOwnerTransLockStat(key: KeyType): OwnerTransLockStat = {
    if (ownerTransLock contains key)
      ownerTransLock(key).lockStat
    else
      OT_UNLOCKED
  }

  /** Get the holder of a ownership.
   *  @param key the key of the ownership tuple.
   *  @return the actor reference of the ownership holder.
   */
  def getOwnerHolder(key: KeyType): ActorRef = {
    assert(storeMgr.getPartitionId(key) == txnSvrActor.txnSvr.myId)

    if (ownerTransLock contains key) {
      ownerTransLock(key).holder
    } else {
      assert(storeMgr.hasTuple(key))
      txnSvrActor.self
    }
  }

  /** Occupy the ownership transfer.
   *  @param key the key of the ownership tuple.
   *  @param txnId the ID of the transaction that wants to occupy the lock.
   */
  def lockOwnerTransLock(key: KeyType, txnId: Long) {
    assert(storeMgr.getPartitionId(key) == txnSvrActor.txnSvr.myId)

    if (ownerTransLock contains key) {
      val otlEntry = ownerTransLock(key)
      import otlEntry.{ lockStat, holder, pendingHeap }
      assert(lockStat == OT_UNLOCKED)
      ownerTransLock(key) =
        OwnerTransLockEntry(OT_LOCKED, holder, txnId, pendingHeap)
    } else {
      assert(storeMgr.hasTuple(key))
      ownerTransLock(key) =
        OwnerTransLockEntry(OT_LOCKED, txnSvrActor.self, txnId, Heap())
    }
  }

  /** Release occupation of the ownership transfer and update the ownership
   *  holder.
   *  @param key the key of the ownership tuple.
   *  @param holder the new holder of the ownership.
   */
  def unlockOwnerTransLock(key: KeyType, holder: ActorRef) {
    assert(ownerTransLock contains key)
    val otlEntry = ownerTransLock(key)
    import otlEntry.{ lockStat, pendingHeap }
    assert(lockStat == OT_LOCKED)

    ownerTransLock(key) =
      OwnerTransLockEntry(OT_UNLOCKED, holder, MaxTxnId, pendingHeap)
  }

  def deleteOwnerTransLock(key: KeyType) {
    assert(getOwnerTransLockStat(key) == OT_UNLOCKED)
    ownerTransLock -= key
  }

  /** Add the ownership request to the pending list.
   *  @param key the key of the ownership tuple.
   *  @param reqCtx the context of the ownership request.
   */
  def addOwnerTransLockPending(key: KeyType, reqCtx: ReqContext) {
    assert(ownerTransLock contains key)
    val otlEntry = ownerTransLock(key)
    import otlEntry.{ lockStat, currTxnId, pendingHeap }
    assert(lockStat == OT_LOCKED)

    import reqCtx.{ txnId, src }
    if (txnId >= currTxnId) {
      val msgTxnAbort = TxnAbort(key, txnId, ABORT_FROM_OTL)
      src ! msgTxnAbort
      if (!(txnSvrActor.txn contains txnId)) markOutFlow(msgTxnAbort)
    } else {
      pendingHeap enqueue reqCtx
    }
  }

  /** Fetch a ownership request from the pending queue.
   *  @param key the key of the ownership tuple.
   *  @return an option of ownership request.
   */
  def fetchOwnerTransLockPending(key: KeyType): Option[ReqContext] = {
    assert(ownerTransLock contains key)
    val otlEntry = ownerTransLock(key)
    import otlEntry.{ lockStat, pendingHeap }
    assert(lockStat == OT_UNLOCKED)

    if (pendingHeap.isEmpty) None else Some(pendingHeap.dequeue)
  }
}