package sg.edu.nus.ownertxn

import akka.actor.{ ActorSystem, Props }
import com.typesafe.config.ConfigFactory
import qlib.Logger._

object Master {
  def apply(conf: Map[String, String]): Master = { 
    new Master(conf)
  }
}

class Master(conf: Map[String, String])
  extends qlib.app.ExecutionBase {

  override val outputFilePrefix = Some("master")

  def run() {
    val hostname = java.net.InetAddress.getLocalHost().getHostName()
    val port = conf("masterPort")
    info(s"master: $hostname:$port")

    import ConfigFactory.{ parseString => configParse }
    val config = configParse(s"akka.remote.netty.tcp.port=${port}")
      .withFallback(configParse(s"akka.remote.netty.tcp.hostname=$hostname"))
      .withFallback(ConfigFactory.load())
    ActorSystem("OwnershipTxnSystem", config)
      .actorOf(Props(new MasterActor(conf, out)), name = "Master")
  }
}