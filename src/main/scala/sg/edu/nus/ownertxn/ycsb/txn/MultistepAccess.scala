package sg.edu.nus.ownertxn.ycsb.txn

import sg.edu.nus.ownertxn.TxnUtil._
import sg.edu.nus.ownertxn.ycsb._
import sg.edu.nus.ownertxn.ycsb.YCSBUtil._
import sg.edu.nus.ownertxn.ycsb.YCSBConstants._
import sg.edu.nus.ownertxn.ycsb.YCSBPrimaryKey._
import sg.edu.nus.ownertxn.ycsb.record._
import qlib.RichRandom._

class MultistepAccess(eng: YCSBTxnExecEngine) extends AnyYCSBTxn(eng) {
  
  override def run() {
    /* prepare */
    import eng.{ storeMgr, steps, distr, locality }
    import storeMgr.{ startLocalWhseId, endLocalWhseId, readTuple, updateTuple }

    val whseId = nextIntIncl(startLocalWhseId, endLocalWhseId)
    val (userWhseId, userId) = {
      for (_ <- 1 to steps) yield {
        if (nextDouble > distr) {
          (whseId, getLocalUserId())
        } else { // for distributed txn
          if (nextDouble < locality) {
            (getNeighborRemoteWhseId(whseId), getDistrUserId())
          } else { // for distributed txn without locality
            (getRandomRemoteWhseId(), getUserId())
          }
        }
      }
    }.unzip

    /* execute */
    startTxn(txnId)

    var continue = true
    for (i <- 0 until steps if continue) {
      waitForOwner(
        getKeyWTTableWTparition(TABLE_NAME, ycsbKey(userId(i)), userWhseId(i)),
        txnId) match {
          case false =>
            abortTxn(txnId); continue = false

          case true => {
            val tuple = readTuple(userWhseId(i), TABLE_NAME, ycsbKey(userId(i)))
              .asInstanceOf[YCSBRecord]
            val temp = tuple.f2; tuple.f2 = tuple.f1; tuple.f1 = temp
            updateTuple(userWhseId(i), TABLE_NAME, ycsbKey(userId(i)), tuple)
          }
        }
    }

    if (continue) commitTxn(txnId)
  }
}
