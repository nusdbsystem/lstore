package sg.edu.nus.ownertxn

import qlib.FileWriter
import qlib.Logger._

import Message._

class MasterActor(conf: Map[String, String], out: FileWriter)
  extends akka.actor.Actor {

  assert(out != null)
  out.printer = Some(System.out)

  def receive = {
    case PartitionReady(partitionId) => onPartitionReady(partitionId)
    case MigrationDone(partitionId) => onMigrationDone(partitionId)
    case RuntimeStat(partitionId, stat) => onRuntimeStat(partitionId, stat)
    case unknown => error(s"invalid message: $unknown")
  }

  private[this] var roundCount = 0
  private[this] val numPartitions = conf("numPartitions").toInt
  info(s"Master is ready, waiting for $numPartitions partitions")

  private[this] var waiting = (0 until numPartitions).toSet
  private[this] var blockingRefs = Set[akka.actor.ActorRef]()

  private def onPartitionReady(partitionId: Int) {
    if (waiting contains partitionId) {
      waiting -= partitionId
      blockingRefs += sender

      if (waiting.isEmpty) {
        roundCount += 1
        info(s"All partitions are ready for Round $roundCount migration. Go!")
        blockingRefs foreach { _ ! MigrationStart }

        waiting = (0 until numPartitions).toSet
        blockingRefs = Set()
      } else {
        info(s"get partition-$partitionId ready, waiting for ${waiting.size} more partition(s)")
      }
    } else {
      error(s"partition-$partitionId is not in the waiting list")
      info(s"Waiting partition IDs: $waiting")
    }
  }

  private def onMigrationDone(partitionId: Int) {
    if (waiting contains partitionId) {
      waiting -= partitionId
      blockingRefs += sender

      if (waiting.isEmpty) {
        info(s"All partitions are ready for Round $roundCount processing. Go!")
        blockingRefs foreach { _ ! ExecStart }

        waiting = (0 until numPartitions).toSet
        blockingRefs = Set()
      } else {
        info(s"get partition-$partitionId migration done, waiting for ${waiting.size} more partition(s)")
      }
    } else {
      error(s"partition-$partitionId is not in the waiting list")
      info(s"Waiting partition IDs: $waiting")
    }
  }

  var statList = List[(Int, Map[String, String])]()

  private def onRuntimeStat(partitionId: Int, stat: Map[String, String]) {
    statList :+= (partitionId, stat)

    if (statList.size == numPartitions) {
      info(s"All reports are obtained for Round $roundCount. Thanks!")
      outputStat()
      statList = Nil
    } else {
      info(s"get report from partition-$partitionId, waiting for ${numPartitions - statList.size} more partition(s)")
    }
  }

  private def outputStat() {

    out write s"--------------------< Round $roundCount >--------------------"

    statList sortBy { _._1 } foreach {
      case (i, s) =>
        val record = s"[Part-$i]" +
          " feed: " + s("inputRate") + " txns/s" +
          ", thrupt: " + s("throughput") + " txns/s" +
          ", rsp: " + s("response") + " ms" +
          ", latency: " + s("latency") + " ns" +
          ", band: " + s("bandwidth") + " Kbps" +
          ", owner: " + s("ownerTableSize") +
          ", abort: " + s("abortRate")
        out write record
    }
    out write "----------------------------------------------------"

    val totalInputRate = statList.map(_._2("inputRate").toInt).sum
    val totalCommit = statList.map(_._2("numTxnCommit").toLong).sum
    val totalAttemptOne = statList.map(_._2("numAttemptOne").toLong).sum
    val totalAttemptTwo = statList.map(_._2("numAttemptTwo").toLong).sum
    val totalAbort = statList.map(_._2("numTxnAbort").toLong).sum
    val totalResponseMillis = statList.map(_._2("accResponseMillis").toLong).sum
    val totalLatencyNanos = statList.map(_._2("accLatencyNanos").toLong).sum
    val totalOutKBits = statList.map(_._2("outKBits").toLong).sum
    val totalExecSec = statList.map(_._2("execSec").toLong).sum
    val totalOwners = statList.map(_._2("ownerTableSize").toInt).sum
    val totalTransTypeA = statList.map(_._2("numTransTypeA").toLong).sum
    val totalTransTypeB = statList.map(_._2("numTransTypeB").toLong).sum
    val totalTransTypeC = statList.map(_._2("numTransTypeC").toLong).sum
    val totalTrans = totalTransTypeA + totalTransTypeB + totalTransTypeC

    val throughput = totalCommit.toDouble / totalExecSec * numPartitions
    val response = totalResponseMillis.toDouble / totalCommit
    val latency = totalLatencyNanos.toDouble / totalCommit
    val bandwidth = totalOutKBits / totalExecSec
    val abortRate = totalAbort.toDouble / (totalCommit + totalAbort)
    val attemptOne = totalAttemptOne.toDouble / totalCommit
    val attemptTwo = totalAttemptTwo.toDouble / totalCommit
    val attemptThreePlus =
      (totalCommit - totalAttemptOne - totalAttemptTwo).toDouble / totalCommit
    val ownersPerNode = totalOwners / numPartitions
    val transTypeA = totalTransTypeA.toDouble / totalTrans
    val transTypeB = totalTransTypeB.toDouble / totalTrans
    val transTypeC = totalTransTypeC.toDouble / totalTrans

    import qlib.DecimalUtils.{ retainFourDigits => r4d }

    val record = "[GLOBAL]" +
      s" feed: $totalInputRate txns/s" +
      s", thrupt: ${throughput.toInt} txns/s" +
      s", rsp: ${response.toInt} ms" +
      s", latency: ${latency.toLong} ns" +
      s", band: ${bandwidth.toInt} Kbps" +
      s", owner: ${ownersPerNode}" +
      s", abort: ${r4d(abortRate)}" +
      s", atmpt(1,2,3+): ${r4d(attemptOne)}, ${r4d(attemptTwo)}, ${r4d(attemptThreePlus)}" +
      s", trans: ${totalTrans}" +
      s", trans(a,b,c): ${r4d(transTypeA * 100)}%, ${r4d(transTypeB * 100)}%, ${r4d(transTypeC * 100)}%"
    out write record
    out write "----------------------------------------------------"
    out.endOfFile()

    // for experiment
    result.write(s"${totalInputRate}" +
      s"\t${throughput.toInt}" +
      s"\t${response.toInt}" +
      s"\t${latency.toLong}" +
      s"\t${bandwidth.toInt}" +
      s"\t${ownersPerNode}" +
      s"\t${abortRate}" +
      s"\t${r4d(attemptOne)}\t${r4d(attemptTwo)}\t${r4d(attemptThreePlus)}" +
      s"\t${totalTrans}" +
      s"\t${r4d(transTypeA)}\t${r4d(transTypeB)}\t${r4d(transTypeC)}")
    result.endOfFile()
  }

  // for experiment
  private[this] val resultFile =
    "result_" + qlib.TimeUtils.getTimestamp(Launcher.date) + ".txt"
  private[this] val result = FileWriter(conf("outputDir") + s"/$resultFile")
}