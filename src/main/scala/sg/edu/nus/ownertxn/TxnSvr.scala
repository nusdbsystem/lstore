package sg.edu.nus.ownertxn

import java.util.{ Map => MapJava }
import scala.beans.BeanProperty
import scala.collection.JavaConverters._
import akka.actor.{ ActorSystem, Props }
import com.typesafe.config.ConfigFactory
import qlib.Logger._

object TxnSvr {

  def apply(slaveHosts: Array[String], conf: Map[String, String]): TxnSvr = {
    new TxnSvr(slaveHosts, conf)
  }

  def apply(slaveHosts: Array[String], conf: MapJava[String, String]): TxnSvr = {
    new TxnSvr(slaveHosts, conf)
  }
}

class TxnSvr(val slaveHosts: Array[String],
             val conf: Map[String, String])
  extends qlib.app.ExecutionBase {

  def this(slaveHosts: Array[String], conf: MapJava[String, String]) {
    this(slaveHosts, conf.asScala.toMap)
  }

  def getConfAsJava(): MapJava[String, String] = conf.asJava
  val hostname = java.net.InetAddress.getLocalHost().getHostName
  @BeanProperty val myId = slaveHosts.indexOf(hostname)
  require(myId >= 0, s"Invalid host: $hostname")

  @BeanProperty val storeMgr: StorageMgr = conf("benchmark") match {
    case "TPCC" => new tpcc.TpccStorageMgr(myId, conf.asJava)
    case "SEATS" => new seats.SeatsStorageMgr(myId, conf.asJava)
    case "TATP" => new tatp.TatpStorageMgr(myId, conf.asJava)
    case "YCSB" => new ycsb.YCSBStorageMgr(myId, conf.asJava)
    case _ => StorageMgr.getCustomizedStorageMgr()
  }
  require(storeMgr != null, "no storage manager is applicable")

  @BeanProperty val txnSvrActor = {
    import ConfigFactory.{ parseString => configParse }
    val config = configParse("akka.remote.netty.tcp.port=" + conf("port"))
      .withFallback(configParse(s"akka.remote.netty.tcp.hostname=$hostname"))
      .withFallback(ConfigFactory.load())

    ActorSystem("OwnershipTxnSystem", config)
      .actorOf(Props(TxnSvrActor(this)), name = "TxnSvr")
  }

  @BeanProperty var txnExecEngine: TxnExecEngine = conf("benchmark") match {
    case "TPCC" => new tpcc.TpccTxnExecEngine(this)
    case "SEATS" => new seats.SeatsTxnExecEngine(this)
    case "TATP" => new tatp.TatpTxnExecEngine(this)
    case "YCSB" => new ycsb.YCSBTxnExecEngine(this)
    case _ => new TxnExecEngine(this)
  }

  def run() {
    info(s"partition-$myId @ $hostname:" + conf("port"))
    info("benchmark: " + conf("benchmark"))

    (new Thread(txnExecEngine)).start
  }
}