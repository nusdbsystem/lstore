package sg.edu.nus.ownertxn

import java.lang.System.{ currentTimeMillis, nanoTime }
import scala.collection.mutable.{ Map => MutMap }
import qlib.Logger._
import qlib.Monitor._

import Message._
import OwnerMgr.ownerTransLock

object TxnSvrProfiler {

  private[this] var txnSvrActor: TxnSvrActor = null
  private[this] val sw = qlib.Stopwatch.createUnstarted()

  /** Initialize the ownership manager
   *  @param txnSvrActor the transaction server actor.
   */
  def initTxnSvrProfiler(txnSvrActor: TxnSvrActor) {
    this.txnSvrActor = txnSvrActor
    sw.start()
  }

  private[this] val txnIssueTime = MutMap[Long, Long]()

  def markTxnIssue(txnId: Long) {
    txnIssueTime(txnId) = currentTimeMillis
  }

  private[this] val txnStartTime = MutMap[Long, Long]()

  def markTxnStart(txnId: Long, timestamp: Long) {
    txnStartTime(txnId) = timestamp
  }

  private[this] var accResponseMillis = 0L
  private[this] var accLatencyNanos = 0L

  private[this] var numTxnCommit = 0L
  private[this] var numAttemptOne = 0L
  private[this] var numAttemptTwo = 0L

  def markTxnCommit(txnId: Long, runCount: Int) {
    accLatencyNanos += nanoTime - txnStartTime(txnId)
    accResponseMillis += currentTimeMillis - txnIssueTime(txnId)

    txnIssueTime -= txnId
    txnStartTime -= txnId

    numTxnCommit += 1
    assert(runCount > 0)
    runCount match {
      case 1 => numAttemptOne += 1
      case 2 => numAttemptTwo += 1
      case _ =>
    }
  }

  private[this] var numTxnAbort = 0L

  def markTxnAbort(txnId: Long) {
    txnIssueTime -= txnId
    txnStartTime -= txnId

    numTxnAbort += 1
  }

  private[this] var outBytes = 0L

  def markOutFlow(flowBytes: Int) {
    outBytes += flowBytes
  }

  def markOutFlow(msg: Any) {
    val flowBytes = msg match {
      case TxnAbort(key, txnId, fromOTL) => 17
      case OwnerReq(key, txnId) => 16
      case OwnerRsp(key, values, txnId, fromPartitionId) => 16 + values.size
      case OwnerTransReq(key, reqCtx) => 24
      case OwnerAck(key, obtained) => 4
      case _ => 0
    }
    markOutFlow(flowBytes)
  }

  private[this] var numTransTypeA = 0L
  private[this] var numTransTypeB = 0L
  private[this] var numTransTypeC = 0L

  def markTransType(transType: String) {
    transType match {
      case "a" => numTransTypeA += 1
      case "b" => numTransTypeB += 1
      case "c" => numTransTypeC += 1
      case _ => assert(false, s"unknown transfer type: $transType")
    }
  }

  private var lastMillis = currentTimeMillis
  private var lastNumTxnCommit = numTxnCommit
  private var lastAccResponseMillis = accResponseMillis
  private var lastAccLatencyNanos = accLatencyNanos
  private var lastOutBytes = outBytes
  val BYTES_PER_KBITS: Double = 125
  // val BYTES_PER_MBITS: Double = 125000

  info("T: Throughput, R: Response Time, L: Latency, B: Bandwidth" +
    ", U: CPU, M: Memory, OT: Owner Table, AR: Abort Rate")

  def logProfile() {
    val currMillis = currentTimeMillis
    val deltaSec: Double = (currMillis - lastMillis) / 1000.0
    lastMillis = currMillis

    val deltaNumTxnCommit = (numTxnCommit - lastNumTxnCommit).toDouble
    lastNumTxnCommit = numTxnCommit

    val deltaAccResponseMillis = accResponseMillis - lastAccResponseMillis
    lastAccResponseMillis = accResponseMillis

    val deltaAccLatencyNanos = accLatencyNanos - lastAccLatencyNanos
    lastAccLatencyNanos = accLatencyNanos

    val deltaOutBytes = outBytes - lastOutBytes
    lastOutBytes = outBytes

    val throughput = deltaNumTxnCommit / deltaSec

    val response = deltaAccResponseMillis / deltaNumTxnCommit

    val latency = deltaAccLatencyNanos / deltaNumTxnCommit

    val bandwidth = deltaOutBytes / deltaSec / BYTES_PER_KBITS

    val cpu = (getSystemCpuLoad * 1000).toInt / 10.0

    val memory = getUsedMemoryMB

    val abortRate = numTxnAbort.toDouble / (numTxnAbort + numTxnCommit)

    println(s"${sw.elapsedSec} >" +
      s" T: ${throughput.toInt} txns/s" +
      s", R: ${response.toInt} ms" +
      s", L: ${latency.toLong} ns" +
      s", B: ${bandwidth.toInt} Kbps" +
      s", U: ${cpu}%" +
      s", M: ${memory} MB" +
      s", OT: ${ownerTransLock.size}" +
      s", AR: ${abortRate}")
  }

  val EXEC_SEC_EXTEND = 3

  def getStat(): Map[String, String] = {
    val execSec = sw.elapsedSec - EXEC_SEC_EXTEND
    val throughput = numTxnCommit / execSec
    val response = accResponseMillis.toDouble / numTxnCommit
    val latency = accLatencyNanos.toDouble / numTxnCommit
    val bandwidth = outBytes / execSec / BYTES_PER_KBITS
    val abortRate = numTxnAbort.toDouble / (numTxnAbort + numTxnCommit)

    Map("numTxnCommit" -> s"$numTxnCommit",
      "numAttemptOne" -> s"$numAttemptOne",
      "numAttemptTwo" -> s"$numAttemptTwo",
      "numTxnAbort" -> s"$numTxnAbort",
      "accResponseMillis" -> s"$accResponseMillis",
      "accLatencyNanos" -> s"$accLatencyNanos",
      "outKBits" -> s"${(outBytes / BYTES_PER_KBITS).toLong}",
      "execSec" -> s"${execSec.toLong}",
      "inputRate" -> txnSvrActor.txnSvr.conf("hz"),
      "throughput" -> s"${throughput.toInt}",
      "response" -> s"${response.toInt}",
      "latency" -> s"${latency.toLong}",
      "bandwidth" -> s"${bandwidth.toInt}",
      "ownerTableSize" -> s"${ownerTransLock.size}",
      "abortRate" -> s"${abortRate}",
      "numTransTypeA" -> s"${numTransTypeA}",
      "numTransTypeB" -> s"${numTransTypeB}",
      "numTransTypeC" -> s"${numTransTypeC}")
  }

  def reportStat() {
    txnSvrActor.master ! RuntimeStat(txnSvrActor.txnSvr.myId, getStat)
    info("runtime statistics has been sent to Master")
  }

  def reportDummyStat() {
    val stat = Map("numTxnCommit" -> "1",
      "numAttemptOne" -> "1",
      "numAttemptTwo" -> "0",
      "numTxnAbort" -> "0",
      "accResponseMillis" -> "0",
      "accLatencyNanos" -> "0",
      "outKBits" -> "0",
      "execSec" -> "1",
      "inputRate" -> txnSvrActor.txnSvr.conf("hz"),
      "throughput" -> "0",
      "response" -> "0",
      "latency" -> "0",
      "bandwidth" -> "0",
      "ownerTableSize" -> "0",
      "abortRate" -> "0",
      "numTransTypeA" -> "0",
      "numTransTypeB" -> "0",
      "numTransTypeC" -> "0")

    txnSvrActor.master ! RuntimeStat(txnSvrActor.txnSvr.myId, stat)
    info("dummy statistics has been sent to Master")
  }

  def resetProfiler() {
    assert(txnIssueTime.isEmpty, "some transactions are still running")
    assert(txnStartTime.isEmpty, "some transactions are still running")

    accResponseMillis = 0L
    accLatencyNanos = 0L
    numTxnCommit = 0L
    numAttemptOne = 0L
    numAttemptTwo = 0L
    numTxnAbort = 0L
    outBytes = 0L

    lastMillis = currentTimeMillis
    lastNumTxnCommit = numTxnCommit
    lastAccResponseMillis = accResponseMillis
    lastAccLatencyNanos = accLatencyNanos
    lastOutBytes = outBytes

    sw.reset().start()
  }
}