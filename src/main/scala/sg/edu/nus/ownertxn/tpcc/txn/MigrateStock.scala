package sg.edu.nus.ownertxn.tpcc.txn

import sg.edu.nus.ownertxn.tpcc.TpccTxnExecEngine

import sg.edu.nus.ownertxn.tpcc.TPCCConfig.configItemCount
import sg.edu.nus.ownertxn.tpcc.TPCCConstants.TABLENAME_STOCK
import sg.edu.nus.ownertxn.tpcc.TPCCPrimaryKey._

import qlib.RichRandom.nextDouble

object MigrateStock {

  def apply(eng: TpccTxnExecEngine): MigrateStock = {
    new MigrateStock(eng)
  }
}

class MigrateStock(eng: TpccTxnExecEngine) extends AnyTpccTxn(eng) {

  import eng.{ storeMgr, initTxn, startTxn, waitForOwner, commitTxn }

  initTxn(txnId)

  def run() {
    startTxn(txnId)
    var gotOwner = true

    import eng.migratePercent
    if (migratePercent > 0) {
      val startLocalWhseId = storeMgr.startLocalWhseId
      val endLocalWhseId = storeMgr.endLocalWhseId
      val numTotalWhse = storeMgr.numTotalWhse
      val numWhsePerPartition = storeMgr.numWhsePerPartition

      val startRemoteWhseId: Long =
        (startLocalWhseId + numWhsePerPartition - 1) % numTotalWhse + 1
      val endRemoteWhseId: Long =
        (endLocalWhseId + numWhsePerPartition - 1) % numTotalWhse + 1

      for (whseId <- startRemoteWhseId to endRemoteWhseId) {
        for (itemId <- 1 to configItemCount) {
          if (nextDouble < migratePercent) {
            gotOwner = waitForOwner(getKeyWTTableWTparition(
              TABLENAME_STOCK, stockKey(itemId), whseId), txnId)
            assert(gotOwner)
          }
        }
      }
    }

    commitTxn(txnId)
  }
}
