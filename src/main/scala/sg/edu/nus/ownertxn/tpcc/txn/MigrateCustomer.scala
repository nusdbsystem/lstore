package sg.edu.nus.ownertxn.tpcc.txn

import sg.edu.nus.ownertxn.tpcc.TpccTxnExecEngine

import sg.edu.nus.ownertxn.tpcc.TPCCConfig.configCustPerDist
import sg.edu.nus.ownertxn.tpcc.TPCCConstants.TABLENAME_CUSTOMER
import sg.edu.nus.ownertxn.tpcc.TPCCPrimaryKey._

import qlib.RichRandom.nextDouble

object MigrateCustomer {

  def apply(eng: TpccTxnExecEngine): MigrateCustomer = {
    new MigrateCustomer(eng)
  }
}

class MigrateCustomer(eng: TpccTxnExecEngine) extends AnyTpccTxn(eng) {

  import eng.{ storeMgr, initTxn, startTxn, waitForOwner, commitTxn }

  initTxn(txnId)

  def run() {
    startTxn(txnId)
    var gotOwner = true

    import eng.migratePercent
    if (migratePercent > 0) {
      val startLocalWhseId = storeMgr.startLocalWhseId
      val endLocalWhseId = storeMgr.endLocalWhseId
      val numTotalWhse = storeMgr.numTotalWhse
      val numWhsePerPartition = storeMgr.numWhsePerPartition

      val startRemoteWhseId: Long =
        (startLocalWhseId + numWhsePerPartition - 1) % numTotalWhse + 1
      val endRemoteWhseId: Long =
        (endLocalWhseId + numWhsePerPartition - 1) % numTotalWhse + 1

      for (whseId <- startRemoteWhseId to endRemoteWhseId) {
        for (districtId <- 1 to 10) {
          for (customerId <- 1 to configCustPerDist) {
            if (nextDouble < migratePercent) {
              gotOwner = waitForOwner(
                getKeyWTTableWTparition(TABLENAME_CUSTOMER,
                  customerKey(customerId, districtId), whseId),
                txnId)
              assert(gotOwner)
            }
          }
        }
      }
    }

    commitTxn(txnId)
  }
}
