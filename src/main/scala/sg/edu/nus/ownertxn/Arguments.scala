package sg.edu.nus.ownertxn

import java.nio.file.{ Paths, Files }
import scala.{ Option => _ }
import org.kohsuke.args4j.Option

class Arguments extends qlib.app.ArgumentsBase {

  @Option(name = "-c", aliases = Array("--conf"), metaVar = "<path>",
    usage = "configuration file of server hosts [required]")
  var confFile = ""

  @Option(name = "--port", metaVar = "<num>",
    usage = "port number [def:5150]")
  var port = 5150

  @Option(name = "-w", aliases = Array("--num-warehouses"), metaVar = "<num>",
    usage = "number of warehouses per partition [def:1]")
  var numWarehouses = 1

  @Option(name = "-f", aliases = Array("--freq"), metaVar = "<num>",
    usage = "frequency (HZ) of generating transactions [def:1]")
  var hz = 1

  @Option(name = "-stress", usage = "run for stress test [def:false]")
  var stress = false

  @Option(name = "-t", aliases = Array("--time"), metaVar = "<num>",
    usage = "time span (sec) for generating transactions [def:0(infinite)]")
  var time = 0L

  @Option(name = "-distr", metaVar = "<num>",
    usage = "fraction of distributed data (general) [def:0.1]")
  var distr = 0.1

  @Option(name = "--in-situ", metaVar = "<num>",
    usage = "percentage of in-situ data processing (general) [def:0.8]")
  var inSituDataPercent = 0.8

  @Option(name = "-l", aliases = Array("--locality"), metaVar = "<num>",
    usage = "probability of distributed data request with locality [def:0.9]")
  var locality = 0.9

  // @Option(name = "-dn", aliases = Array("--distr-neworder"), metaVar = "<num>",
  //   usage = "fraction of distributed data for NewOrder transactions [def:0.01]")
  // var distrNewOrder = 0.01

  // @Option(name = "-dp", aliases = Array("--distr-payment"), metaVar = "<num>",
  //   usage = "fraction of distributed data for Payment transactions [def:0.15]")
  // var distrPayment = 0.15

  // @Option(name = "-no", aliases = Array("--neworder-only"),
  //   usage = "issue NewOrder transactions only [def:false]")
  // var neworderOnly = false

  // @Option(name = "-po", aliases = Array("--payment-only"),
  //   usage = "issue Payment transactions only [def:false]")
  // var paymentOnly = false

  @Option(name = "-p", aliases = Array("--parallel"), metaVar = "<num>",
    usage = "parallelism of the transaction execution engine [def:1]")
  var parallel = 1

  @Option(name = "-q", aliases = Array("--task-queue-limit"), metaVar = "<num>",
    usage = "maximum of queuing transactions [def:3*\"-f\"]")
  var taskQueueLimit = 0

  @Option(name = "-m", aliases = Array("--master"),
    usage = "run as the master [def:false]")
  var master = false

  @Option(name = "--master-port", metaVar = "<num>",
    usage = "master prot number [def:5151]")
  var masterPort = 5151

  @Option(name = "--with-log", usage = "enable transaction logging [def:false]")
  var withLog = false

  @Option(name = "-lp", aliases = Array("--log-penalty"), metaVar = "<num>",
    hidden = true, usage = "logging penalty (ms) to simulate transaction logging [def:2]")
  var logPenaltyMillis = 2;

  @Option(name = "-ap", aliases = Array("--abort-penalty"), metaVar = "<num>",
    hidden = true, usage = "abort penalty (us) to simulate rollback [def:5000]")
  var abortPenaltyUs = 5000

  @Option(name = "-ra", aliases = Array("--restart-aborted"), 
    hidden = true, usage = "restart aborted transactions [def:false]")
  var restartAborted = false

  @Option(name = "-mp", aliases = Array("--migrate-percent"), metaVar = "<num>",
    usage = "migration percentage [def: 0]")
  var migratePercent = 0D

  @Option(name = "-rap", aliases = Array("--remote-access-pattern"),
    metaVar = "<str>", usage = "remote access pattern: rand, binei or nei" +
      " [def:rand]")
  var remoteAccessPatternStr = "rand"

  @Option(name = "-shift", aliases = Array("--shifting-remote-access"),
    usage = "enable the test of shifting remote access [def:false]")
  var shiftRemoteAccess = false

  @Option(name = "--rand-time", metaVar = "<num>", hidden = true,
    usage = "time span (sec) for remote rand access [def:10]")
  var remoteRandTime = 10

  @Option(name = "--binei-time", metaVar = "<num>", hidden = true,
    usage = "time span (sec) for remote binei access [def:10]")
  var remoteBineiTime = 10

  @Option(name = "--nei-time", metaVar = "<num>", hidden = true,
    usage = "time span (sec) for remote nei access [def:10]")
  var remoteNeiTime = 10

  @Option(name = "-b", aliases = Array("--benchmark"), metaVar = "<str>",
    usage = "benchmark choice (TPCC, SEATS, TATP, YCSB) [def:TPCC]")
  var benchmark = "TPCC"

  @Option(name = "--report-period", metaVar = "<num>", hidden = false,
    usage = "period (sec) of reporting the performance [def:1]")
  var reportPeriod = 1

  @Option(name = "-ys", aliases = Array("--ycsb-steps"), metaVar = "<num>",
    usage = "steps per YCSB transaction [def:1]")
  var stepsPerYcsbTxn = 1

  override def sanityCheckArgs() {
    require(confFile != "", "configuration files must be given")
    require(Files.exists(Paths.get(confFile)), s"file not found: $confFile")
    require(port >= 1024, "port numbers 0 to 1023 are reserved by system")
    require(0 <= distr && distr <= 1)
    require(0 < inSituDataPercent && inSituDataPercent < 1)
    require(0 <= locality && locality <= 1)
    // require(distrNewOrder >= 0 && distrNewOrder <= 1)
    // require(distrPayment >= 0 && distrPayment <= 1)
    // require((neworderOnly && paymentOnly) == false)
    require(parallel > 0, "parallelism must be positive")
    require(taskQueueLimit >= 0)
    require(numWarehouses > 0, "number of warehouses must be positive")
    require(0 <= migratePercent && migratePercent <= 1)
    require(!(shiftRemoteAccess) || remoteRandTime > 0)
    require(!(shiftRemoteAccess) || remoteBineiTime > 0)
    require(!(shiftRemoteAccess) || remoteNeiTime > 0)
    require(reportPeriod > 0)
    require(stepsPerYcsbTxn > 0)
  }

  var masterHost = ""
  var slaveHosts = Array[String]()
  var remoteAccessPattern = 1

  override def deriveArgs() {
    val in = scala.io.Source.fromFile(confFile, "UTF-8")

    val hosts = in.getLines
      .map { _.trim }
      .filterNot { line => line.startsWith("#") || line.isEmpty }
      .toArray

    in.close()

    require(hosts.size > 2, "there must be as least 1 master and 2 slaves")
    masterHost = hosts.head
    slaveHosts = hosts.tail

    // remoteAccessPattern = {
    //   import tpcc.TpccTxnExecEngine.{
    //     REMOTE_RAND_ACCESS => TPCC_REMOTE_RANDOM,
    //     REMOTE_BINEI_ACCESS => TPCC_REMOTE_BINEIGHBOR,
    //     REMOTE_NEI_ACCESS => TPCC_REMOTE_NEIGHBOR, 
    //     UNDEF_REMOTE_ACCESS => TPCC_REMOTE_UNDEF
    //   }

    //   val rac = remoteAccessPatternStr.trim.toUpperCase match {
    //     case "RAND" => TPCC_REMOTE_RANDOM
    //     case "BINEI" => TPCC_REMOTE_BINEIGHBOR
    //     case "NEI" => TPCC_REMOTE_NEIGHBOR
    //     case _ => TPCC_REMOTE_UNDEF
    //   }
    //   require(rac != TPCC_REMOTE_UNDEF, 
    //     s"invalid remote access pattern: ${remoteAccessPatternStr}")

    //   rac
    // }

    import tpcc.TpccTxnExecEngine._
    remoteAccessPattern = remoteAccessPatternStr.trim.toLowerCase match {
      case "rand" => REMOTE_RAND_ACCESS
      case "binei" => REMOTE_BINEI_ACCESS
      case "nei" => REMOTE_NEI_ACCESS
      case _ => UNDEF_REMOTE_ACCESS
    }
    require(remoteAccessPattern > 0, s"invalid remote access pattern: $remoteAccessPatternStr")

    taskQueueLimit = if (taskQueueLimit == 0) hz * 3 else taskQueueLimit

    benchmark = benchmark.trim.toUpperCase
  }

  def getConf(): Map[String, String] = {
    Map("outputDir" -> s"${outputDir}",
      "masterHost" -> s"${masterHost}",
      "masterPort" -> s"${masterPort}",
      "numPartitions" -> s"${slaveHosts.size}",
      "numWarehouses" -> s"${numWarehouses}",
      "port" -> s"${port}",
      "hz" -> s"${hz}",
      "stress" -> s"${stress}",
      "time" -> s"${time}",
      "distr" -> s"${distr}",
      "inSituDataPercent" -> s"${inSituDataPercent}",
      "locality" -> s"${locality}",
      // "distrNewOrder" -> s"${distrNewOrder}",
      // "distrPayment" -> s"${distrPayment}",
      // "neworderOnly" -> s"${neworderOnly}",
      // "paymentOnly" -> s"${paymentOnly}",
      "parallel" -> s"${parallel}",
      "taskQueueLimit" -> s"${taskQueueLimit}",
      "withLog" -> s"${withLog}",
      "logPenaltyMillis" -> s"${logPenaltyMillis}",
      "abortPenaltyUs" -> s"${abortPenaltyUs}",
      "restartAborted" -> s"${restartAborted}",
      "migratePercent" -> s"${migratePercent}",
      "remoteAccessPattern" -> s"${remoteAccessPattern}",
      "shiftRemoteAccess" -> s"${shiftRemoteAccess}",
      "remoteRandTime" -> s"${remoteRandTime}",
      "remoteBineiTime" -> s"${remoteBineiTime}",
      "remoteNeiTime" -> s"${remoteNeiTime}",
      "benchmark" -> s"${benchmark}",
      "reportPeriod" -> s"${reportPeriod}",
      "stepsPerYcsbTxn" -> s"${stepsPerYcsbTxn}")
  }
}
