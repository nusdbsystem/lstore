package sg.edu.nus.ownertxn

object Tuple {
	type KeyType = Long
}

abstract class Tuple extends java.io.Serializable
{
    def size(): Int
}
