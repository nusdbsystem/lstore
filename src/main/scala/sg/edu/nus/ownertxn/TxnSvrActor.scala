package sg.edu.nus.ownertxn

import akka.actor.ActorRef
import akka.pattern.ask
import scala.collection.mutable.{ Map => MutMap }
import scala.concurrent.Await
import scala.concurrent.duration._
import qlib.Logger._
import qlib.TimerThread

import Message._
import Tuple.KeyType
import OwnerMgr._
import OwnerMgr.AbortType._
import OwnerMgr.OwnerLockStat._
import OwnerMgr.OwnerTransLockStat._
import TxnSvrProfiler._

object TxnSvrActor {

  def apply(txnSvr: TxnSvr): TxnSvrActor = new TxnSvrActor(txnSvr)
}

class TxnSvrActor(val txnSvr: TxnSvr)
  extends akka.actor.Actor {

  import txnSvr.{ myId, conf }
  import txnSvr.storeMgr._

  val master = {
    val hostname = conf("masterHost")
    val port = conf("masterPort")
    context.actorSelection(
      s"akka.tcp://OwnershipTxnSystem@$hostname:$port/user/Master")
  }

  val partition = {
    val port = conf("port")
    txnSvr.slaveHosts map { hostname =>
      context.actorSelection(
        s"akka.tcp://OwnershipTxnSystem@$hostname:$port/user/TxnSvr")
    }
  }

  case class TxnContext(blockingRef: ActorRef, keysObtained: List[KeyType])

  // map: txn id -> (blockRef, list[keys_obtained])
  val txn = MutMap.empty[Long, TxnContext]

  initOwnerMgr(this)
  initTxnSvrProfiler(this)

  def receive = {
    case PartitionReady(partitionId) => onPartitionReady(partitionId)
    case MigrationStart => onMigrationStart()
    case MigrationDone(partitionId) => onMigrationDone(partitionId)
    case ExecStart => onExecStart()
    case TxnIssue(txnId) => onTxnIssue(txnId)
    case TxnStart(txnId, timestamp) => onTxnStart(txnId, timestamp)
    case TxnCommit(txnId, runCount) => onTxnCommit(txnId, runCount)
    case TxnAbort(key, txnId, abortType) => onTxnAbort(key, txnId, abortType)
    case AskForOwner(key, txnId) => onAskForOwner(key, txnId)
    case OwnerReq(key, txnId) => onOwnerReq(key, txnId)
    case OwnerRsp(key, values, txnId, fromPartitionId) => onOwnerRsp(key, values, txnId, fromPartitionId)
    case OwnerTransReq(key, reqCtx) => onOwnerTransReq(key, reqCtx)
    case OwnerAck(key, obtained) => onOwnerAck(key, obtained)
    case unknown => error(s"invalid message: $unknown")
  }

  private def onPartitionReady(partitionId: Int) {
    assert(partitionId == myId)
    info(s"partition-$myId is ready")
    txn(0) = TxnContext(sender, null)
    master ! PartitionReady(myId)
  }

  private def onMigrationStart() {
    info("start data migration")
    txn(0).blockingRef ! MigrationStart
    txn -= 0
  }

  private def onMigrationDone(partitionId: Int) {
    assert(partitionId == myId)
    info("migration is done")
    txn(0) = TxnContext(sender, null)
    master ! MigrationDone(myId)
  }

  private def onExecStart() {
    info("start transaction processing")

    val reportPeriod = conf("reportPeriod").toInt
    val lastingForSec = {
      val time = conf("time").toLong
      if (time == 0) {
        reportDummyStat(); 0
      } else {
        time + TxnSvrProfiler.EXEC_SEC_EXTEND
      }
    }
    resetProfiler()
    TimerThread(logProfile, reportStat, reportPeriod, lastingForSec).start

    txn(0).blockingRef ! ExecStart
    txn -= 0
  }

  private def onTxnIssue(txnId: Long) {
    assert(!(txn contains txnId), "localCount=" + TxnId.getlocalCount(txnId))
    txn(txnId) = TxnContext(null, Nil)
    sender ! txnId
    markTxnIssue(txnId)
  }

  private def onTxnStart(txnId: Long, timestamp: Long) {
    assert(txn contains txnId)
    markTxnStart(txnId, timestamp)
  }

  private def onTxnCommit(txnId: Long, runCount: Int) {
    markTxnCommit(txnId, runCount)

    assert(txn contains txnId)
    txn(txnId).keysObtained foreach { release(_) }
    txn -= txnId
  }

  private def onTxnAbort(key: KeyType, txnId: Long, abortType: AbortType) {
    markTxnAbort(txnId)

    assert { txn contains txnId }
    val t = txn(txnId)
    import t.{ blockingRef, keysObtained }
    blockingRef ! false
    keysObtained foreach { release(_) }
    txn -= txnId

    assert(getOwnerLockStat(key) == O_NEW)
    val partitionId = getPartitionId(key)
    assert(!hasTuple(key))
    if (abortType == ABORT_FROM_OL) {
      val partitionId = getPartitionId(key)
      val msgOwnerAck = OwnerAck(key, false)
      partition(partitionId) ! msgOwnerAck
      if (partitionId != myId) markOutFlow(msgOwnerAck)
    }

    unlockOwnerLock(key)

    fetchOwnerLockPending(key) match {
      case Some(reqCtx) =>
        assert(txn contains reqCtx.txnId)
        updateOwnerLock(key, reqCtx.txnId, O_NEW)

        val msgOwnerReq = OwnerReq(key, reqCtx.txnId)
        val toPartitionId = getPartitionId(key)
        partition(toPartitionId) ! msgOwnerReq
        if (toPartitionId != myId) markOutFlow(msgOwnerReq)

      case None =>
        deleteOwnerLock(key)
    }
  }

  def release(key: KeyType) {
    getOwnerLockStat(key) match {
      case O_UNLOCKED => assert(false)

      case O_NEW =>
        val partitionId = getPartitionId(key)
        val msgOwnerAck = OwnerAck(key, true)
        partition(partitionId) ! msgOwnerAck
        if (partitionId != myId) markOutFlow(msgOwnerAck)

      case O_LOCKED =>
    }

    unlockOwnerLock(key)

    fetchOwnerLockPending(key) match {
      case Some(reqCtx) =>
        if (txn contains reqCtx.txnId) {
          onAskForOwner(key, reqCtx)
        } else {
          onOwnerTransReq(key, reqCtx)
        }

      case None =>
    }
  }

  private def onAskForOwner(key: KeyType, reqCtx: ReqContext) {
    import reqCtx.{ src, txnId }
    assert(txn contains txnId, s"txnId=$txnId")
    val keysObtained = txn(txnId).keysObtained
    txn(txnId) = TxnContext(src, keysObtained)

    getOwnerLockStat(key) match {
      case O_UNLOCKED =>
        lockOwnerLock(key, txnId)
        getOwnerLockStat(key) match {
          case O_LOCKED =>
            assert(hasTuple(key))
            src ! true
            txn(txnId) = TxnContext(null, key :: keysObtained)

          case O_NEW =>
            assert(!hasTuple(key))
            val msgOwnerReq = OwnerReq(key, txnId)
            val toPartitionId = getPartitionId(key)
            partition(toPartitionId) ! msgOwnerReq
            if (toPartitionId != myId) markOutFlow(msgOwnerReq)
            debug(s"sent $msgOwnerReq -> partition-$toPartitionId")

          case O_UNLOCKED => assert(false)
        }

      case O_NEW | O_LOCKED =>
        addOwnerLockPending(key, ReqContext(txnId, src))
    }
  }

  private def onAskForOwner(key: KeyType, txnId: Long) {
    onAskForOwner(key, ReqContext(txnId, sender))
  }

  private def onOwnerReq(key: KeyType, reqCtx: ReqContext) {
    debug(s"got OwnerReq(key=$key, txnId=${reqCtx.txnId})")

    getOwnerTransLockStat(key) match {
      case OT_UNLOCKED =>
        lockOwnerTransLock(key, reqCtx.txnId)

        if (hasTuple(key)) {
          onOwnerTransReq(key, reqCtx)
        } else {
          val holder = getOwnerHolder(key)
          assert(holder != self)
          val msgOwnerTransReq = OwnerTransReq(key, reqCtx)
          holder ! msgOwnerTransReq
          markOutFlow(msgOwnerTransReq)
          debug(s"sent $msgOwnerTransReq")
        }

      case OT_LOCKED =>
        addOwnerTransLockPending(key, reqCtx)
    }
  }

  private def onOwnerReq(key: KeyType, txnId: Long) {
    onOwnerReq(key, ReqContext(txnId, sender))
  }

  private def onOwnerRsp(key: KeyType, values: Tuple, txnId: Long,
                         fromPartitionId: Int) {
    debug(s"got OwnerRsp(key=$key, txnId=$txnId, from=$fromPartitionId)")
    assert(getOwnerLockStat(key) == O_NEW)
    assert(!hasTuple(key), s"key=$key, tuple=$values")
    insertTuple(key, values)
    debug(s"insert: key=$key, tuple=$values")

    assert(txn contains txnId)
    val t = txn(txnId)
    import t.{ blockingRef, keysObtained }
    blockingRef ! true
    txn(txnId) = TxnContext(null, key :: keysObtained)

    val keyPartitionId = getPartitionId(key)
    if (myId == keyPartitionId) {
      markTransType("a")
    } else {
      if (fromPartitionId == keyPartitionId) {
        markTransType("b")
      } else {
        markTransType("c")
      }
    }
  }

  private def onOwnerTransReq(key: KeyType, reqCtx: ReqContext) {
    debug(s"got OwnerTransReq(key=$key, txnId=${reqCtx.txnId})")
    assert(hasTuple(key))

    getOwnerLockStat(key) match {
      case O_UNLOCKED =>
        val msgOwnerRsp = OwnerRsp(key, readTuple(key), reqCtx.txnId, myId)
        reqCtx.src ! msgOwnerRsp
        markOutFlow(msgOwnerRsp)
        debug(s"sent $msgOwnerRsp")
        deleteTuple(key)
        assert(!hasTuple(key))

        fetchOwnerLockPending(key) match {
          case Some(reqCtx) =>
            import reqCtx.{ txnId, src }
            assert(txn contains txnId)
            txn(txnId) = TxnContext(src, txn(txnId).keysObtained)

            val pendingHeap = ownerLock(key).pendingHeap
            ownerLock(key) = OwnerLockEntry(O_NEW, txnId, pendingHeap)

            val msgOwnerReq = OwnerReq(key, txnId)
            val toPartitionId = getPartitionId(key)
            assert(toPartitionId != myId)
            partition(toPartitionId) ! msgOwnerReq
            markOutFlow(msgOwnerReq)
            debug(s"sent $msgOwnerReq -> partition-$toPartitionId")

          case None =>
            deleteOwnerLock(key)
        }

      case O_NEW | O_LOCKED =>
        addOwnerLockPending(key, reqCtx)
    }
  }

  private def onOwnerAck(key: KeyType, obtained: Boolean) {
    assert(getPartitionId(key) == myId)

    if (obtained) {
      unlockOwnerTransLock(key, sender)
    } else {
      val holder = getOwnerHolder(key)
      assert(holder != sender)
      unlockOwnerTransLock(key, holder)
    }

    fetchOwnerTransLockPending(key) match {
      case Some(reqCtx) =>
        onOwnerReq(key, reqCtx)

      case None =>
        if (getOwnerHolder(key) == self) deleteOwnerTransLock(key)
    }
  }
}
