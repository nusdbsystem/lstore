package sg.edu.nus.ownertxn

object Launcher extends qlib.app.LauncherBase {

  logAssertStat()
  override val args = new Arguments

  def getExecutor = {
    val conf = args.getConf

    if (args.master) {
      val fw = qlib.FileWriter("slaves", overwrite = true)
      args.slaveHosts foreach { fw write _ }
      fw.endOfFile()

      Some(Master(conf))
    } else {
      Some(TxnSvr(args.slaveHosts, conf))
    }
  }
}