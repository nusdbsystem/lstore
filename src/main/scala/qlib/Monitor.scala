package qlib

import com.sun.management._
import java.lang.management.ManagementFactory._

object Monitor {

  private[this] val osBean = getPlatformMXBean(classOf[OperatingSystemMXBean])

  def getProcessCpuLoad(): Double = osBean.getProcessCpuLoad

  def getSystemCpuLoad(): Double = osBean.getSystemCpuLoad

  private[this] val rt = Runtime.getRuntime
  private[this] val MB = 1024 * 1024

  private def bytesToMB(bytes: Long): Double =
    (bytes.toDouble / MB * 10).toInt / 10.0

  def getMaxMemoryMB(): Double = bytesToMB(rt.maxMemory)

  def getTotalMemoryMB(): Double = bytesToMB(rt.totalMemory)

  def getFreeMemoryMB(): Double = bytesToMB(rt.freeMemory)

  def getUsedMemoryMB(): Double = bytesToMB(rt.totalMemory - rt.freeMemory)
}
