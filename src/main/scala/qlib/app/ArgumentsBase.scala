package qlib.app

class ArgumentsBase {

  protected[this] val _log = org.slf4j.LoggerFactory.getLogger(this.getClass)

  import scala.{ Option => _ }
  import org.kohsuke.args4j._

  @Option(name = "-d", aliases = Array("--output-dir"), metaVar = "<path>",
    usage = "output directory [def:output]")
  var outputDir = "./output"

  @Option(name = "-o", aliases = Array("--output-file"), metaVar = "<str>",
    usage = "output filename [def:none]")
  var outputFile = ""

  @Option(name = "--overwrite", hidden = true,
    usage = "overwrite the output file if exists [def:false]")
  var overwrite = false

  @Option(name = "--ignore-output", hidden = true,
    usage = "ignore output [def:false]")
  var ignoreOutput = false

  @Option(name = "-rt", aliases = Array("--run-time"),
    usage = "show total execution time [def:false]")
  var showTotalTime = false

  @Option(name = "-h", aliases = Array("-?", "--help"), help = true,
    usage = "print this help message")
  var help = false

  protected val _screenWidth = 80

  protected def sanityCheckArgs() {}

  protected def deriveArgs() {}

  protected def argsInfo: List[String] = s"Output directory: $outputDir" ::
    s"Output filename: $outputFile" :: Nil

  protected def exampleArgs: List[String] = Nil

  def parseArgs(args: Array[String]): Boolean = {
    if (args.length > 0) _log debug ("Argument(s): " + args.mkString(" "))
    val parser = new CmdLineParser(this)
    parser.getProperties.withUsageWidth(_screenWidth)
    try {
      import scala.collection.JavaConverters._
      parser.parseArgument(args.toList.asJava)
      if (help) {
        printHelp(System.out)
      } else {
        sanityCheckArgs()
        deriveArgs()
        argsInfo foreach { _log.debug(_) }
      }
      true
    } catch {
      case e: CmdLineException => {
        System.err.println(e.getMessage + "\n")
        printHelp(System.err)
        false
      }
    }
  }

  private def printHelp(out: java.io.PrintStream) {
    out.println("OPTIONS: ")
    (new CmdLineParser(this)).printUsage(out)
    out.println()
    if (!exampleArgs.isEmpty) {
      out.println("EXAMPLES: ")
      exampleArgs foreach { x => out.println("  " + x) }
      out.println()
    }
  }
}