package qlib.app

import qlib.Logger._

trait LauncherBase {

  val args = new ArgumentsBase

  // Note: This MUST be implemented as a method.
  def getExecutor(): Option[ExecutionBase]

  val outputFileSuffix = "txt"

  val date = new java.util.Date

  protected def run(argsCmd: Array[String]) {
    if (args parseArgs argsCmd) {
      if (!args.help) {
        getExecutor() match {
          case Some(exec) => {
            exec.out = getOutputWriter(exec.outputFilePrefix)
            if (exec.out.hasWriter) {
              info("Output: " + exec.out.getFilename)
            } else {
              debug("No output is generating")
            }

            exec.run()
            exec.out.endOfFile()
          }
          case None => error("No execution is applicable")
        }
      }
    } else {
      error("Invalid option(s)")
    }
  }

  import qlib.FileWriter
  import qlib.TimeUtils._
  protected def getOutputWriter(prefix: Option[String]): FileWriter = {
    if (args.ignoreOutput || prefix.isEmpty) {
      new FileWriter
    } else {
      var name = args.outputFile
      if (name == null || name.isEmpty) {
        name = prefix.get + "_" + getTimestamp(date) + "." + outputFileSuffix
      }
      new FileWriter(args.outputDir + "/" + name, args.overwrite)
    }
  }

  def main(argsCmd: Array[String]) = {
    debug(s"START @ $date")
    val sw = qlib.Stopwatch.createStarted()
    run(argsCmd)
    sw.stop()
    debug(s"End @ Runtime: $sw")

    if (args.showTotalTime)
      println(s"Total time: $sw")
  }

  protected def logAssertStat() {
    var assertEnable = false
    assert { assertEnable = true; true }
    if (assertEnable) {
      info("assertion is enabled")
    } else {
      info("assertion is disabled")
    }
  }
}