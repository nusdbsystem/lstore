package qlib.app

trait ExecutionBase extends Runnable {

  val outputFilePrefix: Option[String] = None

  var out: qlib.FileWriter = null
}