package qlib

import language.implicitConversions
import scala.util.{ Random => ScalaRandom }
import java.util.{ Random => JavaRandom }

object RichRandom extends RichRandom {
  implicit def javaRandomToRandom(r: JavaRandom): ScalaRandom =
    new ScalaRandom(r)
}

class RichRandom extends ScalaRandom {
  def nextInt(lower: Int, upper: Int): Int = {
    require(lower < upper)
    lower + nextInt(upper - lower)
  }

  var intLower = 0;
  var intUpper = 100;
  override def nextInt() = nextInt(intLower, intUpper)

  def nextIntIncl(lower: Int, upper: Int): Int =
    nextInt(lower, upper + 1)

  def nextIntIncl(upper: Int): Int = nextIntIncl(0, upper)

  def nextDouble(lower: Double, upper: Double): Double = {
    require(lower < upper)
    lower + super.nextDouble() * (upper - lower)
  }

  def nextDouble(upper: Double): Double = nextDouble(0, upper)

  var doubleLower = 0;
  var doubleUpper = 1;
  override def nextDouble() = nextDouble(doubleLower, doubleUpper)

  override def nextString(len: Int): String = alphanumeric.take(len).mkString

  def nextString(len: Int, lenVar: Boolean): String = {
    require(len > 0)
    var l = if (lenVar) nextIntIncl(1, len) else len
    nextString(l)
  }

  var strLen = 10;
  var strLenVar = true;
  def nextString: String = nextString(strLen, strLenVar)

  private val _relOps = Array("=", "!=", ">", ">=", "<", "<=")
  def nextRelOp = _relOps(nextInt(_relOps.length))
}