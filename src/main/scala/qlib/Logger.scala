package qlib

import scala.annotation.elidable
import scala.annotation.elidable.{ SEVERE, WARNING, INFO, FINE, FINEST }

object Logger {

  private[this] val _log = org.slf4j.LoggerFactory.getLogger(this.getClass)

  // @elidable(SEVERE)
  def error(msg: String) { _log.error(msg) }

  // @elidable(WARNING)
  def warn(msg: String) { _log.warn(msg) }

  // @elidable(INFO)
  def info(msg: String) { _log.info(msg) }

  @elidable(FINE)
  def debug(msg: String) { _log.debug(msg) }

  @elidable(FINEST)
  def trace(msg: String) { _log.trace(msg) }
}