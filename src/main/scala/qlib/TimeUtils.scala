package qlib

object TimeUtils {
  import java.text._
  private[this] val _defDateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss")

  import java.util._
  def getTimestamp(date: Date): String = _defDateFormat.format(date)

  def getTimestamp: String = getTimestamp(new Date)

  def getTimestamp(millis: Long): String = getTimestamp(new Date(millis))

  def getTimestamp(date: Date, format: String): String =
    (new SimpleDateFormat(format)).format(date)

  def getTimestamp(format: String): String = getTimestamp(new Date, format)

  def getTimestamp(millis: Long, format: String): String =
    getTimestamp(new Date(millis), format)

  import java.util.concurrent.TimeUnit
  import java.util.concurrent.TimeUnit._
  def abbreviate(unit: TimeUnit): String = {
    unit match {
      case NANOSECONDS => "ns"
      case MICROSECONDS => "us"
      case MILLISECONDS => "ms"
      case SECONDS => "s"
      case MINUTES => "min"
      case HOURS => "h"
      case DAYS => "d"
      case _ => throw new Exception("Invalid time unit: " + unit)
    }
  }
}