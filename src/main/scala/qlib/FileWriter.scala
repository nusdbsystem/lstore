package qlib

import java.io._
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets._
import org.apache.commons.io.FilenameUtils._

object FileWriter {

  def apply(filename: String, overwrite: Boolean = false): FileWriter = {
    new FileWriter(filename, overwrite)
  }

  def newWriter(file: File, charset: Charset): BufferedWriter = {
    new BufferedWriter(
      new OutputStreamWriter(new FileOutputStream(file), charset))
  }
}

class FileWriter {
  private[this] val _log = org.slf4j.LoggerFactory.getLogger(this.getClass)

  private[this] var _filename: String = null
  private[this] var _writer: Option[BufferedWriter] = None

  var printer: Option[PrintStream] = None
  var flushSize = 512

  def this(filename: String, overwrite: Boolean = false) {
    this()
    _filename = normalize(filename)
    _writer = getWriter(overwrite)
  }

  def this(dir: String, prefix: String, suffix: String, overwrite: Boolean) {
    this(null, overwrite)
    _filename = constrFilename(dir, prefix, suffix)
  }

  def this(dir: String, prefix: String, suffix: String) {
    this(dir, prefix, suffix, false)
  }

  def constrFilename(dir: String, prefix: String, suffix: String): String = {
    val filename = if (dir == null) null else {
      (if (dir.isEmpty) "." else dir) + "/" +
        (if (prefix != null && !prefix.isEmpty) prefix + "_") +
        TimeUtils.getTimestamp + "." + suffix
    }
    normalize(filename)
  }

  private def getWriter(overwrite: Boolean): Option[BufferedWriter] = {
    if (_filename == null || _filename.isEmpty) {
      _log.debug("Dummy file writer is in use")
      None
    } else {
      import org.apache.commons.io.FilenameUtils._
      mkdir(getFullPath(_filename))

      val file = new File(_filename)
      if (file.exists) {
        if (overwrite) {
          file.delete
          createNewFile(file)
          _log.info("Overwriting existing file: " + _filename)
          getWriter(file)
        } else {
          _log.error("File exists (not overwriting): " + _filename)
          None
        }
      } else {
        createNewFile(file)
        _log.debug("New file created: " + _filename)
        getWriter(file)
      }
    }
  }

  private def getWriter(file: File): Option[BufferedWriter] = {
    try {
      Some(FileWriter.newWriter(file, UTF_8))
    } catch {
      case e: FileNotFoundException => {
        _log error e.getMessage
        None
      }
    }
  }

  private def mkdir(path: String): Unit = (new File(path)).mkdirs

  private def createNewFile(file: File) {
    try {
      file.createNewFile()
    } catch {
      case e: IOException => _log error e.getMessage
    }
  }

  def write(msg: Any = "") {
    val msgStr = if (msg == null) null else msg.toString
    append(msgStr)
    flush(false)
    printer match {
      case Some(ps) => ps println msgStr
      case None => // do nothing
    }
  }

  def endOfFile(markEOF: Boolean = false) {
    if (markEOF) write("EOF")
    flush(true)
  }

  private[this] var _count = 0
  private def append(msg: String) {
    _writer match {
      case Some(writer) =>
        try {
          writer.append(msg + "\n")
          _count += 1
        } catch {
          case e: IOException => _log error e.getMessage
        }
      case None => // do nothing
    }
  }

  private def flush(immediate: Boolean) {
    _writer match {
      case Some(writer) =>
        if (immediate || (_count % flushSize == 0))
          try {
            writer.flush()
          } catch {
            case e: IOException => _log error e.getMessage
          }
      case None => // do nothing
    }
  }

  def getFilename: String = _filename

  import PartialFunction._
  def hasWriter: Boolean = cond(_writer) { case Some(_) => true }
}