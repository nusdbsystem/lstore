package qlib

import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeUnit._

object Stopwatch {
  def createUnstarted() = new Stopwatch()

  def createStarted() = (new Stopwatch()).start()

  private def chooseUnit(nanos: Long): TimeUnit = {
    if (DAYS.convert(nanos, NANOSECONDS) > 0) DAYS
    else if (HOURS.convert(nanos, NANOSECONDS) > 0) HOURS
    else if (MINUTES.convert(nanos, NANOSECONDS) > 0) MINUTES
    else if (SECONDS.convert(nanos, NANOSECONDS) > 0) SECONDS
    else if (MILLISECONDS.convert(nanos, NANOSECONDS) > 0) MILLISECONDS
    else if (MICROSECONDS.convert(nanos, NANOSECONDS) > 0) MICROSECONDS
    else NANOSECONDS
  }
}

class Stopwatch private () {

  private[this] var _isRunning = false
  private[this] var _elapsedNanos = 0L
  private[this] var _startTick = 0L

  def isRunning = _isRunning

  def start(): Stopwatch = {
    if (_isRunning) throw new Exception("This stopwatch is already running")
    _isRunning = true
    _startTick = System.nanoTime
    this
  }

  def stop(): Stopwatch = {
    if (!_isRunning) throw new Exception("This stopwatch is already stopped")
    _isRunning = false
    _elapsedNanos += System.nanoTime - _startTick
    this
  }

  def reset(): Stopwatch = {
    _elapsedNanos = 0L
    _isRunning = false
    this
  }

  private def elapsedNanos =
    if (_isRunning) System.nanoTime - _startTick + _elapsedNanos
    else _elapsedNanos

  def elapsed(unit: TimeUnit): Long = unit.convert(elapsedNanos, NANOSECONDS)

  def elapsedSec(): Long = SECONDS.convert(elapsedNanos, NANOSECONDS)

  def elapsedMillis(): Long = MILLISECONDS.convert(elapsedNanos, NANOSECONDS)

  override def toString() = {
    val nanos = elapsedNanos
    val unit = Stopwatch.chooseUnit(nanos)
    val value = nanos.toDouble / NANOSECONDS.convert(1, unit)
    value + " " + TimeUtils.abbreviate(unit)
  }
}
