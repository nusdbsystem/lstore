package qlib

import java.util.concurrent.TimeUnit.MILLISECONDS
import Logger._

object TimerThread {
  def apply(mainFunc: () => Unit,
            postFunc: () => Unit = { () => },
            periodInSec: Int = 1,
            lastingForSec: Long = 0L): Thread =
    new Thread(new TimerThread(mainFunc, postFunc, periodInSec, lastingForSec))
}

class TimerThread(mainFunc: () => Unit,
                  postFunc: () => Unit = { () => },
                  periodInSec: Int = 1,
                  lastingForSec: Long = 0L)
  extends Runnable {

  require(periodInSec > 0)
  require(lastingForSec >= 0L)

  def run {
    val periodInMillis = periodInSec * 1000L
    val lastingForMillis = lastingForSec * 1000
    val sw = Stopwatch.createUnstarted

    val continue: () => Boolean = lastingForMillis match {
      case 0L => { () => true }
      case _ => { () => lastingForMillis > sw.elapsed(MILLISECONDS) }
    }

    var barInMillis = 0L

    sw.start()

    while (continue()) {
      barInMillis += periodInMillis

      mainFunc()

      val sleepForMillis = barInMillis - sw.elapsed(MILLISECONDS)
      if (sleepForMillis >= 0) {
        Thread.sleep(sleepForMillis)
      } else {
        warn("overloaded")
      }
    }

    postFunc()

    sw.stop()
    trace(s"TimerThread has run for $sw")
  }
}