package qlib

object DecimalUtils {

  def retainInt(decimal: Double): Int = decimal.toInt

  def retainOneDigit(decimal: Double): Double =
    (decimal * 10).toInt / 10.0

  def retainTwoDigits(decimal: Double): Double =
    (decimal * 100).toInt / 100.0

  def retainThreeDigits(decimal: Double): Double =
    (decimal * 1000).toInt / 1000.0

  def retainFourDigits(decimal: Double): Double =
    (decimal * 10000).toInt / 10000.0
  
  def retainFiveDigits(decimal: Double): Double =
    (decimal * 100000).toInt / 100000.0

  def retainSixDigits(decimal: Double): Double =
    (decimal * 1000000).toInt / 1000000.0
}