package qlib

import java.util.concurrent.TimeUnit.MILLISECONDS

object Ticker {
  def apply(hz: Int, lastingForSec: Long = 0L): Ticker =
    new Ticker(hz, lastingForSec)
}

class Ticker(hz: Int, lastingForSec: Long = 0L) {

  require(1 <= hz && hz <= 200 && 1000 % hz == 0,
    "supported Hz\'s: 1, 2, 4, 5, 8, 10, 20, 25, 40, 50, 100, 125, 200")
  private[this] val tickMillis = 1000L / hz
  private[this] val lastingForMillis = lastingForSec * 1000
  private[this] val sw = Stopwatch.createUnstarted

  def start(): Ticker = { sw.start(); this }

  private[this] val continue: () => Boolean = lastingForMillis match {
    case 0L => { () => true }
    case _ => { () => lastingForMillis > sw.elapsed(MILLISECONDS) }
  }

  var barInMillis = 0L

  /** Sleep until next tick arrives.
   *  @return the tick timestamp; -1 if the lasting time is exceeded.
   */
  def waitUntilNextTick(): Long = {
    require(sw.isRunning, "ticker must be started")

    if (continue()) {
      while (barInMillis <= sw.elapsed(MILLISECONDS))
        barInMillis += tickMillis

      val sleepForMillis = barInMillis - sw.elapsed(MILLISECONDS)
      Thread.sleep(sleepForMillis)
      barInMillis
    } else {
      -1
    }
  }

  def getHz(): Int = hz

  def getLastingForSec(): Long = lastingForSec
}