package sg.edu.nus.ownertxn.seats.record;

public class Airline extends sg.edu.nus.ownertxn.Tuple
{
    public Airline() {}
    
    public    int AL_ID;
    public    String AL_IATA_CODE;
    public    String AL_ICAO_CODE;
    public    String AL_CALL_SIGN;
    public    String AL_NAME;
    public    int AL_CO_ID;   //reference country co_id
    public    long AL_IATTR00;
    public    long AL_IATTR01;
    public    long AL_IATTR02;
    public    long AL_IATTR03;
    public    long AL_IATTR04;
    public    long AL_IATTR05;
    public    long AL_IATTR06;
    public    long AL_IATTR07;
    public    long AL_IATTR08;
    public    long AL_IATTR09;
    public    long AL_IATTR10;
    public    long AL_IATTR11;
    public    long AL_IATTR12;
    public    long AL_IATTR13;
    public    long AL_IATTR14;
    public    long AL_IATTR15;
    public int size() {
        return 0;
    }

    @Override
    public String toString() {
        return "Airline(" + "..." + ")";
    }
}