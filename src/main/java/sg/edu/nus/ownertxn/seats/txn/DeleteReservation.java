package sg.edu.nus.ownertxn.seats.txn;

import java.util.BitSet;

import org.apache.commons.collections15.Buffer;

import sg.edu.nus.ownertxn.seats.SEATSConstants;
import sg.edu.nus.ownertxn.seats.SEATSPrimaryKey;
import sg.edu.nus.ownertxn.seats.SeatsStorageMgr;
import sg.edu.nus.ownertxn.seats.SeatsTxnExecEngine;
import sg.edu.nus.ownertxn.seats.SeatsStorageMgr.SecondaryIndexType;
import sg.edu.nus.ownertxn.seats.SeatsTxnExecEngine.CacheType;
import sg.edu.nus.ownertxn.seats.record.*;

public class DeleteReservation extends AnySeatsTxn
{
    public DeleteReservation(SeatsTxnExecEngine eng) {
        super(eng);
        eng.initTxn(txnId);
    }
    
    protected Object[] getDeleteReservationParams() {
        // Pull off the first cached reservation and drop it on the cluster...
        Buffer<sg.edu.nus.ownertxn.seats.util.Reservation> cache = eng.CACHE_RESERVATIONS.get(SeatsTxnExecEngine.CacheType.PENDING_DELETES);
        assert(cache != null) : "Unexpected " + SeatsTxnExecEngine.CacheType.PENDING_DELETES;
        sg.edu.nus.ownertxn.seats.util.Reservation r = null;
        synchronized (cache) {
            if (cache.isEmpty() == false) r = cache.remove();
        } // SYNCH
        if (r == null) {
            return (null);
        }
        int rand;
        /*
        if (config.force_all_distributed) {
            rand = SEATSConstants.PROB_DELETE_WITH_CUSTOMER_ID_STR + 1;
        }
        else if (config.force_all_singlepartition) {
            rand = 100;
        } else {
            rand = rng.number(1, 100);
        }
        */
        rand =  rng.number(1, 100);
        
        // Parameters
        int f_id = r.flight_id;
        int c_id = -1;
        String c_id_str = "";
        String ff_c_id_str = "";
        int part = eng.storeMgr.startLocalWhseId;
        
        // Delete with the Customer's id as a string 
        if (rand <= SEATSConstants.PROB_DELETE_WITH_CUSTOMER_ID_STR) {
            c_id_str = String.format(SEATSConstants.CUSTOMER_ID_STR, r.customer_id);
        }
        // Delete using their FrequentFlyer information
        else if (rand <= SEATSConstants.PROB_DELETE_WITH_CUSTOMER_ID_STR + SEATSConstants.PROB_DELETE_WITH_FREQUENTFLYER_ID_STR) {
            ff_c_id_str = String.format(SEATSConstants.CUSTOMER_ID_STR, r.customer_id);
        }
        // Delete using their Customer id
        else {
            c_id = r.customer_id;
        }
        
        Object params[] = new Object[]{
        	part,
            f_id,           // [0] f_id
            c_id,           // [1] c_id
            c_id_str,       // [2] c_id_str
            ff_c_id_str,    // [3] ff_c_id_str
            r				// reservation
        };
        return params;
    }
    public void run() {
        /* prepare */
        SeatsStorageMgr sm = eng.storeMgr;
        

        /* execute */
        boolean gotOwner = false;
        eng.startTxn(txnId);
        Object [] params = getDeleteReservationParams();
        int partitonNum = (int)params[0];
        int f_id = (int)params[1];
        int c_id = (int)params[2];
        String c_id_str = (String)params[3];
        String ff_c_id_str = (String)params[4];
        sg.edu.nus.ownertxn.seats.util.Reservation r = (sg.edu.nus.ownertxn.seats.util.Reservation)params[5];
        //SQL GetFlight f_id
        Flight flight = (Flight)sm.readTuple(partitonNum, SEATSConstants.TABLENAME_FLIGHT, SEATSPrimaryKey.flightKey(f_id));
        assert(flight != null);
        int ff_al_id = Integer.MIN_VALUE;
        ff_al_id = flight.F_AL_ID;
        Customer [] customers = null;
        if(c_id == -1)
        {
        	boolean has_al_id = false;
        	if(c_id_str != null && c_id_str.length() >0 )
        	{
        		//SQL GetCustomerByIdStr c_id_str
        		customers = (Customer [])sm.readTuple(partitonNum, SEATSConstants.TABLENAME_CUSTOMER, c_id_str, SecondaryIndexType.IDX_C_C_ID_STR);
        	}
        	else{
        		assert(ff_c_id_str.isEmpty() == false);
        		assert(ff_al_id != Integer.MIN_VALUE);
        		//SQL GetCustomerByFFNumber ff_c_id_str
        		assert(false);
        	}
        	if(customers.length==0 || customers==null)eng.abortTxn(txnId);
        	c_id = customers[0].C_ID;
        }
        //SQL GetCustomerReservation r_c_id r_f_id
        Customer customer = (Customer) sm.readTuple(partitonNum, SEATSConstants.TABLENAME_CUSTOMER, c_id);
        Reservation [] reservations_c_id = (Reservation [])sm.readTuple(partitonNum, SEATSConstants.TABLENAME_RESERVATION, c_id, SecondaryIndexType.IDX_RESERVATION_C_ID);
        Reservation [] reservations_f_id = (Reservation [])sm.readTuple(partitonNum, SEATSConstants.TABLENAME_RESERVATION, f_id, SecondaryIndexType.IDX_RESERVATION_F_ID);
        int index=-1;
        for(int i=0;i<reservations_c_id.length;i++)
        {
        	for(int j=0;j<reservations_f_id.length;j++)
        	{
        		if(reservations_c_id[i].R_ID==reservations_f_id[j].R_ID)
        		{
        			index=i;
        			break;
        		}
        		if(index==i)break;
        	}
        }
        long c_iattr00 = customer.C_IATTR00 + 1;
        long seats_left = flight.F_SEATS_LEFT;
        int r_id = reservations_c_id[index].R_ID;
        double r_price = reservations_c_id[index].R_PRICE;
        
        //SQL DeleteReservation r_id c_id f_id        
        //SQL UpdateFlight f_id
        //SQL UpdateCustomer -1*r_price c_iattr00 c_id
        sm.deleteTuple(partitonNum, SEATSConstants.TABLENAME_RESERVATION, r_id);
        flight.F_SEATS_LEFT = flight.F_SEATS_LEFT + 1;
        sm.insertTuple(partitonNum, SEATSConstants.TABLENAME_FLIGHT, f_id, flight);
        customer.C_BALANCE = customer.C_BALANCE + r_price;
        customer.C_IATTR00 = c_iattr00;
        customer.C_IATTR10 = customer.C_IATTR10 - 1;
        customer.C_IATTR11 = customer.C_IATTR10 - 1;
        sm.insertTuple(partitonNum, SEATSConstants.TABLENAME_CUSTOMER, c_id, customer);
        BitSet seats = eng.getSeatsBitSet(r.flight_id);
        seats.set(r.seatnum,false);
        // And then put it up for a pending insert
        if (rng.nextInt(100) < SEATSConstants.PROB_REQUEUE_DELETED_RESERVATION) {
            Buffer<sg.edu.nus.ownertxn.seats.util.Reservation> cache = eng.CACHE_RESERVATIONS.get(CacheType.PENDING_INSERTS);
            assert(cache != null) : "Unexpected " + CacheType.PENDING_INSERTS;
            synchronized (cache) {
                cache.add(r);
            } // SYNCH
        }
        eng.commitTxn(txnId);
    }
}
