/***************************************************************************
 *  Copyright (C) 2011 by H-Store Project                                  *
 *  Brown University                                                       *
 *  Massachusetts Institute of Technology                                  *
 *  Yale University                                                        *
 *                                                                         *
 *  http://hstore.cs.brown.edu/                                            *
 *                                                                         *
 *  Permission is hereby granted, free of charge, to any person obtaining  *
 *  a copy of this software and associated documentation files (the        *
 *  "Software"), to deal in the Software without restriction, including    *
 *  without limitation the rights to use, copy, modify, merge, publish,    *
 *  distribute, sublicense, and/or sell copies of the Software, and to     *
 *  permit persons to whom the Software is furnished to do so, subject to  *
 *  the following conditions:                                              *
 *                                                                         *
 *  The above copyright notice and this permission notice shall be         *
 *  included in all copies or substantial portions of the Software.        *
 *                                                                         *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,        *
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF     *
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. *
 *  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR      *
 *  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,  *
 *  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR  *
 *  OTHER DEALINGS IN THE SOFTWARE.                                        *
 ***************************************************************************/
package sg.edu.nus.ownertxn.seats;

import java.io.File;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections15.map.ListOrderedMap;

import sg.edu.nus.ownertxn.util.Histogram;
import sg.edu.nus.ownertxn.util.RandomDistribution.FlatHistogram;
import sg.edu.nus.ownertxn.util.RandomGenerator;
import sg.edu.nus.ownertxn.util.StringUtil;

public class SEATSProfile {

    // ----------------------------------------------------------------
    // PERSISTENT DATA MEMBERS
    // ----------------------------------------------------------------
    
    /**
     * Data Scale Factor
     */
    protected double scale_factor;
    /**
     * For each airport id, store the last id of the customer that uses this airport
     * as their local airport. The customer ids will be stored as follows in the dbms:
     * <16-bit AirportId><48-bit CustomerId>
     */
    protected final static Histogram<Integer> airport_max_customer_id = new Histogram<Integer>();
    /**
     * The date when flights total data set begins
     */
    protected static Timestamp flight_start_date = new Timestamp(System.currentTimeMillis());
    /**
     * The date for when the flights are considered upcoming and are eligible for reservations
     */
    protected static Timestamp flight_upcoming_date;
    /**
     * The number of days in the past that our flight data set includes.
     */
    protected static long flight_past_days;
    /**
     * The number of days in the future (from the flight_upcoming_date) that our flight data set includes
     */
    protected static long flight_future_days;
    /**
     * The number of FLIGHT rows created.
     */
    protected static int num_flights = 0;
    /**
     * The number of CUSTOMER rows
     */
    protected static int num_customers = 0;
    /**
     * The number of RESERVATION rows
     */
    protected static int num_reservations = 0;

    /**
     * TODO
     **/
    protected final Map<String, Histogram<String>> histograms = new HashMap<String, Histogram<String>>();
    
    /**
     * Each AirportCode will have a histogram of the number of flights 
     * that depart from that airport to all the other airports
     */
    protected final Map<String, Histogram<String>> airport_histograms = new HashMap<String, Histogram<String>>();
    public HashMap<String,Integer> ap_code_2_apid = new HashMap<String,Integer>();

    // ----------------------------------------------------------------
    // TRANSIENT DATA MEMBERS
    // ----------------------------------------------------------------
    
    /**
     * Specialized random number generator
     */
    protected transient final RandomGenerator rng;
    
    /**
     * Depart Airport Code -> Arrive Airport Code
     * Random number generators based on the flight distributions 
     */
    private final Map<String, FlatHistogram<String>> airport_distributions = new HashMap<String, FlatHistogram<String>>();
	public File airline_data_dir;
    
    // ----------------------------------------------------------------
    // CONSTRUCTOR
    // ----------------------------------------------------------------
    
    public SEATSProfile(RandomGenerator rng,File airline_data_dir) {
        this.rng = rng;
        this.airline_data_dir = airline_data_dir;
        
    }
    
    // ----------------------------------------------------------------
    // SAVE / LOAD PROFILE
    // ----------------------------------------------------------------
    
    /**
     * Save the profile information into the database 
     */
    protected final void saveProfile() {
        
        // CONFIG_PROFILE
    	/*
        Table catalog_tbl = catalogContext.database.getTables().get(SEATSConstants.TABLENAME_CONFIG_PROFILE);
        VoltTable vt = CatalogUtil.getVoltTable(catalog_tbl);
        assert(vt != null);
        vt.addRow(
            this.scale_factor,                  // CFP_SCALE_FACTOR
            this.airport_max_customer_id.toJSONString(), // CFP_AIPORT_MAX_CUSTOMER
            this.flight_start_date,             // CFP_FLIGHT_START
            this.flight_upcoming_date,          // CFP_FLIGHT_UPCOMING
            this.flight_past_days,              // CFP_FLIGHT_PAST_DAYS
            this.flight_future_days,            // CFP_FLIGHT_FUTURE_DAYS
            this.num_flights,                   // CFP_NUM_FLIGHTS
            this.num_customers,                 // CFP_NUM_CUSTOMERS
            this.num_reservations,              // CFP_NUM_RESERVATIONS
            JSONUtil.toJSONString(this.code_id_xref) // CFP_CODE_ID_XREF
        );
        if (debug.val)
            LOG.debug(String.format("Saving profile information into %s\n%s", catalog_tbl, this));
        baseClient.loadVoltTable(catalog_tbl.getName(), vt);
        
        // CONFIG_HISTOGRAMS
        catalog_tbl = catalogContext.database.getTables().get(SEATSConstants.TABLENAME_CONFIG_HISTOGRAMS);
        vt = CatalogUtil.getVoltTable(catalog_tbl);
        assert(vt != null);
        
        for (Entry<String, Histogram<String>> e : this.airport_histograms.entrySet()) {
            vt.addRow(
                e.getKey(),                     // CFH_NAME
                e.getValue().toJSONString(),    // CFH_DATA
                1                               // CFH_IS_AIRPORT
            );
        } // FOR
        if (debug.val) LOG.debug("Saving airport histogram information into " + catalog_tbl);
        baseClient.loadVoltTable(catalog_tbl.getName(), vt);
        
        for (Entry<String, Histogram<String>> e : this.histograms.entrySet()) {
            vt.addRow(
                e.getKey(),                     // CFH_NAME
                e.getValue().toJSONString(),    // CFH_DATA
                0                               // CFH_IS_AIRPORT
            );
        } // FOR
        if (debug.val) LOG.debug("Saving benchmark histogram information into " + catalog_tbl);
        baseClient.loadVoltTable(catalog_tbl.getName(), vt);

        return;
        */
    }
    
    
    
    
    
   

    // -----------------------------------------------------------------
    // FLIGHTS
    // -----------------------------------------------------------------
    
    public int getRandomFlightId() {
        return (int)this.rng.nextInt((int)this.num_flights);
    }
    
    public int getFlightIdCount() {
        return (this.num_flights);
    }
    // ----------------------------------------------------------------
    // HISTOGRAM METHODS
    // ----------------------------------------------------------------
    
    /**
     * Return the histogram for the given name
     * @param name
     * @return
     */
    protected Histogram<String> getHistogram(String name) {
        Histogram<String> h = this.histograms.get(name);
        assert(h != null) : "Invalid histogram '" + name + "'";
        return (h);
    }
    
    /**
     * 
     * @param airport_code
     * @return
     */
    public Histogram<String> getFightsPerAirportHistogram(String airport_code) {
        return (this.airport_histograms.get(airport_code));
    }
    
    /**
     * Returns the number of histograms that we have loaded
     * Does not include the airport_histograms
     * @return
     */
    public int getHistogramCount() {
        return (this.histograms.size());
    }

    // ----------------------------------------------------------------
    // RANDOM GENERATION METHODS
    // ----------------------------------------------------------------
    
    /**
     * Return a random airport id
     * @return
     */
    public int getRandomAirportId() {
        return (this.rng.number(1, (int)this.getAirportCount()));
    }
    public String getAirportCode(int airport_id) {
        Map<String, Integer> m = this.ap_code_2_apid;
        for (Entry<String, Integer> e : m.entrySet()) {
            if (e.getValue() == airport_id) return (e.getKey());
        }
        return (null);
    }
    public int getRandomOtherAirport(int airport_id) {
        String code = this.getAirportCode(airport_id);
        FlatHistogram<String> f = this.airport_distributions.get(code);
        if (f == null) {
            synchronized (this.airport_distributions) {
                f = this.airport_distributions.get(code);
                if (f == null) {
                    Histogram<String> h = this.airport_histograms.get(code);
                    assert(h != null);
                    f = new FlatHistogram<String>(rng, h);
                    this.airport_distributions.put(code, f);
                }
            } // SYCH
        }
        assert(f != null);
        String other = f.nextValue();
        return this.getAirportId(other);
    }
    
    int getAirportId(String other) {
		// TODO Auto-generated method stub
		return this.ap_code_2_apid.get(other);
	}

	/**
     * Return a random date in the future (after the start of upcoming flights)
     * @return
     */
    public Timestamp getRandomUpcomingDate() {
        Timestamp upcoming_start_date = this.flight_upcoming_date;
        int offset = rng.nextInt((int)this.flight_future_days);
        return (new Timestamp(upcoming_start_date.getTime() + (offset * SEATSConstants.MICROSECONDS_PER_DAY)));
    }
    
//    /**
//     * Return a random FlightId from our set of cached ids
//     * @return
//     */
//    public FlightId getRandomFlightId() {
//        assert(this.cached_flight_ids.isEmpty() == false);
//        if (trace.val)
//            LOG.trace("Attempting to get a random FlightId");
//        int idx = rng.nextInt(this.cached_flight_ids.size());
//        FlightId flight_id = this.cached_flight_ids.get(idx);
//        if (trace.val)
//            LOG.trace("Got random " + flight_id);
//        return (flight_id);
//    }
    
    // ----------------------------------------------------------------
    // AIRLINE METHODS
    // ----------------------------------------------------------------
    
    public int incrementAirportCustomerCount(int airport_id) {
        int next_id = (int)this.airport_max_customer_id.get(airport_id, 0); 
        this.airport_max_customer_id.put(airport_id);
        return (next_id);
    }
    
    // ----------------------------------------------------------------
    // CUSTOMER METHODS
    // ----------------------------------------------------------------
    
    public Integer getCustomerIdCount(int airport_id) {
        return (this.airport_max_customer_id.get(airport_id));
    }
    public int getCustomerIdCount() {
        return (this.num_customers);
    }
    /**
     * Return a random customer id based out of any airport 
     * @return
     */
    public int getRandomCustomerId() {
        return this.rng.nextInt((int)this.num_customers);
    }
    
    
    /**
     * Return the number of airports that are part of this profile
     * @return
     */
    public int getAirportCount() {
        return (this.getAirportCodes().size());
    }
    
    public Collection<String> getAirportCodes() {
        return this.ap_code_2_apid.keySet();
    }

	public Histogram<String> getAirportCustomerHistogram() {
        Histogram<String> h = new Histogram<String>();
        for (int airport_id : this.airport_max_customer_id.values()) {
            String airport_code = this.getAirportCode(airport_id);
            int count = this.airport_max_customer_id.get(airport_id).intValue();
            h.put(airport_code, count);
        } // FOR
        return (h);
    }
    
    /**
     * Get the list airport codes that have flights
     * @return
     */
    public Collection<String> getAirportsWithFlights() {
        return this.airport_histograms.keySet();
    }
    
    public boolean hasFlights(String airport_code) {
        Histogram<String> h = this.getFightsPerAirportHistogram(airport_code);
        if (h != null) {
            return (h.getSampleCount() > 0);
        }
        return (false);
    }
    
    // -----------------------------------------------------------------
    // FLIGHT DATES
    // -----------------------------------------------------------------

    /**
     * The date in which the flight data set begins
     * @return
     */
    public Timestamp getFlightStartDate() {
        return this.flight_start_date;
    }
    /**
     * 
     * @param start_date
     */
    public void setFlightStartDate(Timestamp start_date) {
        this.flight_start_date = start_date;
    }

    /**
     * The date in which the flight data set begins
     * @return
     */
    public Timestamp getFlightUpcomingDate() {
        return (this.flight_upcoming_date);
    }
    /**
     * 
     * @param startDate
     */
    public void setFlightUpcomingDate(Timestamp upcoming_date) {
        this.flight_upcoming_date = upcoming_date;
    }
    
    /**
     * The date in which upcoming flights begin
     * @return
     */
    public long getFlightPastDays() {
        return (this.flight_past_days);
    }
    /**
     * 
     * @param flight_start_date
     */
    public void setFlightPastDays(long flight_past_days) {
        this.flight_past_days = flight_past_days;
    }
    
    /**
     * The date in which upcoming flights begin
     * @return
     */
    public long getFlightFutureDays() {
        return (this.flight_future_days);
    }
    /**
     * 
     * @param flight_start_date
     */
    public void setFlightFutureDays(long flight_future_days) {
        this.flight_future_days = flight_future_days;
    }
    
    public int getNextReservationId(int clientId) {
        // Offset it by the client id so that we can ensure it's unique
        int r_id = this.num_reservations++ | clientId<<20; 
        return (r_id);
    }
    
    @Override
    public String toString() {
        Map<String, Object> m = new ListOrderedMap<String, Object>();
        m.put("Scale Factor", this.scale_factor);
        m.put("# of Reservations", this.num_reservations);
        m.put("Flight Start Date", this.flight_start_date);
        m.put("Flight Upcoming Date", this.flight_upcoming_date);
        m.put("Flight Past Days", this.flight_past_days);
        m.put("Flight Future Days", this.flight_future_days);
        m.put("Num Flights", this.num_flights);
        m.put("Num Customers", this.num_customers);
        m.put("Num Reservations", this.num_reservations);
        return (StringUtil.formatMaps(m));
    }
}
