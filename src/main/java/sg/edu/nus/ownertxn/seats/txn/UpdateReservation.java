package sg.edu.nus.ownertxn.seats.txn;

import sg.edu.nus.ownertxn.seats.SeatsStorageMgr;
import sg.edu.nus.ownertxn.seats.SeatsTxnExecEngine;
import sg.edu.nus.ownertxn.seats.record.*;

public class UpdateReservation extends AnySeatsTxn
{
    public UpdateReservation(SeatsTxnExecEngine eng) {
        super(eng);
        eng.initTxn(txnId);
    }

    public void run() {
        /* prepare */
        SeatsStorageMgr sm = eng.storeMgr;

        /* execute */
        boolean gotOwner = false;
        eng.startTxn(txnId);

        eng.commitTxn(txnId);
    }
}
