package sg.edu.nus.ownertxn.seats.record;

public class FrequentFlyer extends sg.edu.nus.ownertxn.Tuple
{
    public FrequentFlyer() {}

    public int FF_C_ID; //reference C_ID
    public int FF_AL_ID; // reference AL_ID
    public String FF_C_ID_STR;
    String FF_SATTR00;
    String FF_SATTR01;
    String FF_SATTR02;
    String FF_SATTR03;
    long FF_IATTR00;
    long FF_IATTR01;
    long FF_IATTR02;
    long FF_IATTR03;
    long FF_IATTR04;
    long FF_IATTR05;
    long FF_IATTR06;
    long FF_IATTR07;
    long FF_IATTR08;
    long FF_IATTR09;
    long FF_IATTR10;
    long FF_IATTR11;
    long FF_IATTR12;
    long FF_IATTR13;
    long FF_IATTR14;
    long FF_IATTR15;
    public int size() {
        return 0;
    }

    @Override
    public String toString() {
        return "FrequentFlyer(" + "..." + ")";
    }
}