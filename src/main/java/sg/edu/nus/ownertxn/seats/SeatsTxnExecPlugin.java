package sg.edu.nus.ownertxn.seats;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static qlib.Logger.*;
import sg.edu.nus.ownertxn.TxnSvr;
import sg.edu.nus.ownertxn.seats.txn.*;

public class SeatsTxnExecPlugin extends SeatsTxnExecEngine
{
    public static enum TxnType {
        INVALID,
        DELETE_RESERVATION,
        FIND_FLIGHTS,
        FIND_OPEN_SEATS,
        NEW_RESERVATION,
        UPDATE_CUSTOMER,
        UPDATE_RESERVATION
    }

    private final BlockingQueue<TxnType> _bqueue;

    public SeatsTxnExecPlugin(TxnSvr txnSvr) {
        super(txnSvr);
        _bqueue = new LinkedBlockingQueue<TxnType>();
    }

    @Override
    protected void execute() throws Exception {
        for (;;) {
            switch (_bqueue.take()) {
            case DELETE_RESERVATION:
                threadPool.execute(new DeleteReservation(this));
                break;

            case FIND_FLIGHTS:
                threadPool.execute(new FindFlights(this));
                break;

            case FIND_OPEN_SEATS:
                threadPool.execute(new FindOpenSeats(this));
                break;

            case NEW_RESERVATION:
                threadPool.execute(new NewReservation(this));
                break;

            case UPDATE_CUSTOMER:
                threadPool.execute(new UpdateCustomer(this));
                break;

            case UPDATE_RESERVATION:
                threadPool.execute(new UpdateReservation(this));
                break;

            default:
                error("invalid transaction type");
                break;
            }
        }
    }

    public void addTxn(TxnType txnTypeId) {
        try {
            _bqueue.add(txnTypeId);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}