/******************************************************************************
 *  Copyright 2015 by OLTPBenchmark Project                                   *
 *                                                                            *
 *  Licensed under the Apache License, Version 2.0 (the "License");           *
 *  you may not use this file except in compliance with the License.          *
 *  You may obtain a copy of the License at                                   *
 *                                                                            *
 *    http://www.apache.org/licenses/LICENSE-2.0                              *
 *                                                                            *
 *  Unless required by applicable law or agreed to in writing, software       *
 *  distributed under the License is distributed on an "AS IS" BASIS,         *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *
 *  See the License for the specific language governing permissions and       *
 *  limitations under the License.                                            *
 ******************************************************************************/


package sg.edu.nus.ownertxn.seats;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;

import org.apache.commons.collections15.map.ListOrderedMap;
import org.apache.commons.collections15.set.ListOrderedSet;
import org.slf4j.Logger;

import sg.edu.nus.ownertxn.seats.SEATSConstants;
import static org.slf4j.LoggerFactory.getLogger;
import sg.edu.nus.ownertxn.seats.record.Airline;
import sg.edu.nus.ownertxn.seats.record.Airport;
import sg.edu.nus.ownertxn.seats.record.AirportDistance;
import sg.edu.nus.ownertxn.seats.record.Country;
import sg.edu.nus.ownertxn.seats.record.Flight;
import sg.edu.nus.ownertxn.seats.record.FrequentFlyer;
import sg.edu.nus.ownertxn.seats.util.DistanceUtil;
import sg.edu.nus.ownertxn.seats.util.FlightId;
import sg.edu.nus.ownertxn.seats.util.Reservation;
import sg.edu.nus.ownertxn.seats.util.ReturnFlight;
import sg.edu.nus.ownertxn.seats.util.SEATSHistogramUtil;
import sg.edu.nus.ownertxn.seats.record.Customer;
import sg.edu.nus.ownertxn.util.*;
import sg.edu.nus.ownertxn.util.RandomDistribution.*;

public class SEATSLoader{
    private static final Logger LOG = getLogger(SEATSLoader.class);
    
    // -----------------------------------------------------------------
    // INTERNAL DATA MEMBERS
    // -----------------------------------------------------------------
    
    public final SEATSProfile profile;
    
    /**
     * Mapping from Airports to their geolocation coordinates
     * AirportCode -> <Latitude, Longitude>
     */
    private final ListOrderedMap<String, Pair<Double, Double>> airport_locations = new ListOrderedMap<String, Pair<Double,Double>>();

    /**
     * AirportCode -> Set<AirportCode, Distance>
     * Only store the records for those airports in HISTOGRAM_FLIGHTS_PER_AIRPORT
     */
    private final Map<String, Map<String, Short>> airport_distances = new HashMap<String, Map<String, Short>>();

    /**
     * Store a list of FlightIds and the number of seats
     * remaining for a particular flight.
     */
    private final ListOrderedMap<FlightId, Short> seats_remaining = new ListOrderedMap<FlightId, Short>();

    /**
     * Counter for the number of tables that we have finished loading
     */
    private final AtomicInteger finished = new AtomicInteger(0);

    /**
     * A histogram of the number of flights in the database per airline code
     */
    private final Histogram<String> flights_per_airline = new Histogram<String>(true);
    private final Map<Integer, FlightInfo> flight_infos = new HashMap<Integer, FlightInfo>();
    private static class FlightInfo {
        // NOTE: These need to be strings.
        //       We will automagically convert them to their proper 
        //       ids in loadTable()
        private String depart_airport;
        private String arrive_airport;
        private String airline_code;
        private Timestamp depart_time;
        private Timestamp arrive_time;
        private int seats_remaining = SEATSConstants.FLIGHTS_NUM_SEATS;
        
        /**
         * Decrement the number of available seats for a flight and return
         * the total amount remaining
         */
        public int decrementFlightSeat() {
            assert(this.seats_remaining > 0) : "Invalid seat count";
            return (--this.seats_remaining);
        }
    }
    private final RandomGenerator rng; // FIXME
    
    private final SeatsStorageMgr sm;
    
    // -----------------------------------------------------------------
    // INITIALIZATION
    // -----------------------------------------------------------------
    
    public SEATSLoader(SeatsStorageMgr sm, File configDir) {
    	this.sm = sm;
    	this.rng = new RandomGenerator((int)System.currentTimeMillis()); // TODO: Sync with the base class rng
    	this.profile = new SEATSProfile(this.rng,configDir);
    	this.co_code3_2_co_id = new HashMap<String,Integer>();
    	this.al_code_2_alid = new HashMap<String,Integer>();
    	if (LOG.isDebugEnabled()) LOG.debug("CONSTRUCTOR: " + SEATSLoader.class.getName());
    }

    // -----------------------------------------------------------------
    // LOADING METHODS
    // -----------------------------------------------------------------
    
    public void load() throws IOException {
        if (LOG.isDebugEnabled()) LOG.debug("Begin to load tables...");
        
        // Load Histograms
        if (LOG.isDebugEnabled()) LOG.debug("Loading data files for histograms");
        this.loadHistograms();
        
        // Load the first tables from data files
        if (LOG.isDebugEnabled()) LOG.debug("Loading data files for fixed-sized tables");
        this.loadCountry();
        this.loadAireport();
        this.loadAirline();
        // Once we have those mofos, let's go get make our flight data tables
        this.flights_per_airline.putAll(al_code_2_alid.keySet(), 0);
        this.loadCustomer();
        this.loadAirport_distance();
        this.loadFlight();
        this.loadFrequent_flyer();
        this.loadReservation();
        
        // Save the benchmark profile out to disk so that we can send it
        // to all of the clients
        this.profile.saveProfile();

        if (LOG.isDebugEnabled()) LOG.debug("SEATS loader done.");
    }
    
    /**
     * Load all the histograms used in the benchmark
     */
    protected void loadHistograms() {
        if (LOG.isDebugEnabled()) 
            LOG.debug(String.format("Loading in %d histograms from files stored in '%s'",
                                    SEATSConstants.HISTOGRAM_DATA_FILES.length, profile.airline_data_dir));
        
        // Now load in the histograms that we will need for generating the flight data
        for (String histogramName : SEATSConstants.HISTOGRAM_DATA_FILES) {
            if (this.profile.histograms.containsKey(histogramName)) {
                if (LOG.isDebugEnabled()) 
                    LOG.warn("Already loaded histogram '" + histogramName + "'. Skipping...");
                continue;
            }
            if (LOG.isDebugEnabled()) 
                LOG.debug("Loading in histogram data file for '" + histogramName + "'");
            Histogram<String> hist = null;
            
            try {
                // The Flights_Per_Airport histogram is actually a serialized map that has a histogram
                // of the departing flights from each airport to all the others
                if (histogramName.equals(SEATSConstants.HISTOGRAM_FLIGHTS_PER_AIRPORT)) {
                    Map<String, Histogram<String>> m = SEATSHistogramUtil.loadAirportFlights(profile.airline_data_dir);
                    assert(m != null);
                    if (LOG.isDebugEnabled()) 
                        LOG.debug(String.format("Loaded %d airport flight histograms", m.size()));
                    
                    // Store the airport codes information
                    this.profile.airport_histograms.putAll(m);
                    
                    // We then need to flatten all of the histograms in this map into a single histogram
                    // that just counts the number of departing flights per airport. We will use this
                    // to get the distribution of where Customers are located
                    hist = new Histogram<String>();
                    for (Entry<String, Histogram<String>> e : m.entrySet()) {
                        hist.put(e.getKey(), e.getValue().getSampleCount());
                    } // FOR
                    
                // All other histograms are just serialized and can be loaded directly
                } else {
                    hist = SEATSHistogramUtil.loadHistogram(histogramName, profile.airline_data_dir, true);
                }
            } catch (Exception ex) {
                throw new RuntimeException("Failed to load histogram '" + histogramName + "'", ex);
            }
            assert(hist != null);
            this.profile.histograms.put(histogramName, hist);
            if (LOG.isDebugEnabled()) LOG.debug(String.format("Loaded histogram '%s' [sampleCount=%d, valueCount=%d]",
                                                     histogramName, hist.getSampleCount(), hist.getValueCount()));
        } // FOR

    }
    
    private HashMap<String,Integer> co_code3_2_co_id;
    private void loadCountry() throws IOException
    {
    	File file = new File(this.profile.airline_data_dir+"/table.country.csv");
    	FileReader fr = new FileReader(file);
    	BufferedReader br = new BufferedReader(fr);
    	String tmp = null;
    	int co_id =  0;
    	while((tmp = br.readLine())!=null)
    	{
    		if(co_id == 0){co_id++;continue;}
    		String [] result = tmp.split(",");
    		Country country =new Country();
    		country.CO_ID = co_id;
    		country.CO_NAME = result[0].substring(1, result[0].length()-1);
    		country.CO_CODE2 = result[1].substring(1, result[1].length()-1);
    		country.CO_CODE3 = result[2].substring(1, result[2].length()-1);
    		this.co_code3_2_co_id.put(country.CO_CODE3, new Integer(country.CO_ID));
    		for(int i=sm.startLocalWhseId;i<=sm.endLocalWhseId;i++)
    		{
    			sm.insertTuple(i, SEATSConstants.TABLENAME_COUNTRY, SEATSPrimaryKey.countryKey(co_id),country);
    		}
    		co_id++;
    	}
    	br.close();
    	fr.close();
    }
    private void loadAireport() throws IOException
    {
    	File file = new File(this.profile.airline_data_dir+"/table.airport.csv");
    	FileReader fr = new FileReader(file);
    	BufferedReader br = new BufferedReader(fr);
    	String tmp = null;
    	int ap_id =  0;
    	while((tmp = br.readLine())!=null)
    	{
    		if(ap_id == 0){ap_id++;continue;}
    		String [] result = tmp.split(",");
    		Airport tuple =new Airport();
    		tuple.AP_ID = ap_id;
    		tuple.AP_CODE = result[0].substring(1, result[0].length()-1);
    		this.profile.ap_code_2_apid.put(tuple.AP_CODE, new Integer(ap_id));
    		if (profile.hasFlights(tuple.AP_CODE) == false) continue;
    		tuple.AP_NAME = result[1].substring(1, result[1].length()-1);
    		tuple.AP_CITY = result[2].substring(1, result[2].length()-1);
    		tuple.AP_POSTAL_CODE = null;
    		tuple.AP_CO_ID = this.co_code3_2_co_id.get(result[4].substring(1, result[4].length()-1));
    		tuple.AP_LONGTITUDE = Double.valueOf(result[5]);
    		tuple.AP_LATITUDE = Double.valueOf(result[6]);
    		if(result[7].length()==0)
    			tuple.AP_GMT_OFFSET = 0;
    		else
    			tuple.AP_GMT_OFFSET = Double.valueOf(result[7]);
    		tuple.AP_WAC = Integer.valueOf(result[8]);
            Pair<Double, Double> coords = Pair.of(tuple.AP_LATITUDE, tuple.AP_LONGTITUDE);
            this.airport_locations.put(tuple.AP_CODE, coords);
    		for(int i=sm.startLocalWhseId;i<=sm.endLocalWhseId;i++)
    		{
    			sm.insertTuple(i, SEATSConstants.TABLENAME_AIRPORT, SEATSPrimaryKey.airportKey(ap_id),tuple);
    		}
    		ap_id++;
    	}
    	br.close();
    	fr.close();
    }
    HashMap<String,Integer> al_code_2_alid;
    private void loadAirline() throws IOException
    {
    	File file = new File(this.profile.airline_data_dir+"/table.airline.csv");
    	FileReader fr = new FileReader(file);
    	BufferedReader br = new BufferedReader(fr);
    	String tmp = null;
    	int al_id =  0;
    	while((tmp = br.readLine())!=null)
    	{
    		if(al_id == 0){al_id++;continue;}
    		String [] result = tmp.split("\",\"");
    		Airline tuple =new Airline();
    		tuple.AL_ID = al_id;
    		tuple.AL_IATA_CODE = result[0].substring(1, result[0].length());
    		tuple.AL_ICAO_CODE = result[1];
    		tuple.AL_CALL_SIGN = result[2];
    		tuple.AL_NAME = result[3];
    		tuple.AL_CO_ID = co_code3_2_co_id.get(result[4].substring(0, result[4].length()-1));
    		this.al_code_2_alid.put(tuple.AL_IATA_CODE, new Integer(al_id));
    		for(int i=sm.startLocalWhseId;i<=sm.endLocalWhseId;i++)
    		{
    			sm.insertTuple(i, SEATSConstants.TABLENAME_AIRLINE, SEATSPrimaryKey.airlineKey(al_id),tuple);
    		}
    		al_id++;
    	}
    	br.close();
    	fr.close();
    }

    private void loadCustomer(){
    	Histogram<String> histogram = profile.getHistogram(SEATSConstants.HISTOGRAM_FLIGHTS_PER_AIRPORT);
    	FlatHistogram<String> rand = new FlatHistogram<String>(rng, histogram);
        RandomDistribution.Flat randBalance = new RandomDistribution.Flat(rng, 1000, 10000);
        for(int i=0;i<SEATSConstants.CUSTOMERS_COUNT;i++)
        {
        	String airport_code = null;
        	String c_id_str = null;
        	Integer airport_id = null;
        	double c_balance;
        	while (airport_id == null) {
        		airport_code = rand.nextValue();
        		airport_id = this.profile.ap_code_2_apid.get(airport_code);
        	} // WHILE
        	int next_customer_id = profile.incrementAirportCustomerCount(airport_id);
        	c_id_str = String.format(SEATSConstants.CUSTOMER_ID_STR, next_customer_id);
        	c_balance = (double)randBalance.nextInt();
        	for(int j = sm.startLocalWhseId;j<=sm.endLocalWhseId;j++)
        	{
        		Customer tuple = new Customer();
        		tuple.C_ID = next_customer_id;
        		tuple.C_ID_STR = new String(c_id_str);
        		tuple.C_BASE_AP_ID = new Integer(airport_id);
        		tuple.C_BALANCE = c_balance;
        		sm.insertTuple(j, SEATSConstants.TABLENAME_CUSTOMER, SEATSPrimaryKey.customerKey(tuple.C_ID), tuple);
        	}
        }
        this.profile.num_customers = SEATSConstants.CUSTOMERS_COUNT;
    }

    private void loadAirport_distance(){
    	AirportDistanceIterable airportIterable = new AirportDistanceIterable(Integer.MAX_VALUE);
    	while(airportIterable.hasNext())
    	{
    		for(int i=sm.startLocalWhseId;i<=sm.endLocalWhseId;i++)
    		{
    			AirportDistance tuple = new AirportDistance();
    			tuple.D_AP_ID0 = this.profile.ap_code_2_apid.get(airportIterable.outer_airport);
    			tuple.D_AP_ID1 = this.profile.ap_code_2_apid.get(airportIterable.inner_airport);
    			tuple.D_DISTANCE = airportIterable.distance;
    			sm.insertTuple(i, SEATSConstants.TABLENAME_AIRPORT_DISTANCE,
    					SEATSPrimaryKey.airport_distanceKey(tuple.D_AP_ID0, tuple.D_AP_ID1) , tuple);
    		}
    	}
    }
    private void loadFlight(){
    	FlightIterable flightIterable = new FlightIterable(SEATSConstants.FLIGHTS_DAYS_PAST,SEATSConstants.FLIGHTS_DAYS_FUTURE);
    	while(flightIterable.doOne())
    	{
			Flight tuple = new Flight();
			tuple.F_ID = flightIterable.flight_id;
			tuple.F_AL_ID = al_code_2_alid.get(flightIterable.flightInfo.airline_code);
            flights_per_airline.put(flightIterable.flightInfo.airline_code);
            tuple.F_DEPART_AP_ID = this.profile.ap_code_2_apid.get(flightIterable.flightInfo.depart_airport);
            tuple.F_DEPART_TIME = flightIterable.flightInfo.depart_time.toString();
            tuple.F_ARRIVE_AP_ID = this.profile.ap_code_2_apid.get(flightIterable.flightInfo.arrive_airport);
            tuple.F_ARRIVE_TIME = flightIterable.flightInfo.arrive_time.toString();
            tuple.F_BASE_PRICE = (double)flightIterable.prices.nextInt();
            tuple.F_SEATS_TOTAL = SEATSConstants.FLIGHTS_NUM_SEATS;
            for (int seatnum = 0; seatnum < SEATSConstants.FLIGHTS_NUM_SEATS; seatnum++) {
                if (!flightIterable.seatIsOccupied()) continue;
                flightIterable.flightInfo.decrementFlightSeat();
            } // FOR
            tuple.F_SEATS_LEFT = flightIterable.flightInfo.seats_remaining;
    		for(int i=sm.startLocalWhseId;i<=sm.endLocalWhseId;i++)
    		{
    			Flight tuple1 = new Flight(tuple);
    			sm.insertTuple(i, SEATSConstants.TABLENAME_FLIGHT, SEATSPrimaryKey.flightKey(tuple1.F_ID), tuple1);
    		}
    		sg.edu.nus.ownertxn.seats.record.FlightInfo tuple2 = new sg.edu.nus.ownertxn.seats.record.FlightInfo();
    		tuple2.F_ID = tuple.F_ID;
    		tuple2.F_AL_ID = tuple.F_AL_ID;
    		tuple2.F_DEPART_AP_ID = tuple.F_DEPART_AP_ID;
    		tuple2.F_ARRIVE_AP_ID = tuple.F_ARRIVE_AP_ID;
    		tuple2.F_ARRIVE_TIME = tuple.F_ARRIVE_TIME.toString();
    		tuple2.F_DEPART_AP_ID = tuple.F_DEPART_AP_ID;
    		tuple2.F_DEPART_TIME = tuple.F_DEPART_TIME.toString();
    		tuple2.F_SEATS_TOTAL = tuple.F_SEATS_TOTAL;
    		for(int i=sm.startLocalWhseId;i<=sm.endLocalWhseId;i++)
    		{
    			sg.edu.nus.ownertxn.seats.record.FlightInfo tuple3 = new sg.edu.nus.ownertxn.seats.record.FlightInfo(tuple2);
    			sm.insertTuple(i, SEATSConstants.TABLENAME_FLIGHT_INFO, SEATSPrimaryKey.flightKey(tuple3.F_ID), tuple3);
    		}
    	}
    	this.profile.num_flights = flightIterable.flight_id;
    }
    
    private void loadFrequent_flyer(){
    	FrequentFlyerIterable frequentFlyerIterable  = new FrequentFlyerIterable(SEATSConstants.CUSTOMERS_COUNT);
    	while(frequentFlyerIterable.doOne())
    	{
    		for(int i=sm.startLocalWhseId;i<=sm.endLocalWhseId;i++)
    		{
    		FrequentFlyer tuple = new FrequentFlyer();
    		tuple.FF_C_ID = frequentFlyerIterable.last_customer_id;
    		tuple.FF_AL_ID = al_code_2_alid.get(frequentFlyerIterable.airline_id);
    		tuple.FF_C_ID_STR = frequentFlyerIterable.c_id_str;
    		sm.insertTuple(i, SEATSConstants.TABLENAME_FREQUENT_FLYER, SEATSPrimaryKey.frequent_flyerKey(tuple.FF_C_ID, tuple.FF_AL_ID), tuple);
    		}
    	}
    }
    private void loadReservation(){
    	ReservationIterable reservationIterable = new ReservationIterable((int) Math.round((SEATSConstants.FLIGHTS_PER_DAY_MIN + SEATSConstants.FLIGHTS_PER_DAY_MAX) / 2d * 1));
    	while(reservationIterable.doOne())
    	{
    		for(int i=sm.startLocalWhseId;i<=sm.endLocalWhseId;i++)
    		{
    			sg.edu.nus.ownertxn.seats.record.Reservation tuple = new sg.edu.nus.ownertxn.seats.record.Reservation();
    			tuple.R_ID = reservationIterable.last_id;
    			tuple.R_C_ID = reservationIterable.current.customer_id;
    			tuple.R_F_ID = reservationIterable.current.flight_id;
    			tuple.R_SEAT = reservationIterable.current.seatnum;
    			tuple.R_PRICE = (double)reservationIterable.prices.nextInt();
    			sm.insertTuple(i, SEATSConstants.TABLENAME_RESERVATION, SEATSPrimaryKey.reservationKey(tuple.R_ID, tuple.R_C_ID, tuple.R_F_ID), tuple);
    		}
    	}
    }
    
    
    protected class FrequentFlyerIterable{
        private final short ff_per_customer[];
        private final FlatHistogram<String> airline_rand;
        
        public int last_customer_id = 0;
        public String airline_id;
        public String c_id_str;
        private Collection<String> customer_airlines = new HashSet<String>();
        private int last_id = 0;
        private int total;
        public FrequentFlyerIterable(int num_customers) {
            
            // A customer is more likely to have a FREQUENTY_FLYER account with
            // an airline that has more flights.
            // IMPORTANT: Add one to all of the airlines so that we don't get trapped
            // in an infinite loop
            assert(flights_per_airline.isEmpty() == false);
            flights_per_airline.putAll();
            this.airline_rand = new FlatHistogram<String>(rng, flights_per_airline);
            
            // Loop through for the total customers and figure out how many entries we 
            // should have for each one. This will be our new total;
            int max_per_customer = Math.min(Math.round(SEATSConstants.CUSTOMER_NUM_FREQUENTFLYERS_MAX *1),
                                             flights_per_airline.getValueCount()); 
            Zipf ff_zipf = new Zipf(rng, SEATSConstants.CUSTOMER_NUM_FREQUENTFLYERS_MIN,
                                         max_per_customer,
                                         SEATSConstants.CUSTOMER_NUM_FREQUENTFLYERS_SIGMA);
            int new_total = 0; 
            this.ff_per_customer = new short[(int)num_customers];
            for (int i = 0; i < num_customers; i++) {
                this.ff_per_customer[i] = (short)ff_zipf.nextInt();
                if (this.ff_per_customer[i] > max_per_customer)
                    this.ff_per_customer[i] = (short)max_per_customer;
                new_total += this.ff_per_customer[i]; 
            } // FOR
            this.total = new_total;
        }
        protected boolean doOne()
        {
        	if(this.last_id>=this.total) return false;
        	while (this.last_customer_id < this.ff_per_customer.length && 
                    this.ff_per_customer[this.last_customer_id] <= 0) {
                 this.last_customer_id++;
                 this.customer_airlines.clear();
             } // WHILE
             this.ff_per_customer[this.last_customer_id]--;
             assert(this.customer_airlines.size() < flights_per_airline.getValueCount());
             do {
                 this.airline_id = this.airline_rand.nextValue();
             } while (this.customer_airlines.contains(this.airline_id));
             this.customer_airlines.add(this.airline_id);
             this.c_id_str = String.format(SEATSConstants.CUSTOMER_ID_STR, this.last_customer_id);
            this.last_id++;
            return true;
        }     
    }
    // FREQUENT_FLYER
    // ----------------------------------------------------------------

    
    // ----------------------------------------------------------------
    // AIRPORT_DISTANCE
    // ----------------------------------------------------------------
    protected class AirportDistanceIterable {
        private final int max_distance;
        private final int num_airports;
        private final Collection<String> record_airports;
        
        private int outer_ctr = 0;
        private String outer_airport;
        private Pair<Double, Double> outer_location;
        
        private Integer last_inner_ctr = null;
        private String inner_airport;
        private Pair<Double, Double> inner_location;
        public double distance;
        
        /**
         * Constructor
         * @param catalog_tbl
         * @param max_distance
         */
        public AirportDistanceIterable(int max_distance) {
        	// total work around ????
            this.max_distance = max_distance;
            this.num_airports = airport_locations.size();
            this.record_airports = profile.getAirportCodes();
        }
        
        /**
         * Find the next two airports that are within our max_distance limit
         * We keep track of where we were in the inner loop using last_inner_ctr
         */
        protected boolean hasNext() {
            for ( ; this.outer_ctr < (this.num_airports - 1); this.outer_ctr++) {
                this.outer_airport = airport_locations.get(this.outer_ctr);
                this.outer_location = airport_locations.getValue(this.outer_ctr);
                if (profile.hasFlights(this.outer_airport) == false) continue;
                
                int inner_ctr = (this.last_inner_ctr != null ? this.last_inner_ctr : this.outer_ctr + 1);
                this.last_inner_ctr = null;
                for ( ; inner_ctr < this.num_airports; inner_ctr++) {
                    assert(this.outer_ctr != inner_ctr);
                    this.inner_airport = airport_locations.get(inner_ctr);
                    this.inner_location = airport_locations.getValue(inner_ctr);
                    if (profile.hasFlights(this.inner_airport) == false) continue;
                    this.distance = DistanceUtil.distance(this.outer_location, this.inner_location);

                    // Store the distance between the airports locally if either one is in our
                    // flights-per-airport data set
                    if (this.record_airports.contains(this.outer_airport) &&
                        this.record_airports.contains(this.inner_airport)) {
                        SEATSLoader.this.setDistance(this.outer_airport, this.inner_airport, this.distance);
                    }

                    // Stop here if these two airports are within range
                    if (this.distance > 0 && this.distance <= this.max_distance) {
                        // System.err.println(this.outer_airport + "->" + this.inner_airport + ": " + distance);
                        this.last_inner_ctr = inner_ctr + 1;
                        return (true);
                    }
                } // FOR
            } // FOR
            return (false);
        }
    }
    
    // ----------------------------------------------------------------
    // FLIGHTS
    // ----------------------------------------------------------------
    protected class FlightIterable {
        private int NEXT_FLIGHT_ID = 0;
        
        private final FlatHistogram<String> airlines;
        private final FlatHistogram<String> airports;
        private final Map<String, FlatHistogram<String>> flights_per_airport = new HashMap<String, FlatHistogram<String>>();
        private final FlatHistogram<String> flight_times;
        public final Flat prices;
        
        // private final Set<FlightId> todays_flights = new HashSet<FlightId>();
        private final Set<Integer> todays_flights = new HashSet<Integer>();
        private final ListOrderedMap<Timestamp, Integer> flights_per_day = new ListOrderedMap<Timestamp, Integer>();
        
        private int day_idx = 0;
        private Timestamp today;
        private Timestamp start_date;
        
        // private FlightId flight_id;
        public int flight_id;
        public FlightInfo flightInfo;
        
        private int total;
        
        public FlightIterable(int days_past, int days_future) {
            assert(days_past >= 0);
            assert(days_future >= 0);
            
            this.prices = new Flat(rng,
                    SEATSConstants.RESERVATION_PRICE_MIN,
                    SEATSConstants.RESERVATION_PRICE_MAX);
            
            // Flights per Airline
            Collection<String> all_airlines = al_code_2_alid.keySet();
            Histogram<String> histogram = new Histogram<String>();
            histogram.putAll(all_airlines);
            
            // Embed a Gaussian distribution
            Gaussian gauss_rng = new Gaussian(rng, 0, all_airlines.size());
            this.airlines = new FlatHistogram<String>(gauss_rng, histogram);
            
            // Flights Per Airport
            histogram = profile.getHistogram(SEATSConstants.HISTOGRAM_FLIGHTS_PER_AIRPORT);  
            this.airports = new FlatHistogram<String>(rng, histogram);
            for (String airport_code : histogram.values()) {
                histogram = profile.getFightsPerAirportHistogram(airport_code);
                assert(histogram != null) : "Unexpected departure airport code '" + airport_code + "'";
                this.flights_per_airport.put(airport_code, new FlatHistogram<String>(rng, histogram));
            } // FOR
            
            // Flights Per Departure Time
            histogram = profile.getHistogram(SEATSConstants.HISTOGRAM_FLIGHTS_PER_DEPART_TIMES);  
            this.flight_times = new FlatHistogram<String>(rng, histogram);
            
            // Figure out how many flights that we want for each day
            this.today = new Timestamp(System.currentTimeMillis());
            
            // Sometimes there are more flights per day, and sometimes there are fewer
            int flightsPerDayMin = SEATSConstants.FLIGHTS_PER_DAY_MIN;
            int flightsPerDayMax = SEATSConstants.FLIGHTS_PER_DAY_MAX;
            Gaussian gaussian = new Gaussian(rng, flightsPerDayMin, flightsPerDayMax);
            
            this.total = 0;
            boolean first = true;
            for (long t = this.today.getTime() - (days_past * SEATSConstants.MICROSECONDS_PER_DAY);
                 t < this.today.getTime(); t += SEATSConstants.MICROSECONDS_PER_DAY) {
                Timestamp timestamp = new Timestamp(t);
                if (first) {
                    this.start_date = timestamp;
                    first = false;
                }
                int num_flights = gaussian.nextInt();
                this.flights_per_day.put(timestamp, num_flights);
                this.total += num_flights;
            } // FOR
            if (this.start_date == null) this.start_date = this.today;
            profile.setFlightStartDate(this.start_date);
            
            // This is for upcoming flights that we want to be able to schedule
            // new reservations for in the benchmark
            profile.setFlightUpcomingDate(this.today);
            for (long t = this.today.getTime(), last_date = this.today.getTime() + (days_future * SEATSConstants.MICROSECONDS_PER_DAY);
                 t <= last_date; t += SEATSConstants.MICROSECONDS_PER_DAY) {
                Timestamp timestamp = new Timestamp(t);
                int num_flights = gaussian.nextInt();
                this.flights_per_day.put(timestamp, num_flights);
                this.total += num_flights;
            } // FOR
            
            // Update profile
            profile.setFlightPastDays(days_past);
            profile.setFlightFutureDays(days_future);
        }
        
        /**
         * Convert a time string "HH:MM" to a TimestampType object
         * @param code
         * @return
         */
        private Timestamp convertTimeString(Timestamp base_date, String code) {
            Matcher m = SEATSConstants.TIMECODE_PATTERN.matcher(code);
            boolean result = m.find();
            assert(result) : "Invalid time code '" + code + "'";
            
            int hour = -1;
            try {
                hour = Integer.valueOf(m.group(1));
            } catch (Throwable ex) {
                throw new RuntimeException("Invalid HOUR in time code '" + code + "'", ex);
            }
            assert(hour != -1);
            
            int minute = -1;
            try {
                minute = Integer.valueOf(m.group(2));
            } catch (Throwable ex) {
                throw new RuntimeException("Invalid MINUTE in time code '" + code + "'", ex);
            }
            assert(minute != -1);
            
            long offset = (hour * 60 * SEATSConstants.MICROSECONDS_PER_MINUTE) + (minute * SEATSConstants.MICROSECONDS_PER_MINUTE);
            return (new Timestamp(base_date.getTime() + offset));
        }
        
        /**
         * Select all the data elements for the current tuple
         * @param date
         */
        private FlightInfo populate(Integer flight_id, Timestamp date) {
            FlightInfo flightInfo = new FlightInfo();
            
            // Depart/Arrive Airports
            String airport_code = this.airports.nextValue();
            flightInfo.depart_airport = airport_code;
            flightInfo.arrive_airport = this.flights_per_airport.get(airport_code).nextValue();

            // Depart/Arrive Times
            flightInfo.depart_time = this.convertTimeString(date, this.flight_times.nextValue());
            flightInfo.arrive_time = SEATSLoader.this.calculateArrivalTime(flightInfo.depart_airport,
                                                                           flightInfo.arrive_airport,
                                                                           flightInfo.depart_time);

            // Airline
            flightInfo.airline_code = this.airlines.nextValue();
            // flightInfo.airline_id = profile.getAirlineId(flightInfo.airline_code);
            
            this.flights_per_day.put(date, this.flights_per_day.get(date) - 1);
            SEATSLoader.this.flight_infos.put(flight_id, flightInfo);
            
            return (flightInfo);
        }
        
        /** 
         * Returns true if this seat is occupied (which means we must generate a reservation)
         */
        boolean seatIsOccupied() {
            return (rng.nextInt(100) < SEATSConstants.PROB_SEAT_OCCUPIED);
        }
        
        protected boolean doOne()
        {
        	if(this.NEXT_FLIGHT_ID>=this.total) return false;
        	Integer remaining = null;
            Timestamp date; 
            do {
                // Move to the next day.
                // Make sure that we reset the set of FlightIds that we've used for today
                if (remaining != null) {
                    this.todays_flights.clear();
                    this.day_idx++;
                }
                date = this.flights_per_day.get(this.day_idx);
                remaining = this.flights_per_day.getValue(this.day_idx);
            } while (remaining <= 0 && this.day_idx + 1 < this.flights_per_day.size());
            assert(date != null);
            
            this.flight_id = NEXT_FLIGHT_ID++;
            this.flightInfo = this.populate(this.flight_id, date);
            this.todays_flights.add(this.flight_id);
            return true;
        }
        protected Object specialValue(long id, int columnIdx) {
            Object value = null;
            switch (columnIdx) {
                // FLIGHT ID
                case (0): {
                    // Figure out what date we are currently on
                    Integer remaining = null;
                    Timestamp date; 
                    do {
                        // Move to the next day.
                        // Make sure that we reset the set of FlightIds that we've used for today
                        if (remaining != null) {
                            this.todays_flights.clear();
                            this.day_idx++;
                        }
                        date = this.flights_per_day.get(this.day_idx);
                        remaining = this.flights_per_day.getValue(this.day_idx);
                    } while (remaining <= 0 && this.day_idx + 1 < this.flights_per_day.size());
                    assert(date != null);
                    
                    value = this.flight_id = NEXT_FLIGHT_ID++;
                    this.flightInfo = this.populate(this.flight_id, date);
                    this.todays_flights.add(this.flight_id);
                    break;
                }
                // AIRLINE ID
                case (1): {
                    value = this.flightInfo.airline_code;
                    flights_per_airline.put(this.flightInfo.airline_code);
                    break;
                }
                // DEPART AIRPORT
                case (2): {
                    value = this.flightInfo.depart_airport;
                    break;
                }
                // DEPART TIME
                case (3): {
                    value = this.flightInfo.depart_time;
                    break;
                }
                // ARRIVE AIRPORT
                case (4): {
                    value = this.flightInfo.arrive_airport;
                    break;
                }
                // ARRIVE TIME
                case (5): {
                    value = this.flightInfo.arrive_time;
                    break;
                }
                // BASE PRICE
                case (6): {
                    value = (double)this.prices.nextInt();
                    break;
                }
                // SEATS TOTAL
                case (7): {
                    value = SEATSConstants.FLIGHTS_NUM_SEATS;
                    break;
                }
                // SEATS REMAINING
                case (8): {
                    // We have to figure this out ahead of time since we need to populate the tuple now
                    for (int seatnum = 0; seatnum < SEATSConstants.FLIGHTS_NUM_SEATS; seatnum++) {
                        if (!this.seatIsOccupied()) continue;
                        this.flightInfo.decrementFlightSeat();
                    } // FOR
                    value = this.flightInfo.seats_remaining;
                    break;
                }
                // BAD MOJO!
                default:
                    assert(false) : "Unexpected special column index " + columnIdx;
            } // SWITCH
            return (value);
        }
    }
    
    // ----------------------------------------------------------------
    // RESERVATIONS
    // ----------------------------------------------------------------
        // ----------------------------------------------------------------
    // RESERVATIONS
    // ----------------------------------------------------------------
    protected class ReservationIterable{
        private final RandomDistribution.Flat prices = new RandomDistribution.Flat(rng, SEATSConstants.RESERVATION_PRICE_MIN, SEATSConstants.RESERVATION_PRICE_MAX);
        
        /**
         * For each airport id, store a list of ReturnFlight objects that represent customers
         * that need return flights back to their home airport
         * ArriveAirportId -> ReturnFlights
         */
        private final Map<Integer, TreeSet<ReturnFlight>> airport_returns = new HashMap<Integer, TreeSet<ReturnFlight>>();
        
        /**
         * When this flag is true, then the data generation thread is finished
         */
        private boolean done = false;
        
        /**
         * We use a Gaussian distribution for determining how long a customer will stay at their
         * destination before needing to return to their original airport
         */
        private final Gaussian rand_returns = new Gaussian(rng, SEATSConstants.CUSTOMER_RETURN_FLIGHT_DAYS_MIN,
                                                                SEATSConstants.CUSTOMER_RETURN_FLIGHT_DAYS_MAX);
        
        private final LinkedBlockingDeque<Reservation> queue = new LinkedBlockingDeque<Reservation>(100);
        private Reservation current = null;
        private Throwable error = null;
        public int last_id=0;
        private int total;
        
        /**
         * Constructor
         * @param catalog_tbl
         * @param total
         */
        public ReservationIterable(int total) {
            // Special Columns: R_C_ID, R_F_ID, R_F_AL_ID, R_SEAT, R_PRICE
            this.total = total;
            for (int airport_id : profile.ap_code_2_apid.values()) {
                // Return Flights per airport
                this.airport_returns.put(airport_id, new TreeSet<ReturnFlight>());
            } // FOR
            
            // Data Generation Thread
            // Ok, hang on tight. We are going to fork off a separate thread to generate our
            // tuples because it's easier than trying to pick up where we left off every time
            // That means that when hasNext() is called, it will block and poke this thread to start
            // running. Once this thread has generate a new tuple, it will block itself and then
            // poke the hasNext() thread. This is sort of like a hacky version of Python's yield


            new Thread() { 
                public void run() {
                    try {
                        ReservationIterable.this.generateData();
                    } catch (Throwable ex) {
                        System.out.println("1111");
                        ReservationIterable.this.error = ex;
                    } finally {
                        ReservationIterable.this.done = true;
                    }
                } // run
            }.start();
        }
        
        private void generateData() throws Exception {
            
            Collection<Integer> flight_customer_ids = new HashSet<Integer>();
            Collection<ReturnFlight> returning_customers = new ListOrderedSet<ReturnFlight>();
            
            // Loop through the flights and generate reservations
            for (int flight_id = 0, cnt = profile.num_flights; flight_id < cnt; flight_id++) {
                FlightInfo flightInfo = SEATSLoader.this.flight_infos.remove(flight_id);
                String depart_airport = flightInfo.depart_airport;
                String arrive_airport = flightInfo.arrive_airport;
                Timestamp depart_time = flightInfo.depart_time;
                Timestamp arrive_time = flightInfo.arrive_time;
                flight_customer_ids.clear();
                
                // For each flight figure out which customers are returning
                this.getReturningCustomers(returning_customers, flightInfo);
                int booked_seats = SEATSConstants.FLIGHTS_NUM_SEATS - Math.max(SEATSConstants.FLIGHTS_RESERVED_SEATS,
                                                                               flightInfo.seats_remaining);
                for (int seatnum = 0; seatnum < booked_seats; seatnum++) {
                    Integer customer_id = null;
                    Integer airport_customer_cnt = profile.getCustomerIdCount(profile.getAirportId(depart_airport));
                    boolean local_customer = airport_customer_cnt != null && (flight_customer_ids.size() < airport_customer_cnt.intValue());
                    int tries = 2000;
                    ReturnFlight return_flight = null;
                    while (tries > 0) {
                        return_flight = null;
                        
                        // Always book returning customers first
                        if (returning_customers.isEmpty() == false) {
                            return_flight = CollectionUtil.pop(returning_customers); 
                            customer_id = return_flight.getCustomerId();
                        }
                        // New Outbound Reservation
                        // Prefer to use a customer based out of the local airport
                        else if (local_customer) {
                            customer_id = profile.getRandomCustomerId(); // depart_airport_id
                        }
                        // New Outbound Reservation
                        // We'll take anybody!
                        else {
                            customer_id = profile.getRandomCustomerId();
                        }
                        if (flight_customer_ids.contains(customer_id) == false) break;
                        tries--;
                    } // WHILE
                    assert(tries > 0) : String.format("Safety check! [local=%s]", local_customer);

                    // If this is return flight, then there's nothing extra that we need to do
                    if (return_flight != null) {
                    
                    // If it's a new outbound flight, then we will randomly decide when this customer will return (if at all)
                    } else {
                        if (rng.nextInt(100) < SEATSConstants.PROB_SINGLE_FLIGHT_RESERVATION) {
                            // Do nothing for now...
                            
                        // Create a ReturnFlight object to record that this customer needs a flight
                        // back to their original depart airport
                        } else {
                            int return_days = rand_returns.nextInt();
                            return_flight = new ReturnFlight(customer_id, profile.getAirportId(depart_airport), depart_time, return_days);
                            this.airport_returns.get(profile.ap_code_2_apid.get(arrive_airport)).add(return_flight);
                        }
                    }
                    assert(customer_id != null) : "Null customer id on " + flight_id;
                    assert(flight_customer_ids.contains(customer_id) == false) : flight_id + " already contains " + customer_id; 
                    flight_customer_ids.add(customer_id);
            
                    Reservation r = new Reservation(1001, flight_id, customer_id, seatnum); // id doesn't matter
                    this.queue.put(r);
                } // FOR (seats)
                
            } // FOR (flights)
        }
        
        /**
         * Return a list of the customers that need to return to their original
         * location on this particular flight.
         * @param flight_id
         * @return
         */
        private void getReturningCustomers(Collection<ReturnFlight> returning_customers, FlightInfo flightInfo) {
            Timestamp flight_date = flightInfo.depart_time;
            returning_customers.clear();
            Set<ReturnFlight> returns = this.airport_returns.get(profile.ap_code_2_apid.get(flightInfo.depart_airport));
            if (returns != null && !returns.isEmpty()) {
                for (ReturnFlight return_flight : returns) {
                    if (return_flight.getReturnDate().compareTo(flight_date) > 0) break;
                    if (return_flight.getReturnAirportId() == profile.getAirportId(flightInfo.arrive_airport)) {
                        returning_customers.add(return_flight);
                    }
                } // FOR
                if (!returning_customers.isEmpty()) returns.removeAll(returning_customers);
            } else if (returns == null) {
                LOG.warn(String.format("Null return flights for departing airport '%s'", flightInfo.depart_airport));
            }
        }
        
        protected boolean hasNext() {
            this.current = null;
            while (this.done == false || this.queue.isEmpty() == false) {
                if (this.error != null)
                    throw new RuntimeException("Failed to generate Reservation records", this.error);
                
                try {
                    this.current = this.queue.poll(100, TimeUnit.MILLISECONDS);
                } catch (InterruptedException ex) {
                    throw new RuntimeException("Unexpected interruption!", ex);
                }
                if (this.current != null) return (true);
            } // WHILE
            return (false);
        }
        
        public boolean doOne()
        {
        	if(!this.hasNext()) return false;
        	this.last_id++;
        	return true;
        }
        protected Object specialValue(long id, int columnIdx) {
            assert(this.current != null);
            Object value = null;
            switch (columnIdx) {
                // CUSTOMER ID
                case (1): {
                    value = this.current.customer_id;
                    break;
                }
                // FLIGHT ID
                case (2): {
                    value = this.current.flight_id;
//                    if (profile.getReservationUpcomingOffset() == null &&
//                        flight_id.isUpcoming(profile.getFlightStartDate(), profile.getFlightPastDays())) {
//                        profile.setReservationUpcomingOffset(id);
//                    }
                    break;
                }
                // SEAT
                case (3): {
                    value = this.current.seatnum;
                    break;
                }
                // PRICE
                case (4): {
                    value = (double)this.prices.nextInt();
                    break;
                }
                // BAD MOJO!
                default:
                    assert(false) : "Unexpected special column index " + columnIdx;
            } // SWITCH
            return (value);
        }
    } 
    
    
    // -----------------------------------------------------------------
    // FLIGHT IDS
    // -----------------------------------------------------------------
    
    
    /**
     * Return the number of unique flight ids
     * @return
     */
    public long getFlightIdCount() {
        return (this.seats_remaining.size());
    }
    
    /**
     * Return flight 
     * @param index
     * @return
     */
    public FlightId getFlightId(int index) {
        assert(index >= 0);
        assert(index <= this.getFlightIdCount());
        return (this.seats_remaining.get(index));
    }

    /**
     * Return the number of seats remaining for a flight
     * @param flight_id
     * @return
     */
    public int getFlightRemainingSeats(FlightId flight_id) {
        return ((int)this.seats_remaining.get(flight_id));
    }
    
    /**
     * Decrement the number of available seats for a flight and return
     * the total amount remaining
     */
    public int decrementFlightSeat(FlightId flight_id) {
        Short seats = this.seats_remaining.get(flight_id);
        assert(seats != null) : "Missing seat count for " + flight_id;
        assert(seats >= 0) : "Invalid seat count for " + flight_id;
        return ((int)this.seats_remaining.put(flight_id, (short)(seats - 1)));
    }
    
    // ----------------------------------------------------------------
    // DISTANCE METHODS
    // ----------------------------------------------------------------
    
    public void setDistance(String airport0, String airport1, double distance) {
        short short_distance = (short)Math.round(distance);
        for (String a[] : new String[][] { {airport0, airport1}, { airport1, airport0 }}) {
            if (!this.airport_distances.containsKey(a[0])) {
                this.airport_distances.put(a[0], new HashMap<String, Short>());
            }
            this.airport_distances.get(a[0]).put(a[1], short_distance);
        } // FOR
    }
    
    public Integer getDistance(String airport0, String airport1) {
        assert(this.airport_distances.containsKey(airport0)) : "No distance entries for '" + airport0 + "'";
        assert(this.airport_distances.get(airport0).containsKey(airport1)) : "No distance entries from '" + airport0 + "' to '" + airport1 + "'";
        return ((int)this.airport_distances.get(airport0).get(airport1));
    }

    /**
     * For the current depart+arrive airport destinations, calculate the estimated
     * flight time and then add the to the departure time in order to come up with the
     * expected arrival time.
     * @param depart_airport
     * @param arrive_airport
     * @param depart_time
     * @return
     */
    public Timestamp calculateArrivalTime(String depart_airport, String arrive_airport, Timestamp depart_time) {
        Integer distance = this.getDistance(depart_airport, arrive_airport);
        assert(distance != null) : String.format("The calculated distance between '%s' and '%s' is null", depart_airport, arrive_airport);
        long flight_time = Math.round(distance / SEATSConstants.FLIGHT_TRAVEL_RATE) * 3600000000l; // 60 sec * 60 min * 1,000,000
        return (new Timestamp(depart_time.getTime() + flight_time));
    }
    
    // public static void main(String [] args) throws IOException
    // {
    // 	Map<String,String> conf = new HashMap<String,String>();
    // 	conf.put("numWarehouses","2");
    // 	conf.put("numPartitions","1");
    // 	SeatsStorageMgr sm = new SeatsStorageMgr(0,conf);
    // 	SEATSLoader loader = new SEATSLoader(sm, new File("C:/workspace/owner_txn/src/main/java/sg/edu/nus/ownertxn/seats/data"));
    // 	loader.load();
    // }
}