package sg.edu.nus.ownertxn.seats.record;

public class AirportDistance extends sg.edu.nus.ownertxn.Tuple
{
    public AirportDistance() {}
    
    public int D_AP_ID0; //reference airport ap_id
    public int D_AP_ID1; // reference airport ap_id
    public double D_DISTANCE;

    public int size() {
        return 0;
    }

    @Override
    public String toString() {
        return "AirportDistance(" + "..." + ")";
    }
}