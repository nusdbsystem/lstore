package sg.edu.nus.ownertxn.seats.record;

public class Country extends sg.edu.nus.ownertxn.Tuple
{
    public Country() {
    }
    
    public int CO_ID;
    public String CO_NAME;
    public String CO_CODE2;
    public String CO_CODE3;
    public int size() {
        return 4+64+2+3;
    }

    @Override
    public String toString() {
        return "Country(" + "..." + ")";
    }
}