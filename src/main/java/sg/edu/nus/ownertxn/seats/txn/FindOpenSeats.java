package sg.edu.nus.ownertxn.seats.txn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections15.Buffer;

import sg.edu.nus.ownertxn.Tuple;
import sg.edu.nus.ownertxn.seats.SEATSConstants;
import sg.edu.nus.ownertxn.seats.SEATSPrimaryKey;
import sg.edu.nus.ownertxn.seats.SeatsStorageMgr;
import sg.edu.nus.ownertxn.seats.SeatsStorageMgr.SecondaryIndexType;
import sg.edu.nus.ownertxn.seats.SeatsTxnExecEngine;
import sg.edu.nus.ownertxn.seats.record.*;

public class FindOpenSeats extends AnySeatsTxn
{
    public FindOpenSeats(SeatsTxnExecEngine eng) {
        super(eng);
        eng.initTxn(txnId);
    }
    /**
     * Execute the FindOpenSeat procedure
     */
    private Object[] getFindOpenSeatsParams() {
        int flight_id = eng.profile.getRandomFlightId();
        int part = eng.storeMgr.startLocalWhseId;
        Object params[] = new Object[] {
            flight_id,
            part
        };
        return params;
    }
    public void run() {
        /* prepare */
        SeatsStorageMgr sm = eng.storeMgr;
        Object [] params  = this.getFindOpenSeatsParams();
        final List<sg.edu.nus.ownertxn.seats.util.Reservation> tmp_reservations = new ArrayList<sg.edu.nus.ownertxn.seats.util.Reservation>();
        /* execute */
        boolean gotOwner = false;
        eng.startTxn(txnId);
        int f_id = (int)params[0];
        final long seatmap[] = new long[SEATSConstants.FLIGHTS_NUM_SEATS];
        Arrays.fill(seatmap, -1);
        Flight flight = (Flight)eng.storeMgr.readTuple((int)params[1], SEATSConstants.TABLENAME_FLIGHT, 
        		SEATSPrimaryKey.flightKey((int)params[0]));
        Reservation [] reservation = (Reservation [])eng.storeMgr.readTuple((int)params[1], SEATSConstants.TABLENAME_RESERVATION,
        		(int)SEATSPrimaryKey.flightKey((int)params[0]),SecondaryIndexType.IDX_RESERVATION_F_ID);
        double base_price = flight.F_BASE_PRICE;
        long seats_total = flight.F_SEATS_TOTAL;
        long seats_left = flight.F_SEATS_LEFT;
        double seats_price = base_price + (base_price * (1.0 - (seats_left/(double)seats_total)));
        for(int i=0;i<reservation.length;i++)
        {
        	int r_id = reservation[i].R_ID;
        	int seatnum = (int)reservation[i].R_SEAT;
        	// assert(seatmap[seatnum]==-1):"duplicate seat reservation: R_ID=" + r_id;
            if (seatmap[seatnum] != -1) {
                eng.abortTxn(txnId);
                return;
            }
        	seatmap[seatnum]=1;
        }
        int ctr = 0;
        ArrayList<Object []> seatInfo = new ArrayList<Object []>();
        for(int i=0; i < seatmap.length;i++)
        {
        	if(seatmap[i]==-1)
        	{
        		seatInfo.add(new Object[]{f_id,i,seats_price});
        		if (ctr == SEATSConstants.FLIGHTS_NUM_SEATS) break;
        	}
        }
        int rowCount = seatInfo.size();
        // assert(rowCount!=0);
        if (rowCount == 0) {
            eng.abortTxn(txnId);
            return;
        }
        BitSet seats = eng.getSeatsBitSet(f_id);
        for(int i=0;i<seatInfo.size();i++)
        {
        	int seatnum = (int)seatInfo.get(i)[1];
        	if(seatnum < SEATSConstants.FLIGHTS_RESERVED_SEATS)
        		continue;
        	int customer_id = eng.profile.getRandomCustomerId();
        	sg.edu.nus.ownertxn.seats.util.Reservation r = new sg.edu.nus.ownertxn.seats.util.Reservation(
        			eng.profile.getNextReservationId(eng.storeMgr.myId)
        			,f_id
        			,customer_id
        			,seatnum);
        	tmp_reservations.add(r);
            seats.set(seatnum);
        }
        if(tmp_reservations.isEmpty() == false)
        {
        	Buffer<sg.edu.nus.ownertxn.seats.util.Reservation> cache = eng.CACHE_RESERVATIONS.get(SeatsTxnExecEngine.CacheType.PENDING_INSERTS);
        	// assert(cache != null) : "Unexpected " + SeatsTxnExecEngine.CacheType.PENDING_INSERTS;
            if (cache == null) {
                eng.abortTxn(txnId);
                return;
            }
        	 Collections.shuffle(tmp_reservations);
        	 synchronized (cache) {
                 cache.addAll(tmp_reservations);
             } // SYNCH
        }
        eng.commitTxn(txnId);
    }
}
