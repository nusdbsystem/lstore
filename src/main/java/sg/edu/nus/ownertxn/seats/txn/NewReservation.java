package sg.edu.nus.ownertxn.seats.txn;

import java.sql.Timestamp;
import java.util.BitSet;

import org.apache.commons.collections15.Buffer;

import sg.edu.nus.ownertxn.seats.SEATSConstants;
import sg.edu.nus.ownertxn.seats.SEATSPrimaryKey;
import sg.edu.nus.ownertxn.seats.SeatsStorageMgr;
import sg.edu.nus.ownertxn.seats.SeatsTxnExecEngine;
import sg.edu.nus.ownertxn.seats.SeatsStorageMgr.SecondaryIndexType;
import sg.edu.nus.ownertxn.seats.record.*;

public class NewReservation extends AnySeatsTxn
{
    public NewReservation(SeatsTxnExecEngine eng) {
        super(eng);
        eng.initTxn(txnId);
    }
    /**
     * Execute the FindOpenSeat procedure
     */
    private Object[] getNewReservationParams() {
        sg.edu.nus.ownertxn.seats.util.Reservation reservation = null;
        BitSet seats = null;
        Buffer<sg.edu.nus.ownertxn.seats.util.Reservation> cache = eng.CACHE_RESERVATIONS.get(SeatsTxnExecEngine.CacheType.PENDING_INSERTS);
        assert(cache != null) : "Unexpected " + SeatsTxnExecEngine.CacheType.PENDING_INSERTS;
        while(reservation == null)
        {
        	sg.edu.nus.ownertxn.seats.util.Reservation r = null;
        	synchronized(cache)
        	{
        		if(cache.isEmpty()==false)r = cache.remove();
        	}
        	if(r == null)
        	{
                break;
        	}
        	seats = eng.getSeatsBitSet(r.flight_id);
        	if (eng.isFlightFull(seats)) {
                continue;
            }
        	else if (eng.isCustomerBookedOnFlight(r.customer_id, r.flight_id)) {
                continue;
            }
        	reservation = r;
        }
        if (reservation == null) {
            return (null);
        }
     // Generate a random price for now
        double price = 2.0 * rng.number(SEATSConstants.RESERVATION_PRICE_MIN,
                                        SEATSConstants.RESERVATION_PRICE_MAX);
        
        // Generate random attributes
        int attributes[] = new int[SEATSConstants.NEW_RESERVATION_ATTRS_SIZE];
        for (int i = 0; i < attributes.length; i++) {
            attributes[i] = rng.nextInt();
        } // FOR
        int part = eng.storeMgr.startLocalWhseId;
        Object params[] = new Object[] {
        		part, //warehouse
        		reservation.id, //r_id
                reservation.customer_id, //c_id
                reservation.flight_id, //f_id
                reservation.seatnum, //seatnum
                price, //price
                attributes, //attrs[]
                new Timestamp(System.currentTimeMillis()), //timestamp
                reservation
        };
        return params;
    }

    public void run() {
        /* prepare */
        SeatsStorageMgr sm = eng.storeMgr;
        Object [] params  = this.getNewReservationParams();
        /* execute */
        boolean gotOwner = false;
        eng.startTxn(txnId);
        
        int partitonNum = (int)params[0];
        int r_id = (int)params[1];
        int c_id = (int)params[2];
        int f_id = (int)params[3];
        long seatnum = (long)params[4];
        double price = (double)params[5];
        sg.edu.nus.ownertxn.seats.util.Reservation reserve = (sg.edu.nus.ownertxn.seats.util.Reservation)params[6];
        /*
        Flight flight = (Flight)sm.readTuple(partitonNum, SEATSConstants.TABLENAME_FLIGHT, SEATSPrimaryKey.flightKey(f_id));
        Reservation [] reservations = (Reservation []) sm.readTuple(partitonNum, SEATSConstants.TABLENAME_RESERVATION, 
        		f_id, SecondaryIndexType.IDX_RESERVATION_F_ID);
        Reservation [] reservations1 = (Reservation []) sm.readTuple(partitonNum, SEATSConstants.TABLENAME_RESERVATION, 
        		c_id, SecondaryIndexType.IDX_RESERVATION_C_ID);
        if(flight == null){
            eng.abortTxn(txnId);
            return;
        }
        int airline_id = flight.F_AL_ID;
        long seats_left = flight.F_SEATS_LEFT;
        if(seats_left<=0) {
            eng.abortTxn(txnId);
            return;
        }
        boolean same = false;
        for(Reservation i : reservations)
        {
        	for(Reservation j: reservations1)
        	{
        		if(i.R_F_ID == j.R_F_ID)
        			break;
        	}
        	if(same)break;
        }
        if(same) {
            eng.abortTxn(txnId);
            return;
        }
        */
        /*
        Customer customer = (Customer)sm.readTuple(partitonNum, SEATSConstants.TABLENAME_CUSTOMER, SEATSPrimaryKey.customerKey(c_id));
        if(customer==null) {
            eng.abortTxn(txnId);
            return;
        }
        Reservation reservation = new Reservation();
        reservation.R_ID = r_id;
        reservation.R_C_ID = c_id;
        reservation.R_F_ID = f_id;
        reservation.R_SEAT = seatnum;
        reservation.R_PRICE = price;
        sm.insertTuple(partitonNum, SEATSConstants.TABLENAME_RESERVATION, SEATSPrimaryKey.reservationKey(r_id, c_id, f_id), reservation);
        flight.F_SEATS_LEFT = flight.F_SEATS_LEFT - 1;
        sm.updateTuple(partitonNum, SEATSConstants.TABLENAME_FLIGHT, SEATSPrimaryKey.flightKey(f_id),flight);
        customer.C_IATTR10 = customer.C_IATTR10 + 1;
        customer.C_IATTR11 = customer.C_IATTR11 + 1;
        sm.updateTuple(partitonNum, SEATSConstants.TABLENAME_CUSTOMER, SEATSPrimaryKey.customerKey(c_id), customer);
        
        BitSet seats = eng.getSeatsBitSet(f_id);
        seats.set((int)seatnum);
        eng.requeueReservation(reserve);
        */
        eng.commitTxn(txnId);
    }
}
