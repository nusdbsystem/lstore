package sg.edu.nus.ownertxn.seats.txn;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import sg.edu.nus.ownertxn.seats.SEATSConstants;
import sg.edu.nus.ownertxn.seats.SeatsStorageMgr;
import sg.edu.nus.ownertxn.seats.SeatsStorageMgr.SecondaryIndexType;
import sg.edu.nus.ownertxn.seats.SeatsTxnExecEngine;
import sg.edu.nus.ownertxn.seats.record.*;

public class FindFlights extends AnySeatsTxn
{
    public FindFlights(SeatsTxnExecEngine eng) {
        super(eng);
        eng.initTxn(txnId);
    }

    /**
     * Execute one of the FindFlight transactions
     */
    private Object[] getFindFlightsParams() {

        int depart_airport_id = eng.profile.getRandomAirportId();
        int arrive_airport_id = eng.profile.getRandomOtherAirport(depart_airport_id);

        Timestamp start_date = eng.profile.getRandomUpcomingDate();
        Timestamp stop_date = new Timestamp(start_date.getTime() + (SEATSConstants.MICROSECONDS_PER_DAY * 2));
        
        // If distance is greater than zero, then we will also get flights from nearby airports
        long distance = -1;
        if (rng.nextInt(100) < SEATSConstants.PROB_FIND_FLIGHTS_NEARBY_AIRPORT) {
            distance = SEATSConstants.DISTANCES[rng.nextInt(SEATSConstants.DISTANCES.length)];
        }
        int part = eng.storeMgr.startLocalWhseId;
        Object params[] = new Object[] {
        	part,
            depart_airport_id,
            arrive_airport_id,
            start_date,
            stop_date,
            distance
        };
        return params;
    }
    public void run() {
        /* prepare */
        SeatsStorageMgr sm = eng.storeMgr;

        /* execute */
        boolean gotOwner = false;
        eng.startTxn(txnId);
        Object [] params  = this.getFindFlightsParams();
        int partNum = (int) params[0];
        int depart_aid = (int) params[1];
        int arrive_aid = (int) params[2];
        Timestamp start_date = (Timestamp) params[3];
        Timestamp end_date = (Timestamp) params[4];
        long distance = (long) params[5];
        final List<Integer> arrive_aids = new ArrayList<Integer>();
        arrive_aids.add(arrive_aid);
        
        if(distance>0)
        {
        	//SQL GetNearbyAirport depart_aid distance
        	AirportDistance [] airportDistances = (AirportDistance []) sm.readTuple(partNum, SEATSConstants.TABLENAME_AIRPORT_DISTANCE, depart_aid, SecondaryIndexType.IDX_DISTANCE);
        	ArrayList<AirportDistance> airportDistancesList = new ArrayList<AirportDistance>();
        	for(int i=0;i<airportDistances.length;i++)
        	{
        		airportDistancesList.add(airportDistances[i]);
        	}
        	Collections.sort(airportDistancesList,new Comparator<AirportDistance>(){

				@Override
				public int compare(AirportDistance arg0, AirportDistance arg1) {
					// TODO Auto-generated method stub
					return Integer.valueOf((int) (arg0.D_DISTANCE - arg1.D_DISTANCE));
				}});
        	for(AirportDistance iter: airportDistancesList)
        	{
        		arrive_aids.add(iter.D_AP_ID1);
        	}
        }
        
        int num_nearby = arrive_aids.size();
        if(num_nearby>0)
        {
        	if(num_nearby==1)
        	{
        		
        	}
        }
        eng.commitTxn(txnId);
    }
}
