package sg.edu.nus.ownertxn.seats;

public class SEATSPrimaryKey {
	public static final long tableMask = 0x000ff00000000000L;
    public static final long partitionMask = 0xfff0000000000000L;
    public static final long keyMove = 0x00000fffffffffffL;
    public static final int tablemovebit = 44;
    public static final int partitionmovebit = 52;
    public static int numWhsePerPartition = 0;
    
    public static int countryKey(int co_id)
    {
    	return co_id;
    }
    public static int airportKey(int ap_id)
    {
    	return ap_id;
    }
    public static int airport_distanceKey(int d_ap_id0,int d_ap_id1)
    {
    	return d_ap_id0*10000+d_ap_id1;
    }
    public static int airlineKey(int al_id)
    {
    	return al_id;
    }
    public static int customerKey(int c_id)
    {
    	return c_id;
    }
    public static int frequent_flyerKey(int ff_c_id, int ff_al_id)
    {
    	return ff_al_id*SEATSConstants.CUSTOMERS_COUNT+ff_c_id;
    }
    public static int flightKey(int f_id)
    {
    	return f_id;
    }
    public static int reservationKey(int r_id,int r_c_id,int r_f_id)
    {
    	return r_id;
    }
    public static String getTable(Long key) {
        Integer mask = Long
                .valueOf(
                        (key & tableMask) >> tablemovebit)
                .intValue();
        switch (mask) {
        case 1:
            return "COUNTRY";
        case 2:
            return "AIRLINE";
        case 3:
            return "CUSTOMER";
        case 4:
            return "FREQUENT_FLYER";
        case 5:
            return "AIRPORT";
        case 6:
            return "AIRPORT_DISTANCE";
        case 7:
            return "FLIGHT";
        case 8:
            return "RESERVATION";
        case 9:
            return "CONFIG_PROFILE";
        case 10:
        	return "CONFIG_HISTOGRAMS";
        }
        return null;
    }

    public static Integer getKeyWOTableWOpartition(Long key) {
        return Long.valueOf(key & keyMove).intValue();
    }

    public static Integer getRealPartition(Long key) {
        return (getPartition(key) - 1) / numWhsePerPartition;
    }

    public static Integer getPartition(Long key) {
        return Long.valueOf((key & partitionMask) >> partitionmovebit)
                .intValue();
    }

    public static Long getKeyWTTableWTparition(String table, Integer key,
            Long partition) {
        switch (table) {
        case "COUNTRY":
            return ((partition << partitionmovebit) | (1L << tablemovebit) | key);
        case "AIRLINE":
            return ((partition << partitionmovebit) | (2L << tablemovebit) | key);
        case "CUSTOMER":
            return ((partition << partitionmovebit) | (3L << tablemovebit) | key);
        case "FREQUENT_FLYER":
            return ((partition << partitionmovebit) | (4L << tablemovebit) | key);
        case "AIRPORT":
            return ((partition << partitionmovebit) | (5L << tablemovebit) | key);
        case "AIRPORT_DISTANCE":
            return ((partition << partitionmovebit) | (6L << tablemovebit) | key);
        case "FLIGHT":
            return ((partition << partitionmovebit) | (7L << tablemovebit) | key);
        case "RESERVATION":
            return ((partition << partitionmovebit) | (8L << tablemovebit) | key);
        case "CONFIG_PROFILE":
            return ((partition << partitionmovebit) | (9L << tablemovebit) | key);
        case "CONFIG_HISTOGRAMS":
            return ((partition << partitionmovebit) | (10L << tablemovebit) | key);
        }
        return -1L;
    }
}
