package sg.edu.nus.ownertxn.seats;

import java.util.BitSet;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import static qlib.Logger.*;

import org.apache.commons.collections15.Buffer;
import org.apache.commons.collections15.BufferUtils;
import org.apache.commons.collections15.buffer.CircularFifoBuffer;

import qlib.RichRandom;
import sg.edu.nus.ownertxn.TxnExecEngine;
import sg.edu.nus.ownertxn.TxnSvr;
import sg.edu.nus.ownertxn.seats.txn.*;
import sg.edu.nus.ownertxn.seats.util.Reservation;
import sg.edu.nus.ownertxn.util.RandomGenerator;

public class SeatsTxnExecEngine extends TxnExecEngine
{
    public final SeatsStorageMgr storeMgr;
    
    private AtomicBoolean first = new AtomicBoolean(true);
    
    private RandomGenerator ran = new RandomGenerator((int) System.currentTimeMillis());
    public final SEATSProfile profile;

    public SeatsTxnExecEngine(TxnSvr txnSvr) {
        super(txnSvr);
        storeMgr = (SeatsStorageMgr) txnSvr.getStoreMgr();
        this.profile = new SEATSProfile(ran, null);
    }
    // -----------------------------------------------------------------
    // RESERVED SEAT BITMAPS
    // -----------------------------------------------------------------
    
    public enum CacheType {
        PENDING_INSERTS     (SEATSConstants.CACHE_LIMIT_PENDING_INSERTS),
        PENDING_UPDATES     (SEATSConstants.CACHE_LIMIT_PENDING_UPDATES),
        PENDING_DELETES     (SEATSConstants.CACHE_LIMIT_PENDING_DELETES),
        ;
        
        private CacheType(int limit) {
            this.limit = limit;
        }
        private final int limit;
    }
    public static final Map<CacheType, Buffer<Reservation>> CACHE_RESERVATIONS = new EnumMap<CacheType, Buffer<Reservation>>(CacheType.class);
    private static final Map<Integer, Set<Integer>> CACHE_CUSTOMER_BOOKED_FLIGHTS = new ConcurrentHashMap<Integer, Set<Integer>>();
    private static final Map<Integer, BitSet> CACHE_BOOKED_SEATS = new ConcurrentHashMap<Integer, BitSet>();
    private static final BitSet FULL_FLIGHT_BITSET = new BitSet(SEATSConstants.FLIGHTS_NUM_SEATS);
    static {
        FULL_FLIGHT_BITSET.set(0, SEATSConstants.FLIGHTS_NUM_SEATS);
        for (CacheType ctype : CacheType.values()) {
            Buffer<Reservation> buffer = BufferUtils.synchronizedBuffer(new CircularFifoBuffer<Reservation>(ctype.limit));
            CACHE_RESERVATIONS.put(ctype, buffer);
        } // FOR
    } // STATIC
    
    /**
     * Take an existing Reservation that we know is legit and randomly decide to 
     * either queue it for a later update or delete transaction 
     * @param r
     */
    public void requeueReservation(Reservation r) {
		int idx = ran.nextInt(100);
        if (idx > 20) return;
        
        // Queue this motha trucka up for a deletin' or an updatin'
        CacheType ctype = null;
        if (ran.nextBoolean()) {
            ctype = CacheType.PENDING_DELETES;
        } else {
            ctype = CacheType.PENDING_UPDATES;
        }
        assert(ctype != null);
        
        Buffer<Reservation> cache = CACHE_RESERVATIONS.get(ctype);
        assert(cache != null);
        cache.add(r);
    }
    /**
     * Returns true if the given BitSet for a Flight has all of its seats reserved 
     * @param seats
     * @return
     */
    public boolean isFlightFull(BitSet seats) {
        assert(FULL_FLIGHT_BITSET.size() == seats.size());
        return FULL_FLIGHT_BITSET.equals(seats);
    }
    /**
     * Returns true if the given Customer already has a reservation booked on the target Flight
     * @param customer_id
     * @param flight_id
     * @return
     */
    public boolean isCustomerBookedOnFlight(int customer_id, int flight_id) {
        Set<Integer> flights = CACHE_CUSTOMER_BOOKED_FLIGHTS.get(customer_id);
        return (flights != null && flights.contains(flight_id));
    }
    public final Set<Integer> getCustomerBookedFlights(int customer_id) {
        Set<Integer> f_ids = CACHE_CUSTOMER_BOOKED_FLIGHTS.get(customer_id);
        if (f_ids == null) {
            f_ids = new HashSet<Integer>();
            CACHE_CUSTOMER_BOOKED_FLIGHTS.put(customer_id, f_ids);
        }
        return (f_ids);
    }
    protected final void clearCache() {
        for (BitSet seats : CACHE_BOOKED_SEATS.values()) {
            seats.clear();
        } // FOR
        for (Buffer<Reservation> queue : CACHE_RESERVATIONS.values()) {
            queue.clear();
        } // FOR
        for (Set<Integer> f_ids : CACHE_CUSTOMER_BOOKED_FLIGHTS.values()) {
            synchronized (f_ids) {
                f_ids.clear();
            } // SYNCH
        } // FOR
    }
    public BitSet getSeatsBitSet(int flight_id) {
        BitSet seats = CACHE_BOOKED_SEATS.get(flight_id);
        if (seats == null) {
//            synchronized (CACHE_BOOKED_SEATS) {
                seats = CACHE_BOOKED_SEATS.get(flight_id);
                if (seats == null) {
                    seats = new BitSet(SEATSConstants.FLIGHTS_NUM_SEATS);
                    CACHE_BOOKED_SEATS.put(flight_id, seats);
                }
//            } // SYNCH
        }
        return (seats);
    }
    @Override
    protected void prepare() {}

    @Override
    protected void migrate() {}

    @Override
    protected void execute() throws Exception {
        RichRandom rand = new RichRandom();

        tk.start();
        long tickMillis;
        while ((tickMillis = tk.waitUntilNextTick()) > 0) {
            for (int i = 0; i < batchSizePerTick; ++i) {
                int choice = rand.nextInt(100);
                if (choice < 65) {
                    threadPool.execute(new FindOpenSeats(this));
                }
                else if (choice < 90) {
                    threadPool.execute(new NewReservation(this));
                }
                else if (choice < 93) {
                    threadPool.execute(new FindFlights(this));
                }
                else if (choice < 95) {
                    threadPool.execute(new DeleteReservation(this));
                }
                else if (choice < 98) {
                    threadPool.execute(new UpdateReservation(this));
                }
                else if (choice < 100) {
                    threadPool.execute(new UpdateCustomer(this));
                }
                else {
                    warn("dice overflow: " + choice);
                }

                // if (choice < 10) { // 10%
                //     threadPool.execute(new DeleteReservation(this));
                // }
                // else if (choice < 20) { // 10%
                //     threadPool.execute(new FindFlights(this));
                // }
                // else if (choice < 55) { // 35%
                //     threadPool.execute(new FindOpenSeats(this));
                // }
                // else if (choice < 75) { // 20%
                //     threadPool.execute(new NewReservation(this));
                // }
                // else if (choice < 85) { // 10%
                //     threadPool.execute(new UpdateCustomer(this));
                // }
                // else if (choice < 100) { // 15%
                //     threadPool.execute(new UpdateReservation(this));
                // }
                // else {
                //     warn("dice overflow: " + choice);
                // }
            }
        }
    }
}
