package sg.edu.nus.ownertxn.seats.txn;

import java.util.BitSet;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.collections15.Buffer;
import org.apache.commons.collections15.BufferUtils;
import org.apache.commons.collections15.buffer.CircularFifoBuffer;

import sg.edu.nus.ownertxn.seats.SEATSConstants;
import sg.edu.nus.ownertxn.seats.SeatsTxnExecEngine;
import sg.edu.nus.ownertxn.seats.util.Reservation;
import sg.edu.nus.ownertxn.util.RandomGenerator;
public abstract class AnySeatsTxn implements Runnable
{
    protected final SeatsTxnExecEngine eng;
    public final Long txnId;
    RandomGenerator rng = new RandomGenerator((int) System.currentTimeMillis());

    public AnySeatsTxn(SeatsTxnExecEngine eng) {
        this.eng = eng;
        txnId = eng.genTxnId();
    }
}