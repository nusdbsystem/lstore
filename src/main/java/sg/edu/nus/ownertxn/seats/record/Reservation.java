package sg.edu.nus.ownertxn.seats.record;

public class Reservation extends sg.edu.nus.ownertxn.Tuple
{
    public Reservation() {}
    
    public int R_ID;
    public int R_C_ID; //reference customer c_id
    public int R_F_ID; //reference flight f_id
    public long R_SEAT; 
    public double R_PRICE;
    long R_IATTR00;
    long R_IATTR01;
    long R_IATTR02;
    long R_IATTR03;
    long R_IATTR04;
    long R_IATTR05;
    long R_IATTR06;
    long R_IATTR07;
    long R_IATTR08;
    public int size() {
        return 0;
    }

    @Override
    public String toString() {
        return "Reservation(" + "..." + ")";
    }
}