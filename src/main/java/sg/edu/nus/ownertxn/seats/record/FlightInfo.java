package sg.edu.nus.ownertxn.seats.record;

public class FlightInfo extends sg.edu.nus.ownertxn.Tuple{
	public FlightInfo() {}
	
	public FlightInfo(FlightInfo tmp){
		this.F_ID = tmp.F_ID;
		this.F_AL_ID = tmp.F_AL_ID;
		this.F_DEPART_AP_ID = tmp.F_DEPART_AP_ID;
		this.F_DEPART_TIME = tmp.F_DEPART_TIME;
		this.F_ARRIVE_AP_ID = tmp.F_ARRIVE_AP_ID;
		this.F_ARRIVE_TIME = tmp.F_ARRIVE_TIME;
		this.F_SEATS_TOTAL = tmp.F_SEATS_TOTAL;
	}
	public int F_ID;
	public int F_AL_ID;
	public int F_DEPART_AP_ID;
	public String F_DEPART_TIME;
	public int F_ARRIVE_AP_ID;
	public String F_ARRIVE_TIME;
	public long F_SEATS_TOTAL;
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}
	
    @Override
    public String toString() {
        return "Flight(" + "..." + ")";
    }
}
