package sg.edu.nus.ownertxn.seats.record;

public class Customer extends sg.edu.nus.ownertxn.Tuple
{
    public Customer() {}
    
    public    int C_ID;
    public    String C_ID_STR;
    public    int C_BASE_AP_ID; //reference airport ap_id
    public    double C_BALANCE;
    public    String C_SATTR00;
    public    String C_SATTR01;
    public    String C_SATTR02;
    public    String C_SATTR03;
    public    String C_SATTR04;
    public    String C_SATTR05;
    public    String C_SATTR06;
    public    String C_SATTR07;
    public    String C_SATTR08;
    public    String C_SATTR09;
    public    String C_SATTR10;
    public    String C_SATTR11;
    public    String C_SATTR12;
    public    String C_SATTR13;
    public    String C_SATTR14;
    public    String C_SATTR15;
    public    String C_SATTR16;
    public    String C_SATTR17;
    public    String C_SATTR18;
    public    String C_SATTR19;
    public    long C_IATTR00;
    public    long C_IATTR01;
    public    long C_IATTR02;
    public    long C_IATTR03;
    public    long C_IATTR04;
    public    long C_IATTR05;
    public    long C_IATTR06;
    public    long C_IATTR07;
    public    long C_IATTR08;
    public    long C_IATTR09;
    public    long C_IATTR10;
    public    long C_IATTR11;
    public    long C_IATTR12;
    public    long C_IATTR13;
    public    long C_IATTR14;
    public    long C_IATTR15;
    public    long C_IATTR16;
    public    long C_IATTR17;
    public    long C_IATTR18;
    public    long C_IATTR19;
    public int size() {
        return 0;
    }

    @Override
    public String toString() {
        return "Customer(" + "..." + ")";
    }
}