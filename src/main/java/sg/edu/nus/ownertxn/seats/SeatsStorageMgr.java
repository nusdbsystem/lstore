package sg.edu.nus.ownertxn.seats;

import static java.util.Collections.synchronizedMap;
import static sg.edu.nus.ownertxn.seats.SEATSConstants.TABLENAME_COUNTRY;
import static sg.edu.nus.ownertxn.seats.SEATSConstants.TABLENAME_AIRLINE;
import static sg.edu.nus.ownertxn.seats.SEATSConstants.TABLENAME_CUSTOMER;
import static sg.edu.nus.ownertxn.seats.SEATSConstants.TABLENAME_FREQUENT_FLYER;
import static sg.edu.nus.ownertxn.seats.SEATSConstants.TABLENAME_AIRPORT;
import static sg.edu.nus.ownertxn.seats.SEATSConstants.TABLENAME_AIRPORT_DISTANCE;
import static sg.edu.nus.ownertxn.seats.SEATSConstants.TABLENAME_FLIGHT;
import static sg.edu.nus.ownertxn.seats.SEATSConstants.TABLENAME_FLIGHT_INFO;
import static sg.edu.nus.ownertxn.seats.SEATSConstants.TABLENAME_RESERVATION;
import static sg.edu.nus.ownertxn.seats.SEATSConstants.TABLENAME_CONFIG_PROFILE;
import static sg.edu.nus.ownertxn.seats.SEATSConstants.TABLENAME_CONFIG_HISTOGRAMS;
import static sg.edu.nus.ownertxn.seats.SEATSPrimaryKey.getKeyWOTableWOpartition;
import static sg.edu.nus.ownertxn.seats.SEATSPrimaryKey.getPartition;
import static sg.edu.nus.ownertxn.seats.SEATSPrimaryKey.getRealPartition;
import static sg.edu.nus.ownertxn.seats.SEATSPrimaryKey.getTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sg.edu.nus.ownertxn.Tuple;
import sg.edu.nus.ownertxn.seats.record.AirportDistance;
import sg.edu.nus.ownertxn.seats.record.Customer;
import sg.edu.nus.ownertxn.seats.record.Flight;
import sg.edu.nus.ownertxn.seats.record.FlightInfo;
import sg.edu.nus.ownertxn.seats.record.FrequentFlyer;
import sg.edu.nus.ownertxn.seats.record.Reservation;

public class SeatsStorageMgr extends sg.edu.nus.ownertxn.StorageMgr
{
	public final int startLocalWhseId;
    public final int endLocalWhseId;
    public enum SecondaryIndexType{
    	IDX_DISTANCE (0),
    	IDX_FF_CUSTOMER_ID_STR (1), //string
    	IDX_FF_CUSTOMER_ID (2),
    	IDX_F_DEPART_TIME (3), //string
    	IDX_FLIGHT_INFO_DEPART (4),
    	IDX_RESERVATION_F_ID (5),
    	IDX_RESERVATION_C_ID (6),
    	IDX_C_C_ID_STR (7); //string

    	public int index;
    	SecondaryIndexType(int i){
    		this.index = i;
    	}
    }
    private class SecondaryIndex<T>{
    	private Map<T,List<Integer>> map;
    	SecondaryIndex()
    	{
    		map = new HashMap<T,List<Integer>>();
    	}
    	public void put(T key,Integer value)
    	{
    		if(this.map.get(key)==null)
    		{
    			this.map.put(key, new ArrayList<Integer>());
    		}
    		this.map.get(key).add(value);
    	}
    	public List<Integer> get(T key)
    	{
    		return this.map.get(key);
    	}
    }
    public List<List<SecondaryIndex>> secondaryIndex;
    public SeatsStorageMgr(int myId, Map<String, String> conf) {
        super(myId, conf);
        secondaryIndex = new ArrayList<List<SecondaryIndex>>();
        for(int i=0;i < numTotalWhse;++i)
        {
        	secondaryIndex.add(i,new ArrayList<SecondaryIndex>());
        	for(SecondaryIndexType iter : SecondaryIndexType.values())
        	{
        		if(iter == SecondaryIndexType.IDX_FF_CUSTOMER_ID_STR || iter == SecondaryIndexType.IDX_F_DEPART_TIME||iter == SecondaryIndexType.IDX_C_C_ID_STR)
        			secondaryIndex.get(i).add(iter.index,new SecondaryIndex<String>());
        		else
        			secondaryIndex.get(i).add(iter.index,new SecondaryIndex<Integer>());
        	}
        }
        for (int i = 0; i < numTotalWhse; ++i) {
            data.get(i).put(TABLENAME_COUNTRY,
                    synchronizedMap(new HashMap<Integer, Tuple>()));
            data.get(i).put(TABLENAME_AIRLINE,
                    synchronizedMap(new HashMap<Integer, Tuple>()));
            data.get(i).put(TABLENAME_CUSTOMER,
                    synchronizedMap(new HashMap<Integer, Tuple>()));
            data.get(i).put(TABLENAME_FREQUENT_FLYER,
                    synchronizedMap(new HashMap<Integer, Tuple>()));
            data.get(i).put(TABLENAME_AIRPORT,
                    synchronizedMap(new HashMap<Integer, Tuple>()));
            data.get(i).put(TABLENAME_AIRPORT_DISTANCE,
                    synchronizedMap(new HashMap<Integer, Tuple>()));
            data.get(i).put(TABLENAME_FLIGHT,
                    synchronizedMap(new HashMap<Integer, Tuple>()));
            data.get(i).put(TABLENAME_FLIGHT_INFO, 
            		synchronizedMap(new HashMap<Integer, Tuple>()));
            data.get(i).put(TABLENAME_RESERVATION,
                    synchronizedMap(new HashMap<Integer, Tuple>()));
            data.get(i).put(TABLENAME_CONFIG_PROFILE,
                    synchronizedMap(new HashMap<Integer, Tuple>()));
            data.get(i).put(TABLENAME_CONFIG_HISTOGRAMS,
                    synchronizedMap(new HashMap<Integer, Tuple>()));
        }

        startLocalWhseId = myId * numWhsePerPartition + 1;
        endLocalWhseId = (myId + 1) * numWhsePerPartition;
        SEATSPrimaryKey.numWhsePerPartition = numWhsePerPartition;
    }

    public void insertTuple(Long key, Tuple values) {
        Integer partitionNum = getPartition(key);
        Integer tableKey = getKeyWOTableWOpartition(key);
        String tableName = getTable(key);
        insertTuple(partitionNum, tableName, tableKey, values);
    }

    @SuppressWarnings("unchecked")
	public void insertTuple(Integer partitionNum, String tableName, int l,
            Tuple values) {
    	switch(tableName)
    	{
    	case SEATSConstants.TABLENAME_AIRPORT_DISTANCE:
    		this.secondaryIndex.get(partitionNum-1).get(SecondaryIndexType.IDX_DISTANCE.index)
    		.put(((AirportDistance)values).D_AP_ID0, l);
    		break;
    	case SEATSConstants.TABLENAME_FREQUENT_FLYER:
    		this.secondaryIndex.get(partitionNum-1).get(SecondaryIndexType.IDX_FF_CUSTOMER_ID_STR.index)
    		.put(((FrequentFlyer)values).FF_C_ID_STR, l);
    		this.secondaryIndex.get(partitionNum-1).get(SecondaryIndexType.IDX_FF_CUSTOMER_ID.index)
    		.put(((FrequentFlyer)values).FF_C_ID, l);
    		break;
    	case SEATSConstants.TABLENAME_FLIGHT:
    		this.secondaryIndex.get(partitionNum-1).get(SecondaryIndexType.IDX_F_DEPART_TIME.index)
    		.put(((Flight)values).F_DEPART_TIME, l);
    		break;
    	case SEATSConstants.TABLENAME_FLIGHT_INFO:
    		this.secondaryIndex.get(partitionNum-1).get(SecondaryIndexType.IDX_FLIGHT_INFO_DEPART.index)
    		.put(((FlightInfo)values).F_DEPART_AP_ID, l);
    		break;
    	case SEATSConstants.TABLENAME_RESERVATION:
    		this.secondaryIndex.get(partitionNum-1).get(SecondaryIndexType.IDX_RESERVATION_F_ID.index)
    		.put(((Reservation)values).R_F_ID, l);
    		this.secondaryIndex.get(partitionNum-1).get(SecondaryIndexType.IDX_RESERVATION_C_ID.index)
    		.put(((Reservation)values).R_C_ID, l);
    		break;
    	case SEATSConstants.TABLENAME_CUSTOMER:
    		this.secondaryIndex.get(partitionNum-1).get(SecondaryIndexType.IDX_C_C_ID_STR.index)
    		.put(((Customer)values).C_ID_STR, l);
    	}
        data.get(partitionNum - 1).get(tableName).put(l, values);
    }

    public void deleteTuple(Long key) {
        Integer partitionNum = getPartition(key);
        Integer tableKey = getKeyWOTableWOpartition(key);
        String tableName = getTable(key);
        deleteTuple(partitionNum, tableName, tableKey);
    }

    public void deleteTuple(Integer partitionNum, String tableName, Integer key) {
        data.get(partitionNum - 1).get(tableName).remove(key);
    }

    public Tuple readTuple(Long key) {
        Integer partitionNum = getPartition(key);
        Integer tableKey = getKeyWOTableWOpartition(key);
        String tableName = getTable(key);
        return readTuple(partitionNum, tableName, tableKey);
    }
    public Tuple readTuple(Integer partitonNum, String tableName, Integer key)
    {
    	return data.get(partitonNum - 1).get(tableName).get(key);
    }
    public Tuple [] readTuple(Integer partitonNum, String tableName, Integer key, SecondaryIndexType index) {
    	@SuppressWarnings("unchecked")
		ArrayList<Integer> keys = (ArrayList<Integer>) this.secondaryIndex.get(partitonNum-1).get(index.index).get(key);
    	Tuple [] result = new Tuple[keys.size()];
     	for(int i=0;i<keys.size();i++)
    	{
    		result[i] = data.get(partitonNum - 1).get(tableName).get(keys.get(i));
    	}
        return result;
    }
    public Tuple [] readTuple(Integer partitonNum, String tableName, String key, SecondaryIndexType index) {
    	@SuppressWarnings("unchecked")
		ArrayList<Integer> keys = (ArrayList<Integer>) this.secondaryIndex.get(partitonNum-1).get(index.index).get(key);
    	Tuple [] result = new Tuple[keys.size()];
     	for(int i=0;i<keys.size();i++)
    	{
    		result[i] = data.get(partitonNum - 1).get(tableName).get(keys.get(i));
    	}
        return result;
    }

    public void updateTuple(Long key, Tuple values) {
        Integer partitionNum = getPartition(key);
        Integer tableKey = getKeyWOTableWOpartition(key);
        String tableName = getTable(key);
        updateTuple(partitionNum, tableName, tableKey, values);
    }

    public void updateTuple(Integer partitionNum, String tableName, Integer key,
            Tuple values) {
        data.get(partitionNum - 1).get(tableName).put(key, values);
    }

    public boolean hasTuple(Long key) {
        Integer partitionNum = getPartition(key);
        Integer tableKey = getKeyWOTableWOpartition(key);
        String tableName = getTable(key);
        return hasTuple(partitionNum, tableName, tableKey);
    }

    public boolean hasTuple(Integer partitionNum, String tableName,
            Integer tableKey) {
        return data.get(partitionNum - 1).get(tableName).containsKey(tableKey);
    }

    public int getPartitionId(Long key) {
        return getRealPartition(key);
    }
}