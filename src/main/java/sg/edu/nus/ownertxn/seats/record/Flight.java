package sg.edu.nus.ownertxn.seats.record;

public class Flight extends sg.edu.nus.ownertxn.Tuple
{
    public Flight() {}
    public Flight( Flight flight)
    {
    	this.F_ID=flight.F_ID;
    	this.F_AL_ID=flight.F_AL_ID; // reference AL_ID
    	this.F_DEPART_AP_ID=flight.F_DEPART_AP_ID; // reference aireline al_id
    	this.F_DEPART_TIME=flight.F_DEPART_TIME;
    	this.F_ARRIVE_AP_ID=flight.F_ARRIVE_AP_ID;
    	this.F_ARRIVE_TIME=flight.F_ARRIVE_TIME;
    	this.F_STATUS=flight.F_STATUS;
    	this.F_BASE_PRICE=flight.F_BASE_PRICE;
    	this.F_SEATS_TOTAL=flight.F_SEATS_TOTAL;
    	this.F_SEATS_LEFT=flight.F_SEATS_LEFT;
    	this.F_IATTR00=flight.F_IATTR00;
    	this.F_IATTR01=flight.F_IATTR01;
    	this.F_IATTR02=flight.F_IATTR02;
    	this.F_IATTR03=flight.F_IATTR03;
    	this.F_IATTR04=flight.F_IATTR04;
    	this.F_IATTR05=flight.F_IATTR05;
    	this.F_IATTR06=flight.F_IATTR06;
    	this.F_IATTR07=flight.F_IATTR07;
    	this.F_IATTR08=flight.F_IATTR08;
    	this.F_IATTR09=flight.F_IATTR09;
    	this.F_IATTR10=flight.F_IATTR10;
    	this.F_IATTR11=flight.F_IATTR11;
    	this.F_IATTR12=flight.F_IATTR12;
    	this.F_IATTR13=flight.F_IATTR13;
    	this.F_IATTR14=flight.F_IATTR14;
    	this.F_IATTR15=flight.F_IATTR15;
    	this.F_IATTR16=flight.F_IATTR16;
    	this.F_IATTR17=flight.F_IATTR17;
    	this.F_IATTR18=flight.F_IATTR18;
    	this.F_IATTR19=flight.F_IATTR19;
    	this.F_IATTR20=flight.F_IATTR20;
    	this.F_IATTR21=flight.F_IATTR21;
    	this.F_IATTR22=flight.F_IATTR22;
    	this.F_IATTR23=flight.F_IATTR23;
    	this.F_IATTR24=flight.F_IATTR24;
    	this.F_IATTR25=flight.F_IATTR25;
    	this.F_IATTR26=flight.F_IATTR26;
    	this.F_IATTR27=flight.F_IATTR27;
    	this.F_IATTR28=flight.F_IATTR28;
    	this.F_IATTR29=flight.F_IATTR29;
    }
    public    int F_ID;
    public    int F_AL_ID; // reference AL_ID
    public    int F_DEPART_AP_ID; // reference aireline al_id
    public    String F_DEPART_TIME;
    public    int F_ARRIVE_AP_ID;
    public    String F_ARRIVE_TIME;
    public    long F_STATUS;
    public    double F_BASE_PRICE;
    public    long F_SEATS_TOTAL;
    public    long F_SEATS_LEFT;
    public    long F_IATTR00;
    public    long F_IATTR01;
    public    long F_IATTR02;
    public    long F_IATTR03;
    public    long F_IATTR04;
    public    long F_IATTR05;
    public    long F_IATTR06;
    public    long F_IATTR07;
    public    long F_IATTR08;
    public    long F_IATTR09;
    public    long F_IATTR10;
    public    long F_IATTR11;
    public    long F_IATTR12;
    public    long F_IATTR13;
    public    long F_IATTR14;
    public    long F_IATTR15;
    public    long F_IATTR16;
    public    long F_IATTR17;
    public    long F_IATTR18;
    public    long F_IATTR19;
    public    long F_IATTR20;
    public    long F_IATTR21;
    public    long F_IATTR22;
    public    long F_IATTR23;
    public    long F_IATTR24;
    public    long F_IATTR25;
    public    long F_IATTR26;
    public    long F_IATTR27;
    public    long F_IATTR28;
    public    long F_IATTR29;
    public int size() {
        return 0;
    }

    @Override
    public String toString() {
        return "Flight(" + "..." + ")";
    }
}