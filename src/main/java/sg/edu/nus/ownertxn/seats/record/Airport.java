package sg.edu.nus.ownertxn.seats.record;

public class Airport extends sg.edu.nus.ownertxn.Tuple
{
    public Airport() {}

    public    int AP_ID;
    public    String AP_CODE;
    public    String AP_NAME;
    public    String AP_CITY;
    public    String AP_POSTAL_CODE;
    public    int AP_CO_ID; // reference country co_id
    public    double AP_LONGTITUDE;
    public    double AP_LATITUDE;
    public    double AP_GMT_OFFSET;
    public    int AP_WAC;
    public    long AP_IATTR00;
    public    long AP_IATTR01;
    public    long AP_IATTR03;
    public    long AP_IATTR04;
    public    long AP_IATTR05;
    public    long AP_IATTR06;
    public    long AP_IATTR07;
    public    long AP_IATTR08;
    public    long AP_IATTR09;
    public    long AP_IATTR10;
    public    long AP_IATTR11;
    public    long AP_IATTR12;
    public    long AP_IATTR13;
    public    long AP_IATTR14;
    public    long AP_IATTR15;
    
    public int size() {
        return 0;
    }

    @Override
    public String toString() {
        return "Airport(" + "..." + ")";
    }
}