package sg.edu.nus.ownertxn;

public class TxnUtil
{
    private static TxnExecEngine _eng = null;

    public static void init(TxnExecEngine eng) {
        _eng = eng;
    }

    public static void initTxn(long txnId) {
        _eng.initTxn(txnId);
    }

    public static void startTxn(long txnId) {
        _eng.startTxn(txnId);
    }

    public static boolean waitForOwner(long key, long txnId) {
        return _eng.waitForOwner(key, txnId);
    }

    public static void commitTxn(long txnId) {
        _eng.commitTxn(txnId);
    }

    public static void commitTxn(long txnId, int runCount) {
        _eng.commitTxn(txnId, runCount);
    }

    public static void abortTxn(long txnId) {
        _eng.abortTxn(txnId);
    }

    public static void abortAndRestartTxn(long txnId, Runnable txn) {
        _eng.abortAndRestartTxn(txnId, txn);
    }
}