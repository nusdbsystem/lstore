package sg.edu.nus.ownertxn.tatp.record;

public class Subscriber extends sg.edu.nus.ownertxn.Tuple
{
    public Subscriber() {}

    public int size() {
        return 0;
    }

    @Override
    public String toString() {
        return "Subscriber(" + "..." + ")";
    }
}