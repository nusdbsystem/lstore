package sg.edu.nus.ownertxn.tatp.record;

public class SpecialFacility extends sg.edu.nus.ownertxn.Tuple
{
    public SpecialFacility() {}

    public int size() {
        return 0;
    }

    @Override
    public String toString() {
        return "SpecialFacility(" + "..." + ")";
    }
}