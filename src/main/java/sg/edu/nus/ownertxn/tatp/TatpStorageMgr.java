package sg.edu.nus.ownertxn.tatp;

import java.util.Map;

import sg.edu.nus.ownertxn.Tuple;

public class TatpStorageMgr extends sg.edu.nus.ownertxn.StorageMgr
{
    public TatpStorageMgr(int myId, Map<String, String> conf) {
        super(myId, conf);
    }

    public void insertTuple(Long key, Tuple values) {}

    public void deleteTuple(Long key) {}

    public Tuple readTuple(Long key) {
        return data.get(0).get("TABLENAME").get(key);
    }

    public void updateTuple(Long key, Tuple values) {}

    public boolean hasTuple(Long key) {
        return true;
    }

    public int getPartitionId(Long key) {
        return 0;
    }
}