package sg.edu.nus.ownertxn.tatp.txn;

import sg.edu.nus.ownertxn.tatp.TatpTxnExecEngine;

public abstract class AnyTatpTxn implements Runnable
{
    protected final TatpTxnExecEngine eng;
    public final Long txnId;

    public AnyTatpTxn(TatpTxnExecEngine eng) {
        this.eng = eng;
        txnId = eng.genTxnId();
    }
}