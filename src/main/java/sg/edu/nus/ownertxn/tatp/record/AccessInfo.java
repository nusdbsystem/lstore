package sg.edu.nus.ownertxn.tatp.record;

public class AccessInfo extends sg.edu.nus.ownertxn.Tuple
{
    public AccessInfo() {}

    public int size() {
        return 0;
    }

    @Override
    public String toString() {
        return "AccessInfo(" + "..." + ")";
    }
}