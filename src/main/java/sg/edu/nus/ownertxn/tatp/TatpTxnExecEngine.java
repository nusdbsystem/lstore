package sg.edu.nus.ownertxn.tatp;

import qlib.RichRandom;
import static qlib.Logger.*;
import sg.edu.nus.ownertxn.TxnExecEngine;
import sg.edu.nus.ownertxn.TxnSvr;
import sg.edu.nus.ownertxn.tatp.txn.*;

public class TatpTxnExecEngine extends TxnExecEngine
{
    public final TatpStorageMgr storeMgr;

    public TatpTxnExecEngine(TxnSvr txnSvr) {
        super(txnSvr);
        storeMgr = (TatpStorageMgr) txnSvr.getStoreMgr();
    }

    @Override
    protected void prepare() {}

    @Override
    protected void migrate() {}

    @Override
    protected void execute() throws Exception {
        RichRandom rand = new RichRandom();

        tk.start();
        long tickMillis;
        while ((tickMillis = tk.waitUntilNextTick()) > 0) {
            for (int i = 0; i < batchSizePerTick; ++i) {
                int choice = rand.nextInt(100);
                if (choice < 35) { // 35%
                    threadPool.execute(new GetSubscriberData(this));
                }
                else if (choice < 45) { // 10%
                    threadPool.execute(new GetNewDestination(this));
                }
                else if (choice < 80) { // 35%
                    threadPool.execute(new GetAccessData(this));
                }
                else if (choice < 82) { // 2%
                    threadPool.execute(new UpdateSubscriberData(this));
                }
                else if (choice < 96) { // 14%
                    threadPool.execute(new UpdateLocation(this));
                }
                else if (choice < 98) { // 2%
                    threadPool.execute(new InsertCallForwarding(this));
                }
                else if (choice < 100) { // 2%
                    threadPool.execute(new DeleteCallForwarding(this));
                }
                else {
                    warn("dice overflow: " + choice);
                }
            }
        }
    }
}
