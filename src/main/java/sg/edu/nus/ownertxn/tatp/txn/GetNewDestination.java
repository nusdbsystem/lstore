package sg.edu.nus.ownertxn.tatp.txn;

import static sg.edu.nus.ownertxn.TxnUtil.*;
import sg.edu.nus.ownertxn.tatp.TatpStorageMgr;
import sg.edu.nus.ownertxn.tatp.TatpTxnExecEngine;
import sg.edu.nus.ownertxn.tatp.record.*;

public class GetNewDestination extends AnyTatpTxn
{
    public GetNewDestination(TatpTxnExecEngine eng) {
        super(eng);
        initTxn(txnId);
    }

    public void run() {
        /* prepare */
        TatpStorageMgr sm = eng.storeMgr;

        /* execute */
        boolean gotOwner = false;
        startTxn(txnId);

        commitTxn(txnId);
    }
}
