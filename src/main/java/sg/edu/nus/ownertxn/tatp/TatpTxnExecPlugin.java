package sg.edu.nus.ownertxn.tatp;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static qlib.Logger.*;
import sg.edu.nus.ownertxn.TxnSvr;
import sg.edu.nus.ownertxn.tatp.txn.*;

public class TatpTxnExecPlugin extends TatpTxnExecEngine
{
    public static enum TxnType {
        INVALID,
        GET_SUBSCRIBER_DATA,
        GET_NEW_DESTINATION,
        GET_ACCESS_DATA,
        UPDATE_SUBSCRIBER_DATA,
        UPDATE_LOCATION,
        INSERT_CALL_FORWARDING,
        DELETE_CALL_FORWARDING
    }

    private final BlockingQueue<TxnType> _bqueue;

    public TatpTxnExecPlugin(TxnSvr txnSvr) {
        super(txnSvr);
        _bqueue = new LinkedBlockingQueue<TxnType>();
    }

    @Override
    protected void execute() throws Exception {
        for (;;) {
            switch (_bqueue.take()) {
            case GET_SUBSCRIBER_DATA:
                threadPool.execute(new GetSubscriberData(this));
                break;

            case GET_NEW_DESTINATION:
                threadPool.execute(new GetNewDestination(this));
                break;

            case GET_ACCESS_DATA:
                threadPool.execute(new GetAccessData(this));
                break;

            case UPDATE_SUBSCRIBER_DATA:
                threadPool.execute(new UpdateSubscriberData(this));
                break;

            case UPDATE_LOCATION:
                threadPool.execute(new UpdateLocation(this));
                break;

            case INSERT_CALL_FORWARDING:
                threadPool.execute(new InsertCallForwarding(this));
                break;

            case DELETE_CALL_FORWARDING:
                threadPool.execute(new DeleteCallForwarding(this));
                break;

            default:
                error("invalid transaction type");
                break;
            }
        }
    }

    public void addTxn(TxnType txnTypeId) {
        try {
            _bqueue.add(txnTypeId);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}