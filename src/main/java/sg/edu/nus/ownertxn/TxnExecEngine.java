package sg.edu.nus.ownertxn;

import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import static java.util.concurrent.Executors.newFixedThreadPool;
import static java.util.concurrent.TimeUnit.SECONDS;

import akka.actor.ActorRef;
import static akka.pattern.Patterns.ask;
import akka.util.Timeout;
import qlib.Ticker;
import static qlib.CastUtils.*;
import static qlib.Logger.*;
import static qlib.Monitor.*;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import sg.edu.nus.ownertxn.Message.*;

public class TxnExecEngine implements Runnable
{
    protected final Map<String, String> conf;

    private static final double QEF = 2; // queue expand factor
    protected final int taskQueueLimit;
    protected final int taskQueueHalf;
    protected final BlockingQueue<Runnable> taskQueue;
    protected final ExecutorService threadPool;

    protected final Ticker tk;
    protected int batchSizePerTick;
    protected boolean stress;
    private static final int MAX_TICK_HZ = 50;

    public final double distr;
    public final double locality;
    public final boolean restartAborted;

    private final long _myId;
    private final ActorRef _txnSvrActor;
    private final long _logPenaltyMillis;
    private final long _abortPenaltyNanos;

    public TxnExecEngine(TxnSvr txnSvr) {
        conf = txnSvr.getConfAsJava();

        int hz = getInt(conf.get("hz"));
        int batchSize = 1;
        if (hz > MAX_TICK_HZ) {
            batchSize = hz / MAX_TICK_HZ;
            hz = MAX_TICK_HZ;
        }
        tk = new Ticker(hz, getLong(conf.get("time")));
        batchSizePerTick = batchSize;
        stress = getBoolean(conf.get("stress"));
        info("Stress test: " + stress);

        int p = getInt(conf.get("parallel"));
        info("Parallelism: " + p + " thread" + (p > 1 ? "s" : ""));

        if (stress) {
            taskQueueLimit = getInt(conf.get("taskQueueLimit"));
            info("Task queue limit: " + taskQueueLimit);
            taskQueueHalf = taskQueueLimit / 2;
            taskQueue = new ArrayBlockingQueue<>((int) (taskQueueLimit * QEF));
            threadPool = new ThreadPoolExecutor(p, p, 0L, SECONDS, taskQueue);
        }
        else {
            taskQueueLimit = 0;
            info("Task queue limit: unbounded");
            taskQueueHalf = 0;
            taskQueue = null;
            threadPool = newFixedThreadPool(p);
        }

        distr = getDouble(conf.get("distr"));
        locality = getDouble(conf.get("locality"));
        info("Distributed: " + (distr * 100) + "%, " + "Locality: "
                + (locality * 100) + "%");

        _myId = (long) txnSvr.getMyId();
        _txnSvrActor = txnSvr.getTxnSvrActor();

        if (getBoolean(conf.get("withLog"))) {
            _logPenaltyMillis = getLong(conf.get("logPenaltyMillis"));
        }
        else {
            _logPenaltyMillis = 0;
        }
        info("logging is simulated as " + _logPenaltyMillis
                + " ms per committed transaction");

        _abortPenaltyNanos = getLong(conf.get("abortPenaltyUs")) * 1000;
        info("abort is simulated as " + _abortPenaltyNanos
                + " ns per aborted transaction");

        restartAborted = getBoolean(conf.get("restartAborted"));
        info("restart aborted transactions: " + restartAborted);

        TxnUtil.init(this);
    }

    private static long txnCount = 0;

    public long genTxnId() {
        synchronized (this) {
            ++txnCount;
            return TxnId.getTxnId(_myId, txnCount, System.currentTimeMillis());
        }
    }

    public void run() {
        debug("transaction execution engine started");

        prepare();

        waitForMigrate();

        migrate();

        waitForStart();

        try {
            execute();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            finish();
            threadPool.shutdown();
            // NOTE: Older tasks might still progress, but it allows no more
            // tasks to be submitted.
        }
    }

    protected void prepare() {
        trace("create tables, load data, etc.");
    }

    protected void migrate() {
        trace("migrate part of data from/to other partitions");
    }

    protected void execute() throws Exception {
        trace("genertate and process transactions");
    }

    protected void finish() {
        trace("necessarily clean up when exit");
    }

    private final Timeout _timeout = new Timeout(Duration.create(5, "days"));

    private void waitForMigrate() {
        PartitionReady ready = new PartitionReady((int) _myId);
        Future<Object> future = ask(_txnSvrActor, ready, _timeout);
        try {
            Await.ready(future, _timeout.duration());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void waitForStart() {
        MigrationDone migrateDone = new MigrationDone((int) _myId);
        Future<Object> future = ask(_txnSvrActor, migrateDone, _timeout);
        try {
            Await.ready(future, _timeout.duration());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initTxn(long txnId) {
        TxnIssue msg = new TxnIssue(txnId);
        Future<Object> future = ask(_txnSvrActor, msg, _timeout);
        try {
            Await.ready(future, _timeout.duration());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startTxn(long txnId) {
        TxnStart msg = new TxnStart(txnId, System.nanoTime());
        _txnSvrActor.tell(msg, ActorRef.noSender());
    }

    /**
     * Request for ownership with blocking.
     * 
     * @RETURN: True if the ownership is obtained; otherwise, false.
     */
    public boolean waitForOwner(long key, long txnId) {
        AskForOwner req = new AskForOwner(key, txnId);
        Future<Object> future = ask(_txnSvrActor, req, _timeout);
        boolean result = false;
        try {
            result = (boolean) Await.result(future, _timeout.duration());
        }
        catch (Exception e) {
            error("failed to get ownership: key=" + key);
            e.printStackTrace();
        }

        return result;
    }

    public void commitTxn(long txnId, int runCount) {
        // simulate logging
        if (_logPenaltyMillis > 0) {
            try {
                Thread.sleep(_logPenaltyMillis);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        _txnSvrActor.tell(new TxnCommit(txnId, runCount), ActorRef.noSender());
    }

    public void commitTxn(long txnId) {
        commitTxn(txnId, 1);
    }

    public void abortTxn(long txnId) {
        // simulate abort
        long expire = System.nanoTime() + _abortPenaltyNanos;
        while (System.nanoTime() < expire);
    }

    public void abortAndRestartTxn(long txnId, Runnable txn) {
        abortTxn(txnId);

        if (!threadPool.isShutdown()) {
            initTxn(txnId);
            try {
                threadPool.execute(txn);
            }
            catch (RejectedExecutionException e) {
                warn("failed to restart: thread pool is already shutdown");
            }
        }
    }

    protected void shrinkBatchSizePerTick() {
        int shrink = (int) Math.floor(batchSizePerTick / (tk.getHz() + 0.0));
        batchSizePerTick -= shrink;
        debug("shrink batch size to " + batchSizePerTick);
    }

    protected void raiseBatchSizePerTick() {
        int raise = (int) Math.ceil(batchSizePerTick / (tk.getHz() + 0.0));
        batchSizePerTick += raise;
        debug("raise batch size to " + batchSizePerTick);
    }
}