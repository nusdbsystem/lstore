package sg.edu.nus.ownertxn;

import java.util.Comparator;

public class TxnId implements Comparator<Long>
{
    private static Long partitionMask = 0xff00000000000000L;
    private static Long localCountMask = 0x00ffffffff000000L;
    private static Long timestampMask = 0x0000000000ffffffL;
    private static int partitionMove = 56;
    private static int localCountMove = 24;

    public static int getPartition(Long txnId) {
        return Long.valueOf((txnId & partitionMask) >> partitionMove)
                .intValue();
    }

    public static int getlocalCount(Long txnId) {
        return Long.valueOf((txnId & localCountMask) >> localCountMove)
                .intValue();
    }

    public static int getTimestamp(Long txnId) {
        return Long.valueOf((txnId & timestampMask)).intValue();
    }

    public static Long getTxnId(Long partition, Long localCount, Long timestamp) {
        return (partition << partitionMove) | (localCount << localCountMove)
                | (timestamp & timestampMask);
    }

    @Override
    public int compare(Long o1, Long o2) {
        int o1pId = getPartition(o1);
        int o2pId = getPartition(o2);
        int o1lCount = getlocalCount(o1);
        int o2lCount = getlocalCount(o2);
        int o1TS = getTimestamp(o1);
        int o2TS = getTimestamp(o2);
        if (o1pId == o2pId) {
            assert(o1lCount != o2lCount);
            if (o1lCount < o2lCount)
                return -1;
            else if (o1lCount > o2lCount)
                return 1;
            else
                return 0;
        }
        else {
            if (o1TS < o2TS)
                return -1;
            else if (o1TS > o2TS)
                return 1;
            else if (o1pId < o2pId)
                return -1;
            else
                return 1;
        }
    }
}
