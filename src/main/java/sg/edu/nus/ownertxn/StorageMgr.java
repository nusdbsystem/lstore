package sg.edu.nus.ownertxn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static qlib.CastUtils.*;

public abstract class StorageMgr
{
    public final int myId;
    public final int numWhsePerPartition;
    public final int numTotalWhse;

    protected final List<HashMap<String, Map<Integer, Tuple>>> data;

    public StorageMgr(int myId, Map<String, String> conf) {
        this.myId = myId;
        numWhsePerPartition = getInt(conf.get("numWarehouses"));
        numTotalWhse = getInt(conf.get("numPartitions")) * numWhsePerPartition;

        data = new ArrayList<HashMap<String, Map<Integer, Tuple>>>();
        for (int i = 0; i < numTotalWhse; ++i) {
            data.add(new HashMap<String, Map<Integer, Tuple>>());
        }
    }

    public abstract int getPartitionId(Long key);

    public abstract boolean hasTuple(Long key);

    public abstract void insertTuple(Long key, Tuple tuple);

    public abstract void deleteTuple(Long key);

    public abstract Tuple readTuple(Long key);

    public abstract void updateTuple(Long key, Tuple tuple);

    private static StorageMgr _customized = null;

    public static void setCustomizedStorageMgr(StorageMgr storeMgr) {
        _customized = storeMgr;
    }

    public static StorageMgr getCustomizedStorageMgr() {
        return _customized;
    }
}
