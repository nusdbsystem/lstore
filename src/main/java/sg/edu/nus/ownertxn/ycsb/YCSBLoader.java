package sg.edu.nus.ownertxn.ycsb;

import static sg.edu.nus.ownertxn.ycsb.YCSBConstants.*;
import static sg.edu.nus.ownertxn.ycsb.YCSBPrimaryKey.*;
import sg.edu.nus.ownertxn.ycsb.record.YCSBRecord;
import static sg.edu.nus.ownertxn.ycsb.YCSBUtil.astring;

public class YCSBLoader
{
    private final YCSBStorageMgr sm;

    public YCSBLoader(YCSBStorageMgr sm) {
        this.sm = sm;
    }

    public void load() {
        for (int whseId = sm.startLocalWhseId; whseId <= sm.endLocalWhseId; ++whseId) {
            for (int i = 0; i <= NUM_RECORDS; ++i) {
                YCSBRecord ycsbRecord = new YCSBRecord();
                ycsbRecord.key = i;
                ycsbRecord.f1 = astring(COLUMN_LENGTH);
                ycsbRecord.f2 = astring(COLUMN_LENGTH);
                ycsbRecord.f3 = astring(COLUMN_LENGTH);
                ycsbRecord.f4 = astring(COLUMN_LENGTH);
                ycsbRecord.f5 = astring(COLUMN_LENGTH);
                ycsbRecord.f6 = astring(COLUMN_LENGTH);
                ycsbRecord.f7 = astring(COLUMN_LENGTH);
                ycsbRecord.f8 = astring(COLUMN_LENGTH);
                ycsbRecord.f9 = astring(COLUMN_LENGTH);
                ycsbRecord.f10 = astring(COLUMN_LENGTH);
                sm.insertTuple(whseId, TABLE_NAME, ycsbKey(i), ycsbRecord);
            }
        }
    }
}
