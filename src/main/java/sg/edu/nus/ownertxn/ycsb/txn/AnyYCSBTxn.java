package sg.edu.nus.ownertxn.ycsb.txn;

import sg.edu.nus.ownertxn.ycsb.YCSBTxnExecEngine;

public abstract class AnyYCSBTxn implements Runnable
{
    protected final YCSBTxnExecEngine eng;
    public final Long txnId;

    public AnyYCSBTxn(YCSBTxnExecEngine eng) {
        this.eng = eng;
        txnId = eng.genTxnId();
        eng.initTxn(txnId);
    }
}
