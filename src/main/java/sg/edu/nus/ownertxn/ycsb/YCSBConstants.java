package sg.edu.nus.ownertxn.ycsb;

public class YCSBConstants {
    public static final int NUM_RECORDS = 100000;  // Note: this should match value in YCSB.properties
    
    public static final double ZIPFIAN_CONSTANT = .5;

    public static final int HOT_DATA_WORKLOAD_SKEW = 50;
    public static final int HOT_DATA_SIZE = 50;

    public static final int WARM_DATA_SIZE = 0;
    public static final int WARM_DATA_WORKLOAD_SKEW = 0;


    public static final String TABLE_NAME = "USERTABLE"; 
    public static final int NUM_COLUMNS = 11; 
    public static final int COLUMN_LENGTH = 100;

    public static final int BATCH_SIZE = 10000; 
    public static final int MAX_SCAN = 1000; 

    // Transaction frequencies as specified in YCSB
    public static final int FREQUENCY_INSERT_RECORD = 0;
    public static final int FREQUENCY_DELETE_RECORD = 0;
    public static final int FREQUENCY_READ_RECORD = 50;
    public static final int FREQUENCY_SCAN_RECORD = 0;
    public static final int FREQUENCY_UPDATE_RECORD = 50;
}
