package sg.edu.nus.ownertxn.ycsb;

import java.util.HashMap;
import java.util.Map;
import static java.util.Collections.synchronizedMap;

import static qlib.CastUtils.getDouble;
import static qlib.Logger.*;
import sg.edu.nus.ownertxn.Tuple;
import static sg.edu.nus.ownertxn.ycsb.YCSBPrimaryKey.*;

public class YCSBStorageMgr extends sg.edu.nus.ownertxn.StorageMgr
{
    public final int startLocalWhseId;
    public final int endLocalWhseId;

    public YCSBStorageMgr(int myId, Map<String, String> conf) {
        super(myId, conf);
        for (int i = 0; i < numTotalWhse; ++i) {
            data.get(i).put(YCSBConstants.TABLE_NAME,
                    synchronizedMap(new HashMap<Integer, Tuple>()));
        }
        startLocalWhseId = myId * numWhsePerPartition + 1;
        endLocalWhseId = (myId + 1) * numWhsePerPartition;
        YCSBPrimaryKey.numWhsePerPartition = numWhsePerPartition;

        double inSituDataPercent = getDouble(conf.get("inSituDataPercent"));
        YCSBUtil.setInSituDataPercent(inSituDataPercent);
        info("In-situ data: " + (inSituDataPercent * 100) + "%");

        YCSBUtil.setLocalWhseIdRange(startLocalWhseId, endLocalWhseId);
        YCSBUtil.setNumTotalWhse(numTotalWhse);
        YCSBUtil.setNumWhsePerPartition(numWhsePerPartition);
        info("Local warehouse IDs: #" + startLocalWhseId + " - #"
                + endLocalWhseId + " (" + numWhsePerPartition + "/"
                + numTotalWhse + " total)");
    }

    public boolean hasTuple(Long key) {
        Integer partitionNum = getPartition(key);
        String tableName = getTable(key);
        Integer tableKey = getKeyWOTableWOpartition(key);
        return hasTuple(partitionNum, tableName, tableKey);
    }

    public boolean hasTuple(Integer partitionNum, String tableName,
            Integer tableKey) {
        return data.get(partitionNum - 1).get(tableName).containsKey(tableKey);
    }

    public int getPartitionId(Long key) {
        return getRealPartition(key);
    }

    public void insertTuple(Long key, Tuple values) {
        Integer partitionNum = getPartition(key);
        String tableName = getTable(key);
        Integer tableKey = getKeyWOTableWOpartition(key);
        insertTuple(partitionNum, tableName, tableKey, values);
    }

    public void insertTuple(Integer partitionNum, String tableName,
            Integer key, Tuple values) {
        data.get(partitionNum - 1).get(tableName).put(key, values);
    }

    public void deleteTuple(Long key) {
        Integer partitionNum = getPartition(key);
        String tableName = getTable(key);
        Integer tableKey = getKeyWOTableWOpartition(key);
        deleteTuple(partitionNum, tableName, tableKey);
    }

    public void deleteTuple(Integer partitionNum, String tableName, Integer key) {
        data.get(partitionNum - 1).get(tableName).remove(key);
    }

    public Tuple readTuple(Long key) {
        Integer partitionNum = getPartition(key);
        String tableName = getTable(key);
        Integer tableKey = getKeyWOTableWOpartition(key);
        return readTuple(partitionNum, tableName, tableKey);
    }

    public Tuple readTuple(Integer partitonNum, String tableName, Integer key) {
        return data.get(partitonNum - 1).get(tableName).get(key);
    }

    public Tuple readTuple(Integer partitonNum, String tableName) {
        for (Map.Entry<Integer, Tuple> tmp : data.get(partitonNum - 1)
                .get(tableName).entrySet()) {
            return tmp.getValue();
        }
        return null;
    }

    public void updateTuple(Long key, Tuple values) {
        Integer partitionNum = getPartition(key);
        String tableName = getTable(key);
        Integer tableKey = getKeyWOTableWOpartition(key);
        updateTuple(partitionNum, tableName, tableKey, values);
    }

    public void updateTuple(Integer partitionNum, String tableName,
            Integer key, Tuple values) {
        data.get(partitionNum - 1).get(tableName).put(key, values);
    }
}
