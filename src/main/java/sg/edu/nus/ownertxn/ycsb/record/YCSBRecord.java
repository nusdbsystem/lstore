package sg.edu.nus.ownertxn.ycsb.record;

public class YCSBRecord extends sg.edu.nus.ownertxn.Tuple
{
    public YCSBRecord() {}

    public YCSBRecord(String str) {
        String[] strSplit = str.split(",");
        key = Integer.valueOf(strSplit[0]);
        f1 = strSplit[1];
        f2 = strSplit[2];
        f3 = strSplit[3];
        f4 = strSplit[4];
        f5 = strSplit[5];
        f6 = strSplit[6];
        f7 = strSplit[7];
        f8 = strSplit[8];
        f9 = strSplit[9];
        f10 = strSplit[10];
    }

    public int key;
    public String f1;
    public String f2;
    public String f3;
    public String f4;
    public String f5;
    public String f6;
    public String f7;
    public String f8;
    public String f9;
    public String f10;

    @Override
    public int size() {
        return 1004;
    }

    @Override
    public String toString() {
        return ("YCSBRECORD(" + key + "," + f1 + "," + f2 + "," + f3 + "," + f4
                + "," + f5 + "," + f6 + "," + f7 + "," + f8 + "," + f9 + ","
                + f10 + ")");
    }
}
