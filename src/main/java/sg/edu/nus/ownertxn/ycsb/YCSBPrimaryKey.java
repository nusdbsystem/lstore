package sg.edu.nus.ownertxn.ycsb;

public class YCSBPrimaryKey {
	public static final long tableMask = 0x000ff00000000000L;
    public static final long partitionMask = 0xfff0000000000000L;
    public static final long keyMove = 0x00000fffffffffffL;
    public static final int tablemovebit = 44;
    public static final int partitionmovebit = 52;
    public static int numWhsePerPartition = 0;
    
    public static Integer ycsbKey(Integer key)
    {
    	return key;
    }
    
    public static String getTable(Long key)
    {
    	Integer mask = Long.valueOf((key & tableMask) >> tablemovebit)
                .intValue();
    	switch(mask)
    	{
    	case 1: return "USERTABLE";
    	}
    	return null;
    }
    public static Integer getKeyWOTableWOpartition(Long key) {
        return Long.valueOf(key & keyMove).intValue();
    }

    public static Integer getRealPartition(Long key) {
        return (getPartition(key) - 1) / numWhsePerPartition;
    }

    public static Integer getPartition(Long key) {
        return Long.valueOf((key & partitionMask) >> partitionmovebit)
                .intValue();
    }

    public static Long getKeyWTTableWTparition(String table, Integer key,
            Long partition) {
        switch (table) {
        case "USERTABLE":
            return ((partition << partitionmovebit) | (1L << tablemovebit) | key);
        }
        return -1L;
    }
}
