package sg.edu.nus.ownertxn.ycsb;

import static qlib.CastUtils.*;
import static qlib.Logger.*;
import sg.edu.nus.ownertxn.TxnExecEngine;
import sg.edu.nus.ownertxn.TxnSvr;
import sg.edu.nus.ownertxn.ycsb.txn.*;

public class YCSBTxnExecEngine extends TxnExecEngine
{
    public final YCSBStorageMgr storeMgr;
    public final int steps;

    public YCSBTxnExecEngine(TxnSvr txnSvr) {
        super(txnSvr);
        storeMgr = (YCSBStorageMgr) txnSvr.getStoreMgr();
        steps = getInt(conf.get("stepsPerYcsbTxn"));
        info("steps per YCSB transaction: " + steps);
    }

    private boolean _isLoadingDone = false;

    public boolean isLoadingDone() {
        return _isLoadingDone;
    }

    @Override
    protected void prepare() {
        (new YCSBLoader(storeMgr)).load();
        _isLoadingDone = true;
    }

    @Override
    protected void migrate() {}

    @Override
    protected void execute() throws Exception {
        tk.start();
        long tickMillis;
        while ((tickMillis = tk.waitUntilNextTick()) > 0) {
            if (stress) {
                int taskQueueSize = taskQueue.size();
                if (taskQueueSize > taskQueueLimit) {
                    shrinkBatchSizePerTick();
                    continue;
                }
                else if (taskQueueSize < taskQueueHalf) {
                    raiseBatchSizePerTick();
                }
            }

            for (int i = 0; i < batchSizePerTick; ++i) {
                // threadPool.execute(new MultistepAccess(this));
                threadPool.execute(new CombostepAccess(this));
            }
        }
    }
}
