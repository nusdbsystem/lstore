package sg.edu.nus.ownertxn.ycsb;

import qlib.RichRandom;
import sg.edu.nus.ownertxn.tpcc.RandomGenerator;
import sg.edu.nus.ownertxn.tpcc.TPCCLoader;
import static sg.edu.nus.ownertxn.ycsb.YCSBConstants.*;

public class YCSBUtil
{
    public static final RichRandom rand = new RichRandom();

    private static final int DF = 1000; // decimal factor
    private static double localUserPercent = 0.8;
    private static double distrUserPercent = (DF - localUserPercent * DF) / DF;
    private static int limitLocalUserId = (int) (NUM_RECORDS * localUserPercent);

    public static void setInSituDataPercent(double inSituDataPercent) {
        localUserPercent = inSituDataPercent;
        distrUserPercent = (DF - localUserPercent * DF) / DF;
        limitLocalUserId = (int) (NUM_RECORDS * localUserPercent);
    }

    private static int startLocalWhseId = 0;
    private static int endLocalWhseId = 0;

    public static void setLocalWhseIdRange(int startId, int endId) {
        startLocalWhseId = startId;
        endLocalWhseId = endId;
    }

    private static int numTotalWhse = 0;

    public static void setNumTotalWhse(int whseCount) {
        numTotalWhse = whseCount;
    }

    private static int numWhsePerPartition = 0;

    public static void setNumWhsePerPartition(int whseCountPerPartition) {
        numWhsePerPartition = whseCountPerPartition;
    }

    /**
     * Access a remote warehouse chosen at random.
     */
    public static int getRandomRemoteWhseId() {
        int whseId = rand.nextIntIncl(1, numTotalWhse);
        while (startLocalWhseId <= whseId && whseId <= endLocalWhseId) {
            whseId = rand.nextIntIncl(1, numTotalWhse);
        }
        return whseId;
    }

    /**
     * Access the bi-neighboring warehouse.
     */
    public static int getBiNeighborRemoteWhseId(int myWhseId) {
        int whseId = (myWhseId + (TPCCLoader.gen.nextBoolean() ? 1 : -1)
                * numWhsePerPartition * 2 - 1)
                % numTotalWhse + 1;
        if (whseId < 1) {
            whseId += numTotalWhse;
        }
        return whseId;
    }

    /**
     * Access the neighboring warehouse.
     */
    public static int getNeighborRemoteWhseId(int myWhseId) {
        return (myWhseId + numWhsePerPartition - 1) % numTotalWhse + 1;
    }

    public static int getUserId() {
        return rand.nextIntIncl(1, NUM_RECORDS);
    }

    public static int getLocalUserId() {
        return (int) Math.ceil(getUserId() * localUserPercent);
    }

    public static int getDistrUserId() {
        return (int) Math.ceil(getUserId() * distrUserPercent) + limitLocalUserId;
    }

    public static String astring(int lenMin, int lenMax) {
        return randomString(lenMin, lenMax, 'A', 26);
    }

    public static String astring(int len) {
        return randomString(len, len, 'A', 26);
    }

    public static String nstring(int lenMin, int lenMax) {
        return randomString(lenMin, lenMax, '0', 10);
    }

    public static String randomString(int lenMin, int lenMax, char base,
            int numChars) {
        int length = (lenMin == lenMax ? lenMin : rand.nextIntIncl(lenMin,
                lenMax));
        byte baseByte = (byte) base;
        byte[] bytes = new byte[length];
        for (int i = 0; i < length; ++i) {
            bytes[i] = (byte) (baseByte + rand.nextInt(numChars));
        }
        return new String(bytes);
    }
}
