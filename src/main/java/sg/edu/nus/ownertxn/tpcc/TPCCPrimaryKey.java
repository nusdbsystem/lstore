package sg.edu.nus.ownertxn.tpcc;

public class TPCCPrimaryKey
{
    public static final long tableMask = 0x000ff00000000000L;
    public static final long partitionMask = 0xfff0000000000000L;
    public static final long keyMove = 0x00000fffffffffffL;
    public static final int tablemovebit = 44;
    public static final int partitionmovebit = 52;
    public static int numWhsePerPartition = 0;

    public static Integer warehouseKey(Integer w_id) {
        return w_id;
    }

    public static Integer itemKey(Integer i_id) {
        return i_id;
    }

    public static Integer districtKey(Integer d_id) {
        return d_id;
    }

    public static Integer customerKey(Integer c_id, Integer c_d_id) {
        return districtKey(c_d_id) * TPCCConfig.configCustPerDist + c_id;
    }

    public static Integer historyKey(Integer c_id, Integer c_d_id) {
        return customerKey(c_id, c_d_id);
    }

    public static Integer orderlineKey(Integer d_id, Integer o_id,
            Integer o_number) {
        return orderKey(d_id, o_id) + o_number;
    }

    public static Integer orderKey(Integer d_id, Integer o_id) {
        return districtKey(d_id) * TPCCConfig.configCustPerDist + o_id;
    }

    public static Integer neworderKey(Integer d_id, Integer o_id) {
        return orderKey(d_id, o_id);
    }

    public static Integer stockKey(Integer s_i_id) {
        return TPCCConfig.configItemCount + s_i_id;
    }

    public static String getTable(Long key) {
        Integer mask = Long.valueOf((key & tableMask) >> tablemovebit)
                .intValue();
        switch (mask) {
        case 1:
            return "DISTRICT";
        case 2:
            return "WAREHOUSE";
        case 3:
            return "ITEM";
        case 4:
            return "STOCK";
        case 5:
            return "CUSTOMER";
        case 6:
            return "HISTORY";
        case 7:
            return "OORDER";
        case 8:
            return "ORDER_LINE";
        case 9:
            return "NEW_ORDER";
        }
        return null;
    }

    public static Integer getKeyWOTableWOpartition(Long key) {
        return Long.valueOf(key & keyMove).intValue();
    }

    public static Integer getRealPartition(Long key) {
        return (getPartition(key) - 1) / numWhsePerPartition;
    }

    public static Integer getPartition(Long key) {
        return Long.valueOf((key & partitionMask) >> partitionmovebit)
                .intValue();
    }

    public static Long getKeyWTTableWTparition(String table, Integer key,
            Long partition) {
        switch (table) {
        case "DISTRICT":
            return ((partition << partitionmovebit) | (1L << tablemovebit) | key);
        case "WAREHOUSE":
            return ((partition << partitionmovebit) | (2L << tablemovebit) | key);
        case "ITEM":
            return ((partition << partitionmovebit) | (3L << tablemovebit) | key);
        case "STOCK":
            return ((partition << partitionmovebit) | (4L << tablemovebit) | key);
        case "CUSTOMER":
            return ((partition << partitionmovebit) | (5L << tablemovebit) | key);
        case "HISTORY":
            return ((partition << partitionmovebit) | (6L << tablemovebit) | key);
        case "OORDER":
            return ((partition << partitionmovebit) | (7L << tablemovebit) | key);
        case "ORDER_LINE":
            return ((partition << partitionmovebit) | (8L << tablemovebit) | key);
        case "NEW_ORDER":
            return ((partition << partitionmovebit) | (9L << tablemovebit) | key);
        }
        return -1L;
    }

    // public static void main(String [] args)
    // {
    // //Long tmp = getKeyWTTableWTparition("NEW_ORDER", 2000,
    // 1L);
    // for(int i=0;i<10000;i++)
    // System.out.printf("%x\n",System.currentTimeMillis()>>10);
    // //System.out.println(System.nanoTime());
    // }
}
