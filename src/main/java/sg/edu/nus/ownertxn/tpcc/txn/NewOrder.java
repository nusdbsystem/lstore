package sg.edu.nus.ownertxn.tpcc.txn;

import static sg.edu.nus.ownertxn.TxnUtil.*;
import sg.edu.nus.ownertxn.tpcc.TPCCLoader;
import sg.edu.nus.ownertxn.tpcc.TpccStorageMgr;
import sg.edu.nus.ownertxn.tpcc.TpccTxnExecEngine;
import static sg.edu.nus.ownertxn.tpcc.TPCCConfig.configDistPerWhse;
import static sg.edu.nus.ownertxn.tpcc.TPCCConstants.*;
import static sg.edu.nus.ownertxn.tpcc.TPCCPrimaryKey.*;
import static sg.edu.nus.ownertxn.tpcc.TPCCUtil.*;
import sg.edu.nus.ownertxn.tpcc.record.*;

public class NewOrder extends AnyTpccTxn
{
    private static long _commitCount = 0;

    public static long getCommitCount() {
        return _commitCount;
    }

    private final TpccStorageMgr sm;
    private final int warehouseId;
    private final int districtID;
    private final int customerID;
    private final int numItems;
    private final int[] itemIDs;
    private final int[] supplierWarehouseIDs;
    private final int[] orderQuantities;
    private final int allLocal;

    public NewOrder(TpccTxnExecEngine eng) {
        super(eng);
        sm = eng.storeMgr;

        warehouseId = randomNumber(sm.startLocalWhseId, sm.endLocalWhseId,
                TPCCLoader.gen);
        districtID = randomNumber(1, configDistPerWhse, TPCCLoader.gen);
        // customerID = getCustomerID(TPCCLoader.gen);
        customerID = getLocalCustomerID(TPCCLoader.gen);

        numItems = (int) randomNumber(5, 15, TPCCLoader.gen);
        itemIDs = new int[numItems];
        supplierWarehouseIDs = new int[numItems];
        orderQuantities = new int[numItems];

        boolean isAllLocal = true;

        for (int i = 0; i < numItems; ++i) {
            if (TPCCLoader.gen.nextDouble() > eng.distrNewOrder) {
                supplierWarehouseIDs[i] = warehouseId;
                itemIDs[i] = getLocalItemID(TPCCLoader.gen);
            }
            else { // for distributed txn
                if (TPCCLoader.gen.nextDouble() < eng.locality) {
                    supplierWarehouseIDs[i] = getNeighborRemoteWhseId(warehouseId);
                    itemIDs[i] = getDistrItemID(TPCCLoader.gen);
                }
                else { // for distributed txn without locality
                    switch (eng.remoteAccessPattern) {
                    case TpccTxnExecEngine.REMOTE_RAND_ACCESS:
                        supplierWarehouseIDs[i] = getRandomRemoteWhseId();
                        break;

                    case TpccTxnExecEngine.REMOTE_BINEI_ACCESS:
                        supplierWarehouseIDs[i] = getBiNeighborRemoteWhseId(warehouseId);
                        break;

                    case TpccTxnExecEngine.REMOTE_NEI_ACCESS:
                        supplierWarehouseIDs[i] = getNeighborRemoteWhseId(warehouseId);
                        break;

                    default: // access local warehouse
                        System.err.println("invalid remote access pattern: "
                                + eng.remoteAccessPattern);
                        supplierWarehouseIDs[i] = warehouseId;
                        break;
                    }

                    itemIDs[i] = getItemID(TPCCLoader.gen);
                }

                isAllLocal = false;
            }
            orderQuantities[i] = randomNumber(1, 10, TPCCLoader.gen);
        }
        allLocal = isAllLocal ? 1 : 0;

        initTxn(txnId);
    }

    public void run() {
        ++runCount;
        boolean gotOwner = false;
        startTxn(txnId);

        Warehouse warehouse = (Warehouse) sm.readTuple(warehouseId,
                TABLENAME_WAREHOUSE, warehouseKey(warehouseId));
        District district = (District) sm.readTuple(warehouseId,
                TABLENAME_DISTRICT, districtKey(districtID));
        double w_tax = warehouse.w_tax;
        double d_tax = district.d_tax;
        int d_next_o_id = district.d_next_o_id;
        // district.d_next_o_id = d_next_o_id + 1;
        district.d_next_o_id = d_next_o_id % 1000 + 3001; // for dummy insert
        sm.insertTuple(warehouseId, TABLENAME_DISTRICT,
                districtKey(districtID), district);

        gotOwner = waitForOwner(
                getKeyWTTableWTparition(TABLENAME_CUSTOMER,
                        customerKey(customerID, districtID),
                        Long.valueOf(warehouseId)), txnId);
        if (!gotOwner) {
            if (eng.restartAborted) {
                abortAndRestartTxn(txnId, this);
            }
            else {
                abortTxn(txnId);
            }
            return;
        }
        Customer customer = (Customer) sm.readTuple(warehouseId,
                TABLENAME_CUSTOMER, customerKey(customerID, districtID));

        int orderkey = orderKey(districtID, d_next_o_id);
        Oorder order = new Oorder();
        order.o_d_id = districtID;
        order.o_c_id = customerID;
        order.o_id = d_next_o_id;
        order.o_w_id = warehouseId;
        order.o_carrier_id = 0;
        order.o_ol_cnt = numItems;
        order.o_all_local = allLocal;
        order.o_entry_d = System.currentTimeMillis();

        sg.edu.nus.ownertxn.tpcc.record.NewOrder newOrder = new sg.edu.nus.ownertxn.tpcc.record.NewOrder();
        newOrder.no_w_id = warehouseId;
        newOrder.no_d_id = districtID;
        newOrder.no_o_id = d_next_o_id;
        sm.insertTuple(warehouseId, TABLENAME_OPENORDER, orderkey, order);
        sm.insertTuple(warehouseId, TABLENAME_NEWORDER, orderkey, newOrder);

        for (int i = 0; i < numItems; ++i) {
            Item item = (Item) sm.readTuple(warehouseId, TABLENAME_ITEM,
                    itemKey(itemIDs[i]));

            gotOwner = waitForOwner(
                    getKeyWTTableWTparition(TABLENAME_STOCK,
                            stockKey(itemIDs[i]),
                            (long) supplierWarehouseIDs[i]), txnId);
            if (!gotOwner) {
                if (eng.restartAborted) {
                    abortAndRestartTxn(txnId, this);
                }
                else {
                    abortTxn(txnId);
                }
                return;
            }
            Stock stock = (Stock) sm.readTuple(supplierWarehouseIDs[i],
                    TABLENAME_STOCK, stockKey(itemIDs[i]));

            if (stock.s_quantity - orderQuantities[i] < 10) {
                stock.s_quantity = stock.s_quantity + 10 - orderQuantities[i];
                sm.updateTuple(supplierWarehouseIDs[i], TABLENAME_STOCK,
                        stockKey(itemIDs[i]), stock);
            }
            else {
                stock.s_quantity = stock.s_quantity - orderQuantities[i];
                sm.updateTuple(supplierWarehouseIDs[i], TABLENAME_STOCK,
                        stockKey(itemIDs[i]), stock);
            }
            OrderLine orderLine = new OrderLine();
            orderLine.ol_w_id = warehouseId;
            orderLine.ol_d_id = districtID;
            orderLine.ol_o_id = d_next_o_id;
            orderLine.ol_number = i;
            orderLine.ol_i_id = orderQuantities[i];
            sm.insertTuple(warehouseId, TABLENAME_ORDERLINE,
                    orderlineKey(districtID, d_next_o_id, i), orderLine);
        }

        commitTxn(txnId, runCount);
        ++_commitCount;
    }
}
