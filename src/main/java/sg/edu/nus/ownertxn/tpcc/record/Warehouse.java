package sg.edu.nus.ownertxn.tpcc.record;

public class Warehouse extends sg.edu.nus.ownertxn.Tuple
{
    public Warehouse() {}

    public Warehouse(String str) {
        String[] strSplit = str.split(",");
        w_id = Integer.valueOf(strSplit[0]);
        w_ytd = Float.valueOf(strSplit[1]);
        w_tax = Double.valueOf(strSplit[2]);
    }

    public int w_id; // PRIMARY KEY
    public float w_ytd;
    public double w_tax;
    public String w_name;
    public String w_street_1;
    public String w_street_2;
    public String w_city;
    public String w_state;
    public String w_zip;

    public int size() {
        return 16 + w_name.length() + w_street_1.length() + w_street_2.length()
                + w_city.length() + w_state.length() + w_zip.length();
    }

    @Override
    public String toString() {
        return ("WAREHOUSE(" + w_id + "," + w_ytd + "," + w_tax + ")");
    }
}