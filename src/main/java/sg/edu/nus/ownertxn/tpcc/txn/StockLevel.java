package sg.edu.nus.ownertxn.tpcc.txn;

import java.util.HashSet;
import java.util.Set;

import static sg.edu.nus.ownertxn.TxnUtil.*;
import sg.edu.nus.ownertxn.tpcc.TPCCLoader;
import sg.edu.nus.ownertxn.tpcc.TpccStorageMgr;
import sg.edu.nus.ownertxn.tpcc.TpccTxnExecEngine;
import static sg.edu.nus.ownertxn.tpcc.TPCCConfig.configDistPerWhse;
import static sg.edu.nus.ownertxn.tpcc.TPCCConstants.*;
import static sg.edu.nus.ownertxn.tpcc.TPCCPrimaryKey.*;
import static sg.edu.nus.ownertxn.tpcc.TPCCUtil.*;
import sg.edu.nus.ownertxn.tpcc.record.*;

public class StockLevel extends AnyTpccTxn
{
    public StockLevel(TpccTxnExecEngine eng) {
        super(eng);
        initTxn(txnId);
    }

    public void run() {
        ++runCount;
        /* prepare */
        TpccStorageMgr sm = eng.storeMgr;
        int warehouseId = randomNumber(sm.startLocalWhseId, sm.endLocalWhseId,
                TPCCLoader.gen);

        int districtID = randomNumber(1, configDistPerWhse, TPCCLoader.gen);

        int threhold = randomNumber(10, 20, TPCCLoader.gen);

        /* execute */
        startTxn(txnId);

        District district = (District) sm.readTuple(warehouseId,
                TABLENAME_DISTRICT, districtKey(districtID));
        Integer d_next_o_id = district.d_next_o_id - 1;
        Oorder oorder = (Oorder) sm.readTuple(warehouseId, TABLENAME_OPENORDER,
                orderKey(districtID, d_next_o_id));
        int numItems = oorder.o_ol_cnt;
        Set<Integer> itemOutOfStock = new HashSet<Integer>();
        for (int i = d_next_o_id - 20; i <= d_next_o_id; ++i) {
            for (int j = 0; j < numItems; ++j) {
                OrderLine orderline = (OrderLine) sm.readTuple(warehouseId,
                        TABLENAME_ORDERLINE, orderlineKey(districtID, i, j));
                Integer itemid = orderline.ol_i_id;

                Stock stock = (Stock) sm.readTuple(warehouseId,
                        TABLENAME_STOCK, stockKey(itemid));
                if (stock.s_quantity < threhold) {
                    itemOutOfStock.add(itemid);
                }
            }
        }

        commitTxn(txnId, runCount);
    }
}