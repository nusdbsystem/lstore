package sg.edu.nus.ownertxn.tpcc.record;

import java.sql.Timestamp;

public class History extends sg.edu.nus.ownertxn.Tuple
{
    public History() {}

    public History(String str) {
        String[] strSplit = str.split(",");
        h_c_id = Integer.valueOf(strSplit[0]);
        h_c_d_id = Integer.valueOf(strSplit[1]);
        h_c_w_id = Integer.valueOf(strSplit[2]);
        h_d_id = Integer.valueOf(strSplit[3]);
        h_w_id = Integer.valueOf(strSplit[4]);
        h_amount = Float.valueOf(strSplit[5]);
    }

    public int h_c_id;
    public int h_c_d_id;
    public int h_c_w_id;
    public int h_d_id;
    public int h_w_id;
    public Timestamp h_date;
    public float h_amount;
    public String h_data;

    public int size() {
        return 45;
    }

    @Override
    public String toString() {
        return ("HISTORY(" + h_c_id + "," + h_c_d_id + "," + h_c_w_id + ","
                + h_d_id + "," + h_w_id + "," + h_amount + ")");
    }
}
