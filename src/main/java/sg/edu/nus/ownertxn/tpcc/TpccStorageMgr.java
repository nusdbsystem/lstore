package sg.edu.nus.ownertxn.tpcc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static java.util.Collections.synchronizedMap;

import static qlib.CastUtils.*;
import static qlib.Logger.*;
import sg.edu.nus.ownertxn.Tuple;
import static sg.edu.nus.ownertxn.tpcc.TPCCConstants.*;
import static sg.edu.nus.ownertxn.tpcc.TPCCPrimaryKey.*;
import sg.edu.nus.ownertxn.tpcc.record.*;

public class TpccStorageMgr extends sg.edu.nus.ownertxn.StorageMgr
{
    public final int startLocalWhseId;
    public final int endLocalWhseId;

    public enum SecondaryIndexType {
        IDX_ORDERS(0);

        public int index;

        SecondaryIndexType(int i) {
            this.index = i;
        }
    }

    private class SecondaryIndex<T>
    {
        private Map<T, List<Integer>> map;

        SecondaryIndex() {
            map = new HashMap<T, List<Integer>>();
        }

        public void put(T key, Integer value) {
            if (this.map.get(key) == null) {
                this.map.put(key, new ArrayList<Integer>());
            }
            this.map.get(key).add(value);
        }

        public List<Integer> get(T key) {
            return this.map.get(key);
        }
    }

    public List<List<SecondaryIndex>> secondaryIndex;

    public TpccStorageMgr(int myId, Map<String, String> conf) {
        super(myId, conf);
        secondaryIndex = new ArrayList<List<SecondaryIndex>>();
        for (int i = 0; i < numTotalWhse; ++i) {
            secondaryIndex.add(i, new ArrayList<SecondaryIndex>());
            for (SecondaryIndexType iter : SecondaryIndexType.values()) {
                secondaryIndex.get(i).add(iter.index,
                        new SecondaryIndex<Integer>());
            }
            data.get(i).put(TABLENAME_ITEM,
                    synchronizedMap(new HashMap<Integer, Tuple>()));
            data.get(i).put(TABLENAME_WAREHOUSE,
                    synchronizedMap(new HashMap<Integer, Tuple>()));
            data.get(i).put(TABLENAME_CUSTOMER,
                    synchronizedMap(new HashMap<Integer, Tuple>()));
            data.get(i).put(TABLENAME_DISTRICT,
                    synchronizedMap(new HashMap<Integer, Tuple>()));
            data.get(i).put(TABLENAME_HISTORY,
                    synchronizedMap(new HashMap<Integer, Tuple>()));
            data.get(i).put(TABLENAME_NEWORDER,
                    synchronizedMap(new HashMap<Integer, Tuple>()));
            data.get(i).put(TABLENAME_OPENORDER,
                    synchronizedMap(new HashMap<Integer, Tuple>()));
            data.get(i).put(TABLENAME_ORDERLINE,
                    synchronizedMap(new HashMap<Integer, Tuple>()));
            data.get(i).put(TABLENAME_STOCK,
                    synchronizedMap(new HashMap<Integer, Tuple>()));
        }

        startLocalWhseId = myId * numWhsePerPartition + 1;
        endLocalWhseId = (myId + 1) * numWhsePerPartition;
        TPCCPrimaryKey.numWhsePerPartition = numWhsePerPartition;

        double inSituDataPercent = getDouble(conf.get("inSituDataPercent"));
        TPCCUtil.setInSituDataPercent(inSituDataPercent);
        info("In-situ data: " + (inSituDataPercent * 100) + "%");

        TPCCUtil.setLocalWhseIdRange(startLocalWhseId, endLocalWhseId);
        TPCCUtil.setNumTotalWhse(numTotalWhse);
        TPCCUtil.setNumWhsePerPartition(numWhsePerPartition);
        info("Local warehouse IDs: #" + startLocalWhseId + " - #"
                + endLocalWhseId + " (" + numWhsePerPartition + "/"
                + numTotalWhse + " total)");
    }

    public void insertTuple(Long key, Tuple values) {
        Integer partitionNum = getPartition(key);
        String tableName = getTable(key);
        Integer tableKey = getKeyWOTableWOpartition(key);
        insertTuple(partitionNum, tableName, tableKey, values);
    }

    @SuppressWarnings("unchecked")
    public void insertTuple(Integer partitionNum, String tableName,
            Integer key, Tuple values) {
        data.get(partitionNum - 1).get(tableName).put(key, values);

        switch (tableName) {
        case TPCCConstants.TABLENAME_OPENORDER:
            this.secondaryIndex
                    .get(partitionNum - 1)
                    .get(SecondaryIndexType.IDX_ORDERS.index)
                    .put(customerKey(((Oorder) values).o_c_id,
                            ((Oorder) values).o_d_id), key);
            break;

        default:
            break;
        }
    }

    public void deleteTuple(Long key) {
        Integer partitionNum = getPartition(key);
        String tableName = getTable(key);
        Integer tableKey = getKeyWOTableWOpartition(key);
        deleteTuple(partitionNum, tableName, tableKey);
    }

    public void deleteTuple(Integer partitionNum, String tableName, Integer key) {
        data.get(partitionNum - 1).get(tableName).remove(key);
    }

    public Tuple readTuple(Long key) {
        Integer partitionNum = getPartition(key);
        String tableName = getTable(key);
        Integer tableKey = getKeyWOTableWOpartition(key);
        return readTuple(partitionNum, tableName, tableKey);
    }

    public Tuple readTuple(Integer partitonNum, String tableName, Integer key) {
        return data.get(partitonNum - 1).get(tableName).get(key);
    }

    public Tuple[] readTuple(Integer partitonNum, String tableName,
            Integer key, SecondaryIndexType index) {
        @SuppressWarnings("unchecked")
        ArrayList<Integer> keys = (ArrayList<Integer>) this.secondaryIndex
                .get(partitonNum - 1).get(index.index).get(key);
        Tuple[] result = new Tuple[keys.size()];
        for (int i = 0; i < keys.size(); i++) {
            result[i] = data.get(partitonNum - 1).get(tableName)
                    .get(keys.get(i));
        }
        return result;
    }

    public Tuple readTuple(Integer partitonNum, String tableName) {
        synchronized(this) {
        return data.get(partitonNum - 1)
                .get(tableName).entrySet().iterator().next().getValue();

        // for (Map.Entry<Integer, Tuple> tmp : data.get(partitonNum - 1)
        //         .get(tableName).entrySet()) {
        //     return tmp.getValue();
        // }
        // return null;
            }
    }

    public void updateTuple(Long key, Tuple values) {
        Integer partitionNum = getPartition(key);
        String tableName = getTable(key);
        Integer tableKey = getKeyWOTableWOpartition(key);
        updateTuple(partitionNum, tableName, tableKey, values);
    }

    public void updateTuple(Integer partitionNum, String tableName,
            Integer key, Tuple values) {
        data.get(partitionNum - 1).get(tableName).put(key, values);
    }

    public boolean hasTuple(Long key) {
        Integer partitionNum = getPartition(key);
        String tableName = getTable(key);
        Integer tableKey = getKeyWOTableWOpartition(key);
        return hasTuple(partitionNum, tableName, tableKey);
    }

    public boolean hasTuple(Integer partitionNum, String tableName,
            Integer tableKey) {
        return data.get(partitionNum - 1).get(tableName).containsKey(tableKey);
    }

    public int getPartitionId(Long key) {
        return getRealPartition(key);
    }
}
