package sg.edu.nus.ownertxn.tpcc.txn;

import static sg.edu.nus.ownertxn.TxnUtil.*;
import sg.edu.nus.ownertxn.tpcc.TPCCLoader;
import sg.edu.nus.ownertxn.tpcc.TpccStorageMgr;
import sg.edu.nus.ownertxn.tpcc.TpccTxnExecEngine;
import static sg.edu.nus.ownertxn.tpcc.TPCCConfig.configDistPerWhse;
import static sg.edu.nus.ownertxn.tpcc.TPCCConstants.*;
import static sg.edu.nus.ownertxn.tpcc.TPCCPrimaryKey.*;
import static sg.edu.nus.ownertxn.tpcc.TPCCUtil.*;
import sg.edu.nus.ownertxn.tpcc.TpccStorageMgr.SecondaryIndexType;
import sg.edu.nus.ownertxn.tpcc.record.*;

public class OrderStatus extends AnyTpccTxn
{
    public OrderStatus(TpccTxnExecEngine eng) {
        super(eng);
        initTxn(txnId);
    }

    public void run() {
        ++runCount;
        /* prepare */
        TpccStorageMgr sm = eng.storeMgr;
        int warehouseId = randomNumber(sm.startLocalWhseId, sm.endLocalWhseId,
                TPCCLoader.gen);
        int districtID = randomNumber(1, configDistPerWhse, TPCCLoader.gen);
        int customerID = getCustomerID(TPCCLoader.gen);

        /* execute */
        startTxn(txnId);

        Customer customer = (Customer) sm.readTuple(warehouseId,
                TABLENAME_CUSTOMER, customerKey(customerID, districtID));

        Oorder[] oorder = (Oorder[]) sm.readTuple(warehouseId,
                TABLENAME_OPENORDER, customerKey(customerID, districtID),
                SecondaryIndexType.IDX_ORDERS);

        Oorder lastOorder = oorder[oorder.length-1];
        Integer o_id_last = lastOorder.o_id;
        OrderLine[] orderlines = new OrderLine[lastOorder.o_ol_cnt];
        for (int i = 0; i < lastOorder.o_ol_cnt; ++i) {
            orderlines[i] = (OrderLine) sm.readTuple(warehouseId,
                    TABLENAME_ORDERLINE,
                    orderlineKey(districtID, o_id_last, i));
        }

        commitTxn(txnId, runCount);
    }
}