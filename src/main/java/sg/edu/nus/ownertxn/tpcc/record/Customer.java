package sg.edu.nus.ownertxn.tpcc.record;

import java.sql.Timestamp;

public class Customer extends sg.edu.nus.ownertxn.Tuple
{
    public Customer() {}

    public Customer(String str) {
        String[] strSplit = str.split(",");
        c_id = Integer.valueOf(strSplit[0]);
        c_d_id = Integer.valueOf(strSplit[1]);
        c_w_id = Integer.valueOf(strSplit[2]);
        c_payment_cnt = Integer.valueOf(strSplit[3]);
        c_delivery_cnt = Integer.valueOf(strSplit[4]);
        c_discount = Float.valueOf(strSplit[5]);
        c_credit_lim = Float.valueOf(strSplit[6]);
        c_balance = Float.valueOf(strSplit[7]);
        c_ytd_payment = Float.valueOf(strSplit[8]);
        c_credit = strSplit[9];
    }

    public int c_id;
    public int c_d_id;
    public int c_w_id;
    public int c_payment_cnt;
    public int c_delivery_cnt;
    public Timestamp c_since;
    public float c_discount;
    public float c_credit_lim;
    public float c_balance;
    public float c_ytd_payment;
    public String c_credit;
    public String c_last;
    public String c_first;
    public String c_street_1;
    public String c_street_2;
    public String c_city;
    public String c_state;
    public String c_zip;
    public String c_phone;
    public String c_middle;
    public String c_data;

    public int size() {
        return 500;
    }

    @Override
    public String toString() {
        return ("CUSTOMER(" + c_id + "," + c_d_id + "," + c_w_id + ","
                + c_payment_cnt + "," + c_delivery_cnt + "," + c_discount + ","
                + c_credit_lim + "," + c_balance + "," + c_ytd_payment + ","
                + c_credit + ")");
    }
}
