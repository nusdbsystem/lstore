package sg.edu.nus.ownertxn.tpcc;

import qlib.RichRandom;
import static qlib.CastUtils.*;
import static qlib.Logger.*;
import sg.edu.nus.ownertxn.TxnExecEngine;
import sg.edu.nus.ownertxn.TxnSvr;
import sg.edu.nus.ownertxn.tpcc.txn.*;

public class TpccTxnExecEngine extends TxnExecEngine
{
    public final TpccStorageMgr storeMgr;
    public final int numWhsePerPartition;
    public final double distrNewOrder;
    public final double distrPayment;
    public final double migratePercent;

    public int remoteAccessPattern;
    public static final int UNDEF_REMOTE_ACCESS = 0;
    public static final int REMOTE_RAND_ACCESS = 1;
    public static final int REMOTE_BINEI_ACCESS = 2;
    public static final int REMOTE_NEI_ACCESS = 3;

    public TpccTxnExecEngine(TxnSvr txnSvr) {
        super(txnSvr);
        storeMgr = (TpccStorageMgr) txnSvr.getStoreMgr();
        numWhsePerPartition = getInt(conf.get("numWarehouses"));
        // distrNewOrder = getDouble(conf.get("distrNewOrder"));
        distrNewOrder = distr;
        // distrPayment = getDouble(conf.get("distrPayment"));
        distrPayment = distr;
        migratePercent = getDouble(conf.get("migratePercent"));
        remoteAccessPattern = getInt(conf.get("remoteAccessPattern"));
    }

    private boolean _isLoadingDone = false;

    public boolean isLoadingDone() {
        return _isLoadingDone;
    }

    @Override
    protected void prepare() {
        (new TPCCLoader(storeMgr)).load();
        _isLoadingDone = true;
    }

    @Override
    protected void migrate() {
        Thread threadMigrateStock = new Thread(new MigrateStock(this));
        threadMigrateStock.start();
        Thread threadMigrateCustomer = new Thread(new MigrateCustomer(this));
        threadMigrateCustomer.start();

        try {
            threadMigrateStock.join();
            threadMigrateCustomer.join();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void execute() throws Exception {
        boolean shiftRemoteAccess = getBoolean(conf.get("shiftRemoteAccess"));
        final long neiMillis = getLong(conf.get("remoteNeiTime")) * 1000;
        final long bineiMillis = getLong(conf.get("remoteBineiTime")) * 1000;
        final long randMillis = getLong(conf.get("remoteRandTime")) * 1000;
        final long shiftPeriodMillis = neiMillis + bineiMillis + randMillis;
        long neiEndMillis = neiMillis;
        long bineiEndMillis = neiEndMillis + bineiMillis;
        long randEndMillis = bineiEndMillis + randMillis;
        int lastRemoteAccessPattern = UNDEF_REMOTE_ACCESS;
        RichRandom rand = new RichRandom();

        tk.start();
        long tickMillis;
        long startMillis = System.currentTimeMillis();
        while ((tickMillis = tk.waitUntilNextTick()) > 0) {
            if (shiftRemoteAccess) {
                if (tickMillis <= neiEndMillis) {
                    remoteAccessPattern = REMOTE_NEI_ACCESS;
                }
                else if (tickMillis <= bineiEndMillis) {
                    remoteAccessPattern = REMOTE_BINEI_ACCESS;
                }
                else if (tickMillis < randEndMillis) {
                    remoteAccessPattern = REMOTE_RAND_ACCESS;
                }
                else {
                    neiEndMillis += shiftPeriodMillis;
                    bineiEndMillis += shiftPeriodMillis;
                    randEndMillis += shiftPeriodMillis;
                }
            }
            if (lastRemoteAccessPattern != remoteAccessPattern) {
                info("remote access pattern: " + remoteAccessPattern);
                lastRemoteAccessPattern = remoteAccessPattern;
            }

            if (stress) {
                int taskQueueSize = taskQueue.size();
                if (taskQueueSize > taskQueueLimit) {
                    shrinkBatchSizePerTick();
                    continue;
                }
                else if (taskQueueSize < taskQueueHalf) {
                    raiseBatchSizePerTick();
                }
            }

            for (int i = 0; i < batchSizePerTick; ++i) {
                int choice = rand.nextInt(88);
                // int choice = rand.nextInt(88, 92);
                if (choice < 45) { // 45%
                    threadPool.execute(new NewOrder(this));
                }
                else if (choice < 88) { // 43%
                    threadPool.execute(new Payment(this));
                }
                else if (choice < 92) { // 4%
                    threadPool.execute(new Delivery(this));
                }
                else if (choice < 96) { // 4%
                    threadPool.execute(new OrderStatus(this));
                }
                else if (choice < 100) { // 4%
                    threadPool.execute(new StockLevel(this));
                }
                else {
                    warn("dice overflow: " + choice);
                }
            }
        }
        double minutes = (System.currentTimeMillis() - startMillis) / 60000.0;
        double tpmC = NewOrder.getCommitCount() / minutes;
        info("tpmC: " + (int) tpmC);
    }
}