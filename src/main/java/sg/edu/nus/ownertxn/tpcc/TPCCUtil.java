package sg.edu.nus.ownertxn.tpcc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

import static sg.edu.nus.ownertxn.tpcc.TPCCConfig.*;
import sg.edu.nus.ownertxn.tpcc.record.Customer;

public class TPCCUtil
{
    private static final RandomGenerator _rg = new RandomGenerator(0);

    private static final int DF = 1000; // decimal factor
    private static double localItemPercent = 0.8;
    private static double distrItemPercent = (DF - localItemPercent * DF) / DF;
    private static int limitLocalItemID = (int) (configItemCount * localItemPercent);

    private static double localCustPercent = 0.8;
    private static double distrCustPercent = (DF - localCustPercent * DF) / DF;
    private static int limitLocalCustID = (int) (configCustPerDist * localCustPercent);

    public static void setInSituDataPercent(double inSituDataPercent) {
        localItemPercent = inSituDataPercent;
        distrItemPercent = (DF - localItemPercent * DF) / DF;
        limitLocalItemID = (int) (configItemCount * localItemPercent);

        localCustPercent = inSituDataPercent;
        distrCustPercent = (DF - localCustPercent * DF) / DF;
        limitLocalCustID = (int) (configCustPerDist * localCustPercent);
    }

    private static int startLocalWhseId = 0;
    private static int endLocalWhseId = 0;

    public static void setLocalWhseIdRange(int startId, int endId) {
        startLocalWhseId = startId;
        endLocalWhseId = endId;
    }

    private static int numTotalWhse = 0;

    public static void setNumTotalWhse(int whseCount) {
        numTotalWhse = whseCount;
    }

    private static int numWhsePerPartition = 0;

    public static void setNumWhsePerPartition(int whseCountPerPartition) {
        numWhsePerPartition = whseCountPerPartition;
    }

    /**
     * Access a remote warehouse chosen at random.
     */
    public static int getRandomRemoteWhseId() {
        int whseId = randomNumber(1, numTotalWhse, TPCCLoader.gen);
        while (startLocalWhseId <= whseId && whseId <= endLocalWhseId) {
            whseId = randomNumber(1, numTotalWhse, TPCCLoader.gen);
        }
        return whseId;
    }

    /**
     * Access the bi-neighboring warehouse.
     */
    public static int getBiNeighborRemoteWhseId(int myWhseId) {
        int whseId = (myWhseId + (TPCCLoader.gen.nextBoolean() ? 1 : -1)
                * numWhsePerPartition * 2 - 1)
                % numTotalWhse + 1;
        if (whseId < 1) {
            whseId += numTotalWhse;
        }
        return whseId;
    }

    /**
     * Access the neighboring warehouse.
     */
    public static int getNeighborRemoteWhseId(int myWhseId) {
        return (myWhseId + numWhsePerPartition - 1) % numTotalWhse + 1;
    }

    public static String randomStr(int strLen) {
        if (strLen > 1)
            return _rg.astring(strLen - 1, strLen - 1);
        else
            return "";
    }

    public static String randomNStr(int stringLength) {
        if (stringLength > 0)
            return _rg.nstring(stringLength, stringLength);
        else
            return "";
    }

    public static String getCurrentTime() {
        return dateFormat.format(new java.util.Date());
    }

    public static String formattedDouble(double d) {
        String dS = String.valueOf(d);
        return dS.length() > 6 ? dS.substring(0, 6) : dS;
    }

    // TODO: TPCC-C 2.1.6: For non-uniform random number generation, the
    // constants for item id,
    // customer id and customer name are supposed to be selected ONCE and reused
    // for all terminals.
    // We just hardcode one selection of parameters here, but we should generate
    // these each time.
    private static final int OL_I_ID_C = 7911; // in range [0, 8191]
    private static final int C_ID_C = 259; // in range [0, 1023]
    // NOTE: TPC-C 2.1.6.1 specifies that abs(C_LAST_LOAD_C - C_LAST_RUN_C) must
    // be within [65, 119]
    private static final int C_LAST_LOAD_C = 157; // in range [0, 255]
    private static final int C_LAST_RUN_C = 223; // in range [0, 255]

    public static int getItemID(Random r) {
        return nonUniformRandom(8191, OL_I_ID_C, 1, configItemCount, r);
    }

    public static int getLocalItemID(Random r) {
        return (int) Math.ceil(getItemID(r) * localItemPercent);
    }

    public static int getDistrItemID(Random r) {
        return (int) Math.ceil(getItemID(r) * distrItemPercent)
                + limitLocalItemID;
    }

    public static int getCustomerID(Random r) {
        return nonUniformRandom(1023, C_ID_C, 1, configCustPerDist, r);
    }

    public static int getLocalCustomerID(Random r) {
        return (int) Math.ceil(getCustomerID(r) * localCustPercent);
    }

    public static int getDistrCustomerID(Random r) {
        return (int) Math.ceil(getCustomerID(r) * distrCustPercent)
                + limitLocalCustID;
    }

    public static String getLastName(int num) {
        return nameTokens[num / 100] + nameTokens[(num / 10) % 10]
                + nameTokens[num % 10];
    }

    public static String getNonUniformRandomLastNameForRun(Random r) {
        return getLastName(nonUniformRandom(255, C_LAST_RUN_C, 0, 999, r));
    }

    public static String getNonUniformRandomLastNameForLoad(Random r) {
        return getLastName(nonUniformRandom(255, C_LAST_LOAD_C, 0, 999, r));
    }

    public static int randomNumber(int min, int max, Random r) {
        return (int) (r.nextDouble() * (max - min + 1) + min);
    }

    public static int nonUniformRandom(int cA, int cC, int min, int max,
            Random r) {
        return (((randomNumber(0, cA, r) | randomNumber(min, max, r)) + cC) % (max
                - min + 1))
                + min;
    }
}