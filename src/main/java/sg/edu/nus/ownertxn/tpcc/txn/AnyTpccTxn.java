package sg.edu.nus.ownertxn.tpcc.txn;

import sg.edu.nus.ownertxn.tpcc.TpccTxnExecEngine;

public abstract class AnyTpccTxn implements Runnable
{
    protected final TpccTxnExecEngine eng;
    public final Long txnId;
    protected int runCount = 0;

    public AnyTpccTxn(TpccTxnExecEngine eng) {
        this.eng = eng;
        txnId = eng.genTxnId();
    }
}