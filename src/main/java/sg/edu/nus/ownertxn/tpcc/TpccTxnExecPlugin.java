package sg.edu.nus.ownertxn.tpcc;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static qlib.Logger.*;
import sg.edu.nus.ownertxn.TxnSvr;
import sg.edu.nus.ownertxn.tpcc.txn.*;

public class TpccTxnExecPlugin extends TpccTxnExecEngine
{
    public static enum TxnType {
        INVALID, NEW_ORDER, PAYMENT, DELIVERY, ORDER_STATUS, STOCK_LEVEL
    }

    private final BlockingQueue<TxnType> _bqueue;

    public TpccTxnExecPlugin(TxnSvr txnSvr) {
        super(txnSvr);
        _bqueue = new LinkedBlockingQueue<TxnType>();
    }

    @Override
    protected void execute() throws Exception {
        for (;;) {
            switch (_bqueue.take()) {
            case NEW_ORDER:
                threadPool.execute(new NewOrder(this));
                break;

            case PAYMENT:
                threadPool.execute(new Payment(this));
                break;

            case DELIVERY:
                threadPool.execute(new Delivery(this));
                break;

            case ORDER_STATUS:
                threadPool.execute(new OrderStatus(this));
                break;

            case STOCK_LEVEL:
                threadPool.execute(new StockLevel(this));
                break;

            default:
                error("invalid transaction type");
                break;
            }
        }
    }

    public void addTxn(TxnType txnTypeId) {
        try {
            _bqueue.add(txnTypeId);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}