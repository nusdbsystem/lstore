package sg.edu.nus.ownertxn.tpcc.record;

public class Stock extends sg.edu.nus.ownertxn.Tuple
{
    public Stock() {}

    public Stock(String str) {
        String[] strSplit = str.split(",");
        s_i_id = Integer.valueOf(strSplit[0]);
        s_w_id = Integer.valueOf(strSplit[1]);
        s_order_cnt = Integer.valueOf(strSplit[2]);
        s_remote_cnt = Integer.valueOf(strSplit[3]);
        s_quantity = Integer.valueOf(strSplit[4]);
        s_ytd = Float.valueOf(strSplit[5]);
    }

    public int s_i_id; // PRIMARY KEY 2
    public int s_w_id; // PRIMARY KEY 1
    public int s_order_cnt;
    public int s_remote_cnt;
    public int s_quantity;
    public float s_ytd;
    public String s_data;
    public String s_dist_01;
    public String s_dist_02;
    public String s_dist_03;
    public String s_dist_04;
    public String s_dist_05;
    public String s_dist_06;
    public String s_dist_07;
    public String s_dist_08;
    public String s_dist_09;
    public String s_dist_10;

    public int size() {
        return 280;
    }

    @Override
    public String toString() {
        return ("STOCK(" + s_i_id + "," + s_w_id + "," + s_order_cnt + ","
                + s_remote_cnt + "," + s_quantity + "," + s_ytd + ")");
    }
}