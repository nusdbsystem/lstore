package sg.edu.nus.ownertxn.tpcc.record;

public class DummyRecord extends sg.edu.nus.ownertxn.Tuple
{
    public DummyRecord() {}

    public int size() {
        return 0;
    }

    @Override
    public String toString() {
        return "DummyRecord";
    }
}
