package sg.edu.nus.ownertxn.tpcc;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Random;

import static qlib.Logger.*;
import static sg.edu.nus.ownertxn.tpcc.TPCCConfig.*;
import sg.edu.nus.ownertxn.tpcc.record.*;

public class TPCCLoader
{
    TpccStorageMgr sm;

    public TPCCLoader(TpccStorageMgr sm) {
        this.sm = sm;
        outputFiles = false;
    }

    private static java.util.Date now = null;
    private static java.util.Date startDate = null;
    private static java.util.Date endDate = null;

    public static Random gen;
    private static String fileLocation = "";
    private static boolean outputFiles = true;
    private static PrintWriter out = null;
    private static long lastTimeMS = 0;

    private static final int FIRST_UNPROCESSED_O_ID = 2101;

    protected int loadItem(int itemKount) {

        int k = 0;
        int t = 0;
        int randPct = 0;
        int len = 0;
        int startORIGINAL = 0;

        try {
            now = new java.util.Date();
            t = itemKount;
            debug("\nStart Item Load for " + t + " Items @ " + now + " ...");

            if (outputFiles == true) {
                out = new PrintWriter(new FileOutputStream(fileLocation
                        + "item.csv"));
                debug("\nWriting Item file to: " + fileLocation + "item.csv");
            }

            for (int iter = sm.startLocalWhseId; iter <= sm.endLocalWhseId; ++iter) {
                for (int i = 1; i <= itemKount; ++i) {
                    Item item = new Item();
                    item.i_id = i;
                    item.i_name = TPCCUtil.randomStr(TPCCUtil.randomNumber(14,
                            24, gen));
                    item.i_price = (double) (TPCCUtil.randomNumber(100, 10000,
                            gen) / 100.0);

                    // i_data
                    randPct = TPCCUtil.randomNumber(1, 100, gen);
                    len = TPCCUtil.randomNumber(26, 50, gen);
                    if (randPct > 10) {
                        // 90% of time i_data isa random string of length [26 ..
                        // 50]
                        item.i_data = TPCCUtil.randomStr(len);
                    }
                    else {
                        // 10% of time i_data has "ORIGINAL" crammed somewhere
                        // in
                        // middle
                        startORIGINAL = TPCCUtil
                                .randomNumber(2, (len - 8), gen);
                        item.i_data = TPCCUtil.randomStr(startORIGINAL - 1)
                                + "ORIGINAL"
                                + TPCCUtil.randomStr(len - startORIGINAL - 9);
                    }

                    item.i_im_id = TPCCUtil.randomNumber(1, 10000, gen);

                    k++;

                    String str = item.toString();
                    Integer key = TPCCPrimaryKey.itemKey(item.i_id);
                    if (outputFiles)
                        out.println(str);
                    else
                        sm.insertTuple(iter, TPCCConstants.TABLENAME_ITEM, key,
                                item);

                    if ((k % configCommitCount) == 0) {
                        long tmpTime = new java.util.Date().getTime();
                        String etStr = "  Elasped Time(ms): "
                                + ((tmpTime - lastTimeMS) / 1000.000)
                                + "                    ";
                        debug(etStr.substring(0, 30) + "  Writing record " + k
                                + " of " + t);
                        lastTimeMS = tmpTime;
                    }

                } // end for
            }

            long tmpTime = new java.util.Date().getTime();
            String etStr = "  Elasped Time(ms): "
                    + ((tmpTime - lastTimeMS) / 1000.000)
                    + "                    ";
            debug(etStr.substring(0, 30) + "  Writing record " + k + " of " + t);
            lastTimeMS = tmpTime;

            now = new java.util.Date();
            debug("End Item Load @  " + now);

        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return (k);

    }

    protected int loadWhse() {
        int whseKount = sm.endLocalWhseId - sm.startLocalWhseId + 1;
        try {
            now = new java.util.Date();
            debug("\nStart Whse Load for " + whseKount + " Whses @ " + now
                    + " ...");

            if (outputFiles == true) {
                out = new PrintWriter(new FileOutputStream(fileLocation
                        + "warehouse.csv"));
                debug("\nWriting Warehouse file to: " + fileLocation
                        + "warehouse.csv");
            }

            for (int i = sm.startLocalWhseId; i <= sm.endLocalWhseId; ++i) {
                Warehouse warehouse = new Warehouse();
                warehouse.w_id = i;
                warehouse.w_ytd = 300000;

                // random within [0.0000 .. 0.2000]
                warehouse.w_tax = (double) ((TPCCUtil
                        .randomNumber(0, 2000, gen)) / 10000.0);

                warehouse.w_name = TPCCUtil.randomStr(TPCCUtil.randomNumber(6,
                        10, gen));
                warehouse.w_street_1 = TPCCUtil.randomStr(TPCCUtil
                        .randomNumber(10, 20, gen));
                warehouse.w_street_2 = TPCCUtil.randomStr(TPCCUtil
                        .randomNumber(10, 20, gen));
                warehouse.w_city = TPCCUtil.randomStr(TPCCUtil.randomNumber(10,
                        20, gen));
                warehouse.w_state = TPCCUtil.randomStr(3).toUpperCase();
                warehouse.w_zip = "123456789";

                String str = warehouse.toString();
                Integer key = TPCCPrimaryKey.warehouseKey(warehouse.w_id);
                if (outputFiles == true)
                    out.println(str);
                else
                    sm.insertTuple(i, TPCCConstants.TABLENAME_WAREHOUSE, key,
                            warehouse);
            }

            now = new java.util.Date();

            long tmpTime = new java.util.Date().getTime();
            debug("Elasped Time(ms): " + ((tmpTime - lastTimeMS) / 1000.000));
            lastTimeMS = tmpTime;
            debug("End Whse Load @  " + now);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return (whseKount);
    }

    protected int loadStock(int itemKount) {
        int whseKount = sm.endLocalWhseId - sm.startLocalWhseId + 1;
        int k = 0;
        int t = 0;
        int randPct = 0;
        int len = 0;
        int startORIGINAL = 0;

        try {
            now = new java.util.Date();
            t = (whseKount * itemKount);
            debug("\nStart Stock Load for " + t + " units @ " + now + " ...");

            if (outputFiles == true) {
                out = new PrintWriter(new FileOutputStream(fileLocation
                        + "stock.csv"));
                debug("\nWriting Stock file to: " + fileLocation + "stock.csv");
            }

            for (int i = 1; i <= itemKount; i++) {
                for (int w = sm.startLocalWhseId; w <= sm.endLocalWhseId; ++w) {
                    Stock stock = new Stock();
                    stock.s_i_id = i;
                    stock.s_w_id = w;
                    stock.s_quantity = TPCCUtil.randomNumber(10, 100, gen);
                    stock.s_ytd = 0;
                    stock.s_order_cnt = 0;
                    stock.s_remote_cnt = 0;

                    // s_data
                    randPct = TPCCUtil.randomNumber(1, 100, gen);
                    len = TPCCUtil.randomNumber(26, 50, gen);
                    if (randPct > 10) {
                        // 90% of time i_data isa random string of length [26 ..
                        // 50]
                        stock.s_data = TPCCUtil.randomStr(len);
                    }
                    else {
                        // 10% of time i_data has "ORIGINAL" crammed somewhere
                        // in middle
                        startORIGINAL = TPCCUtil
                                .randomNumber(2, (len - 8), gen);
                        stock.s_data = TPCCUtil.randomStr(startORIGINAL - 1)
                                + "ORIGINAL"
                                + TPCCUtil.randomStr(len - startORIGINAL - 9);
                    }

                    stock.s_dist_01 = TPCCUtil.randomStr(24);
                    stock.s_dist_02 = TPCCUtil.randomStr(24);
                    stock.s_dist_03 = TPCCUtil.randomStr(24);
                    stock.s_dist_04 = TPCCUtil.randomStr(24);
                    stock.s_dist_05 = TPCCUtil.randomStr(24);
                    stock.s_dist_06 = TPCCUtil.randomStr(24);
                    stock.s_dist_07 = TPCCUtil.randomStr(24);
                    stock.s_dist_08 = TPCCUtil.randomStr(24);
                    stock.s_dist_09 = TPCCUtil.randomStr(24);
                    stock.s_dist_10 = TPCCUtil.randomStr(24);

                    k++;
                    String str = stock.toString();
                    Integer key = TPCCPrimaryKey.stockKey(stock.s_i_id);
                    if (outputFiles == true)
                        out.println(str);
                    else
                        sm.insertTuple(w, TPCCConstants.TABLENAME_STOCK, key,
                                stock);

                    if ((k % configCommitCount) == 0) {
                        long tmpTime = new java.util.Date().getTime();
                        String etStr = "  Elasped Time(ms): "
                                + ((tmpTime - lastTimeMS) / 1000.000)
                                + "                    ";
                        debug(etStr.substring(0, 30) + "  Writing record " + k
                                + " of " + t);
                        lastTimeMS = tmpTime;
                    }
                } // end for [w]
            } // end for [i]

            long tmpTime = new java.util.Date().getTime();
            String etStr = "  Elasped Time(ms): "
                    + ((tmpTime - lastTimeMS) / 1000.000)
                    + "                    ";
            debug(etStr.substring(0, 30) + "  Writing final records " + k
                    + " of " + t);
            lastTimeMS = tmpTime;
            now = new java.util.Date();
            debug("End Stock Load @  " + now);

        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return (k);
    }

    protected int loadDist(int distWhseKount) {
        int whseKount = sm.endLocalWhseId - sm.startLocalWhseId + 1;
        int k = 0;
        int t = 0;

        try {
            now = new java.util.Date();

            if (outputFiles == true) {
                out = new PrintWriter(new FileOutputStream(fileLocation
                        + "district.csv"));
                debug("\nWriting District file to: " + fileLocation
                        + "district.csv");
            }

            t = (whseKount * distWhseKount);
            debug("\nStart District Data for " + t + " Dists @ " + now + " ...");

            for (int w = sm.startLocalWhseId; w <= sm.endLocalWhseId; ++w) {
                for (int d = 1; d <= distWhseKount; d++) {
                    District district = new District();

                    district.d_id = d;
                    district.d_w_id = w;
                    district.d_ytd = 30000;

                    // random within [0.0000 .. 0.2000]
                    district.d_tax = (float) ((TPCCUtil.randomNumber(0, 2000,
                            gen)) / 10000.0);

                    district.d_next_o_id = 3001;
                    district.d_name = TPCCUtil.randomStr(TPCCUtil.randomNumber(
                            6, 10, gen));
                    district.d_street_1 = TPCCUtil.randomStr(TPCCUtil
                            .randomNumber(10, 20, gen));
                    district.d_street_2 = TPCCUtil.randomStr(TPCCUtil
                            .randomNumber(10, 20, gen));
                    district.d_city = TPCCUtil.randomStr(TPCCUtil.randomNumber(
                            10, 20, gen));
                    district.d_state = TPCCUtil.randomStr(3).toUpperCase();
                    district.d_zip = "123456789";

                    ++k;
                    String str = district.toString();
                    Integer key = TPCCPrimaryKey.districtKey(district.d_id);
                    if (outputFiles == true)
                        out.println(str);
                    else
                        sm.insertTuple(w, TPCCConstants.TABLENAME_DISTRICT,
                                key, district);
                } // end for [d]
            } // end for [w]

            long tmpTime = new java.util.Date().getTime();
            String etStr = "  Elasped Time(ms): "
                    + ((tmpTime - lastTimeMS) / 1000.000)
                    + "                    ";
            debug(etStr.substring(0, 30) + "  Writing record " + k + " of " + t);
            lastTimeMS = tmpTime;
            now = new java.util.Date();
            debug("End District Load @  " + now);

        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return (k);
    }

    protected int loadCust(int distWhseKount, int custDistKount) {
        int whseKount = sm.endLocalWhseId - sm.startLocalWhseId + 1;
        int k = 0;
        int t = 0;

        PrintWriter outHist = null;

        try {
            now = new java.util.Date();

            if (outputFiles == true) {
                out = new PrintWriter(new FileOutputStream(fileLocation
                        + "customer.csv"));
                debug("\nWriting Customer file to: " + fileLocation
                        + "customer.csv");
                outHist = new PrintWriter(new FileOutputStream(fileLocation
                        + "cust-hist.csv"));
                debug("\nWriting Customer History file to: " + fileLocation
                        + "cust-hist.csv");
            }

            t = (whseKount * distWhseKount * custDistKount * 2);
            debug("\nStart Cust-Hist Load for " + t + " Cust-Hists @ " + now
                    + " ...");

            for (int w = sm.startLocalWhseId; w <= sm.endLocalWhseId; ++w) {
                for (int d = 1; d <= distWhseKount; d++) {

                    for (int c = 1; c <= custDistKount; c++) {
                        Customer customer = new Customer();
                        Timestamp sysdate = new java.sql.Timestamp(
                                System.currentTimeMillis());

                        customer.c_id = c;
                        customer.c_d_id = d;
                        customer.c_w_id = w;

                        // discount is random between [0.0000 ... 0.5000]
                        customer.c_discount = (float) (TPCCUtil.randomNumber(1,
                                5000, gen) / 10000.0);

                        if (TPCCUtil.randomNumber(1, 100, gen) <= 10) {
                            customer.c_credit = "BC"; // 10% Bad Credit
                        }
                        else {
                            customer.c_credit = "GC"; // 90% Good Credit
                        }
                        if (c <= 1000) {
                            customer.c_last = TPCCUtil.getLastName(c - 1);
                        }
                        else {
                            customer.c_last = TPCCUtil
                                    .getNonUniformRandomLastNameForLoad(gen);
                        }
                        customer.c_first = TPCCUtil.randomStr(TPCCUtil
                                .randomNumber(8, 16, gen));
                        customer.c_credit_lim = 50000;

                        customer.c_balance = -10;
                        customer.c_ytd_payment = 10;
                        customer.c_payment_cnt = 1;
                        customer.c_delivery_cnt = 0;

                        customer.c_street_1 = TPCCUtil.randomStr(TPCCUtil
                                .randomNumber(10, 20, gen));
                        customer.c_street_2 = TPCCUtil.randomStr(TPCCUtil
                                .randomNumber(10, 20, gen));
                        customer.c_city = TPCCUtil.randomStr(TPCCUtil
                                .randomNumber(10, 20, gen));
                        customer.c_state = TPCCUtil.randomStr(3).toUpperCase();
                        // TPC-C 4.3.2.7: 4 random digits + "11111"
                        customer.c_zip = TPCCUtil.randomNStr(4) + "11111";

                        customer.c_phone = TPCCUtil.randomNStr(16);

                        customer.c_since = sysdate;
                        customer.c_middle = "OE";
                        customer.c_data = TPCCUtil.randomStr(TPCCUtil
                                .randomNumber(300, 500, gen));

                        History history = new History();
                        history.h_c_id = c;
                        history.h_c_d_id = d;
                        history.h_c_w_id = w;
                        history.h_d_id = d;
                        history.h_w_id = w;
                        history.h_date = sysdate;
                        history.h_amount = 10;
                        history.h_data = TPCCUtil.randomStr(TPCCUtil
                                .randomNumber(10, 24, gen));

                        k = k + 2;

                        String str = customer.toString();
                        Integer key = TPCCPrimaryKey.customerKey(customer.c_id,
                                customer.c_d_id);
                        if (outputFiles == true)
                            out.println(str);
                        else
                            sm.insertTuple(w, TPCCConstants.TABLENAME_CUSTOMER,
                                    key, customer);

                        str = history.toString();
                        key = TPCCPrimaryKey.historyKey(history.h_c_id,
                                history.h_c_d_id);
                        if (outputFiles == true)
                            outHist.println(str);
                        else
                            sm.insertTuple(w, TPCCConstants.TABLENAME_HISTORY,
                                    key, history);

                        if ((k % configCommitCount) == 0) {
                            long tmpTime = new java.util.Date().getTime();
                            String etStr = "  Elasped Time(ms): "
                                    + ((tmpTime - lastTimeMS) / 1000.000)
                                    + "                    ";
                            debug(etStr.substring(0, 30) + "  Writing record "
                                    + k + " of " + t);
                            lastTimeMS = tmpTime;
                        }
                    } // end for [c]
                } // end for [d]
            } // end for [w]

            long tmpTime = new java.util.Date().getTime();
            String etStr = "  Elasped Time(ms): "
                    + ((tmpTime - lastTimeMS) / 1000.000)
                    + "                    ";
            debug(etStr.substring(0, 30) + "  Writing record " + k + " of " + t);
            lastTimeMS = tmpTime;
            now = new java.util.Date();
            if (outputFiles == true) {
                outHist.close();
            }
            debug("End Cust-Hist Data Load @  " + now);

        }
        catch (Exception e) {
            e.printStackTrace();
            if (outputFiles == true) {
                outHist.close();
            }
        }

        return (k);
    }

    protected int loadOrder(int distWhseKount, int custDistKount) {
        int whseKount = sm.endLocalWhseId - sm.startLocalWhseId + 1;
        int k = 0;
        int t = 0;
        PrintWriter outLine = null;
        PrintWriter outNewOrder = null;

        try {
            if (outputFiles == true) {
                out = new PrintWriter(new FileOutputStream(fileLocation
                        + "order.csv"));
                debug("\nWriting Order file to: " + fileLocation + "order.csv");
                outLine = new PrintWriter(new FileOutputStream(fileLocation
                        + "order-line.csv"));
                debug("\nWriting Order Line file to: " + fileLocation
                        + "order-line.csv");
                outNewOrder = new PrintWriter(new FileOutputStream(fileLocation
                        + "new-order.csv"));
                debug("\nWriting New Order file to: " + fileLocation
                        + "new-order.csv");
            }

            now = new java.util.Date();

            t = (whseKount * distWhseKount * custDistKount);
            t = (t * 11) + (t / 3);
            debug("whse=" + whseKount + ", dist=" + distWhseKount + ", cust="
                    + custDistKount);
            debug("\nStart Order-Line-New Load for approx " + t + " rows @ "
                    + now + " ...");

            for (int w = sm.startLocalWhseId; w <= sm.endLocalWhseId; ++w) {

                for (int d = 1; d <= distWhseKount; d++) {
                    // TPC-C 4.3.3.1: o_c_id must be a permutation of [1, 3000]
                    int[] c_ids = new int[custDistKount];
                    for (int i = 0; i < custDistKount; ++i) {
                        c_ids[i] = i + 1;
                    }
                    // Collections.shuffle exists, but there is no
                    // Arrays.shuffle
                    for (int i = 0; i < c_ids.length - 1; ++i) {
                        int remaining = c_ids.length - i - 1;
                        int swapIndex = gen.nextInt(remaining) + i + 1;
                        assert i < swapIndex;
                        int temp = c_ids[swapIndex];
                        c_ids[swapIndex] = c_ids[i];
                        c_ids[i] = temp;
                    }

                    for (int c = 1; c <= custDistKount; c++) {
                        Oorder oorder = new Oorder();
                        oorder.o_id = c;
                        oorder.o_w_id = w;
                        oorder.o_d_id = d;
                        oorder.o_c_id = c_ids[c - 1];
                        // o_carrier_id is set *only* for orders with ids < 2101
                        // [4.3.3.1]
                        if (oorder.o_id < FIRST_UNPROCESSED_O_ID) {
                            oorder.o_carrier_id = TPCCUtil.randomNumber(1, 10,
                                    gen);
                        }
                        else {
                            oorder.o_carrier_id = null;
                        }
                        oorder.o_ol_cnt = 15;
                        oorder.o_all_local = 1;
                        oorder.o_entry_d = System.currentTimeMillis();

                        k++;
                        {// insert
                            String str = oorder.toString();
                            Integer key = TPCCPrimaryKey.orderKey(
                                    oorder.o_d_id, oorder.o_id);
                            if (outputFiles == true)
                                out.println(str);
                            else
                                sm.insertTuple(w,
                                        TPCCConstants.TABLENAME_OPENORDER, key,
                                        oorder);
                        }

                        // 900 rows in the NEW-ORDER table corresponding to the
                        // last
                        // 900 rows in the ORDER table for that district (i.e.,
                        // with
                        // NO_O_ID between 2,101 and 3,000)

                        if (c >= FIRST_UNPROCESSED_O_ID) {
                            NewOrder new_order = new NewOrder();

                            new_order.no_w_id = w;
                            new_order.no_d_id = d;
                            new_order.no_o_id = c;

                            k++;
                            {
                                // insert
                                String str = new_order.toString();
                                Integer key = TPCCPrimaryKey.neworderKey(
                                        new_order.no_d_id, new_order.no_o_id);
                                if (outputFiles == true)
                                    outNewOrder.println(str);
                                else
                                    sm.insertTuple(w,
                                            TPCCConstants.TABLENAME_NEWORDER,
                                            key, new_order);
                            }

                        } // end new order

                        for (int l = 1; l <= oorder.o_ol_cnt; l++) {
                            OrderLine order_line = new OrderLine();
                            order_line.ol_w_id = w;
                            order_line.ol_d_id = d;
                            order_line.ol_o_id = c;
                            order_line.ol_number = l; // ol_number
                            order_line.ol_i_id = TPCCUtil.randomNumber(1,
                                    100000, gen);
                            if (order_line.ol_o_id < FIRST_UNPROCESSED_O_ID) {
                                order_line.ol_delivery_d = oorder.o_entry_d;
                                order_line.ol_amount = 0;
                            }
                            else {
                                order_line.ol_delivery_d = null;
                                // random within [0.01 .. 9,999.99]
                                order_line.ol_amount = (float) (TPCCUtil
                                        .randomNumber(1, 999999, gen) / 100.0);
                            }

                            order_line.ol_supply_w_id = order_line.ol_w_id;
                            order_line.ol_quantity = 5;
                            order_line.ol_dist_info = TPCCUtil.randomStr(24);

                            k++;
                            {// insert
                                String str = order_line.toString();
                                Integer key = TPCCPrimaryKey.orderlineKey(
                                        order_line.ol_d_id, order_line.ol_o_id,
                                        order_line.ol_number);
                                if (outputFiles == true)
                                    outLine.println(str);
                                else
                                    sm.insertTuple(w,
                                            TPCCConstants.TABLENAME_ORDERLINE,
                                            key, order_line);
                            }

                            if ((k % configCommitCount) == 0) {
                                long tmpTime = new java.util.Date().getTime();
                                String etStr = "  Elasped Time(ms): "
                                        + ((tmpTime - lastTimeMS) / 1000.000)
                                        + "                    ";
                                debug(etStr.substring(0, 30)
                                        + "  Writing record " + k + " of " + t);
                                lastTimeMS = tmpTime;
                            }
                        } // end for [l]
                    } // end for [c]
                } // end for [d]
            } // end for [w]

            debug("  Writing final records " + k + " of " + t);
            if (outputFiles == true) {
                outLine.close();
                outNewOrder.close();
            }
            now = new java.util.Date();
            debug("End Orders Load @  " + now);

        }
        catch (Exception e) {
            e.printStackTrace();
            if (outputFiles == true) {
                outLine.close();
                outNewOrder.close();
            }
        }

        return (k);
    }

    public static final class NotImplementedException extends
            UnsupportedOperationException
    {
        private static final long serialVersionUID = 1958656852398867984L;
    }

    public void load() {
        gen = new Random(System.currentTimeMillis());

        startDate = new java.util.Date();
        debug("------------- LoadData Start Date = " + startDate
                + "-------------");

        long startTimeMS = new java.util.Date().getTime();
        lastTimeMS = startTimeMS;

        long totalRows = loadWhse();
        totalRows += loadItem(configItemCount);
        totalRows += loadStock(configItemCount);
        totalRows += loadDist(configDistPerWhse);
        totalRows += loadCust(configDistPerWhse, configCustPerDist);
        totalRows += loadOrder(configDistPerWhse, configCustPerDist);

        long runTimeMS = (new java.util.Date().getTime()) + 1 - startTimeMS;
        endDate = new java.util.Date();
        debug("");
        debug("------------- LoadJDBC Statistics --------------------");
        debug("     Start Time = " + startDate);
        debug("       End Time = " + endDate);
        debug("       Run Time = " + (int) runTimeMS / 1000 + " Seconds");
        debug("    Rows Loaded = " + totalRows + " Rows");
        debug("Rows Per Second = " + (totalRows / (runTimeMS / 1000))
                + " Rows/Sec");
        debug("------------------------------------------------------");

    }
} // end LoadData Class
