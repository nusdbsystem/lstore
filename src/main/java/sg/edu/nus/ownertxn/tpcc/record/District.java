package sg.edu.nus.ownertxn.tpcc.record;

public class District extends sg.edu.nus.ownertxn.Tuple
{
    public District() {}

    public District(String str) {
        String[] strSplit = str.split(",");
        d_id = Integer.valueOf(strSplit[0]);
        d_w_id = Integer.valueOf(strSplit[1]);
        d_next_o_id = Integer.valueOf(strSplit[2]);
        d_ytd = Float.valueOf(strSplit[3]);
        d_tax = Float.valueOf(strSplit[4]);
    }

    public int d_id;
    public int d_w_id;
    public int d_next_o_id;
    public float d_ytd;
    public float d_tax;
    public String d_name;
    public String d_street_1;
    public String d_street_2;
    public String d_city;
    public String d_state;
    public String d_zip;

    public int size() {
        return 20 + d_name.length() + d_street_1.length() + d_street_2.length()
                + d_city.length() + d_state.length() + d_zip.length();
    }

    @Override
    public String toString() {
        return ("DISTRICT(" + d_id + "," + d_w_id + "," + d_next_o_id + ","
                + d_ytd + "," + d_tax + ")");
    }
}
