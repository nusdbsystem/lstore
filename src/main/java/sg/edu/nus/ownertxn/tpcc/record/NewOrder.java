package sg.edu.nus.ownertxn.tpcc.record;

public class NewOrder extends sg.edu.nus.ownertxn.Tuple
{
    public NewOrder() {}

    public NewOrder(String str) {
        String[] strSplit = str.split(",");
        no_w_id = Integer.valueOf(strSplit[0]);
        no_d_id = Integer.valueOf(strSplit[1]);
        no_o_id = Integer.valueOf(strSplit[2]);
    }

    public int no_w_id;
    public int no_d_id;
    public int no_o_id;

    public int size() {
        return 12;
    }

    @Override
    public String toString() {
        return ("NewOrder(" + no_w_id + "," + no_d_id + "," + no_o_id + ")");
    }
}