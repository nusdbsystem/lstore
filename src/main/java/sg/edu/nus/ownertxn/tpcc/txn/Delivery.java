package sg.edu.nus.ownertxn.tpcc.txn;

import java.sql.Timestamp;

import static sg.edu.nus.ownertxn.TxnUtil.*;
import sg.edu.nus.ownertxn.tpcc.TPCCConfig;
import sg.edu.nus.ownertxn.tpcc.TPCCLoader;
import sg.edu.nus.ownertxn.tpcc.TpccStorageMgr;
import sg.edu.nus.ownertxn.tpcc.TpccTxnExecEngine;
import static sg.edu.nus.ownertxn.tpcc.TPCCConfig.configDistPerWhse;
import static sg.edu.nus.ownertxn.tpcc.TPCCConstants.*;
import static sg.edu.nus.ownertxn.tpcc.TPCCPrimaryKey.*;
import static sg.edu.nus.ownertxn.tpcc.TPCCUtil.*;
import sg.edu.nus.ownertxn.tpcc.record.*;

public class Delivery extends AnyTpccTxn
{
    public Delivery(TpccTxnExecEngine eng) {
        super(eng);
        initTxn(txnId);
    }

    public void run() {
        ++runCount;
        /* prepare */
        TpccStorageMgr sm = eng.storeMgr;
        int warehouseId = randomNumber(sm.startLocalWhseId, sm.endLocalWhseId,
                TPCCLoader.gen);
        int carrier = randomNumber(1, 10, TPCCLoader.gen);
        Timestamp time = new Timestamp(System.currentTimeMillis());

        /* execute */
        startTxn(txnId);

        sg.edu.nus.ownertxn.tpcc.record.NewOrder[] tmp = new sg.edu.nus.ownertxn.tpcc.record.NewOrder[configDistPerWhse];
        int[] no_o_ids = new int[configDistPerWhse];
        for (int d_id = 0; d_id < configDistPerWhse; ++d_id) {
            tmp[d_id] = (sg.edu.nus.ownertxn.tpcc.record.NewOrder) sm
                    .readTuple(warehouseId, TABLENAME_NEWORDER);
            if (tmp[d_id] != null) {
                no_o_ids[d_id] = tmp[d_id].no_o_id;
            }
            sm.deleteTuple(warehouseId, TABLENAME_NEWORDER,
                    orderKey(d_id + 1, tmp[d_id].no_o_id));

            Oorder oorder = (Oorder) sm.readTuple(warehouseId,
                    TABLENAME_OPENORDER, orderKey(d_id + 1, tmp[d_id].no_o_id));
            int c_id = oorder.o_c_id;
            oorder.o_carrier_id = carrier;
            sm.updateTuple(warehouseId, TABLENAME_OPENORDER,
                    orderKey(d_id + 1, tmp[d_id].no_o_id), oorder);

            // float ol_total = 0;
            // for (int i = 0; i < oorder.o_ol_cnt; ++i) {
            //     OrderLine orderline = (OrderLine) sm.readTuple(warehouseId,
            //             TABLENAME_ORDERLINE,
            //             orderlineKey(d_id + 1, tmp[d_id].no_o_id, i));
            //     orderline.ol_delivery_d = time.getTime();
            //     ol_total += orderline.ol_amount;
            //     sm.insertTuple(warehouseId, TABLENAME_ORDERLINE,
            //             orderlineKey(d_id + 1, tmp[d_id].no_o_id, i), orderline);
            // }

            // Customer customer = (Customer) sm.readTuple(warehouseId,
            //         TABLENAME_CUSTOMER, customerKey(c_id, d_id + 1));
            // customer.c_balance = customer.c_balance - ol_total;
            // sm.insertTuple(warehouseId, TABLENAME_ORDERLINE,
            //         customerKey(c_id, d_id + 1), customer);
        }

        commitTxn(txnId, runCount);
    }
}