package sg.edu.nus.ownertxn.tpcc.record;

public class OrderLine extends sg.edu.nus.ownertxn.Tuple
{
    public OrderLine() {}

    public OrderLine(String str) {
        String[] strSplit = str.split(",");
        ol_w_id = Integer.valueOf(strSplit[0]);
        ol_d_id = Integer.valueOf(strSplit[1]);
        ol_o_id = Integer.valueOf(strSplit[2]);
        ol_number = Integer.valueOf(strSplit[3]);
        ol_i_id = Integer.valueOf(strSplit[4]);
        ol_supply_w_id = Integer.valueOf(strSplit[5]);
        ol_quantity = Integer.valueOf(strSplit[6]);
        ol_delivery_d = Long.valueOf(strSplit[7]);
        ol_amount = Float.valueOf(strSplit[8]);
    }

    public int ol_w_id;
    public int ol_d_id;
    public int ol_o_id;
    public int ol_number;
    public int ol_i_id;
    public int ol_supply_w_id;
    public int ol_quantity;
    public Long ol_delivery_d;
    public float ol_amount;
    public String ol_dist_info;

    public int size() {
        return 59;
    }

    @Override
    public String toString() {
        return ("ORDERLINE(" + ol_w_id + "," + ol_d_id + "," + ol_o_id + ","
                + ol_number + "," + ol_i_id + "," + ol_supply_w_id + ","
                + ol_quantity + "," + ol_delivery_d + "," + ol_amount + ")");
    }
}