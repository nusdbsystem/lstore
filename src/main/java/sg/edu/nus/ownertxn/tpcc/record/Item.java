package sg.edu.nus.ownertxn.tpcc.record;

public class Item extends sg.edu.nus.ownertxn.Tuple
{
    public Item() {}

    public Item(String str) {
        String[] strSplit = str.split(",");
        i_id = Integer.valueOf(strSplit[0]);
        i_im_id = Integer.valueOf(strSplit[1]);
        i_price = Double.valueOf(strSplit[2]);
    }

    public int i_id; // PRIMARY KEY
    public int i_im_id;
    public double i_price;
    public String i_name;
    public String i_data;

    public int size() {
        return 70;
    }

    @Override
    public String toString() {
        return ("ITEM(" + i_id + "," + i_im_id + "," + i_price + ")");
    }
}