package sg.edu.nus.ownertxn.tpcc.record;

public class Oorder extends sg.edu.nus.ownertxn.Tuple
{
    public Oorder() {}

    public Oorder(String str) {
        String[] strSplit = str.split(",");
        o_id = Integer.valueOf(strSplit[0]);
        o_w_id = Integer.valueOf(strSplit[1]);
        o_d_id = Integer.valueOf(strSplit[2]);
        o_c_id = Integer.valueOf(strSplit[3]);
        o_carrier_id = Integer.valueOf(strSplit[4]);
        o_ol_cnt = Integer.valueOf(strSplit[5]);
        o_all_local = Integer.valueOf(strSplit[6]);
        o_entry_d = Long.valueOf(strSplit[7]);
    }

    public int o_id;
    public int o_w_id;
    public int o_d_id;
    public int o_c_id;
    public Integer o_carrier_id;
    public int o_ol_cnt;
    public int o_all_local;
    public long o_entry_d;

    public int size() {
        return 36;
    }

    @Override
    public String toString() {
        return ("OORDER(" + o_id + "," + o_w_id + "," + o_d_id + "," + o_c_id
                + "," + "o_carrier_id" + "," + o_ol_cnt + "," + o_all_local
                + "," + o_entry_d + ")");
    }
}