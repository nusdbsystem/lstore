package sg.edu.nus.ownertxn.tpcc.txn;

import static sg.edu.nus.ownertxn.TxnUtil.*;
import sg.edu.nus.ownertxn.tpcc.TPCCLoader;
import sg.edu.nus.ownertxn.tpcc.TpccStorageMgr;
import sg.edu.nus.ownertxn.tpcc.TpccTxnExecEngine;
import static sg.edu.nus.ownertxn.tpcc.TPCCConfig.configDistPerWhse;
import static sg.edu.nus.ownertxn.tpcc.TPCCConstants.*;
import static sg.edu.nus.ownertxn.tpcc.TPCCPrimaryKey.*;
import static sg.edu.nus.ownertxn.tpcc.TPCCUtil.*;
import sg.edu.nus.ownertxn.tpcc.record.*;

public class Payment extends AnyTpccTxn
{
    TpccStorageMgr sm;

    private final int warehouseId;
    private final int customerDistrictId;
    private final int customerWarehouseId;
    private final int customerId;
    private final float paymentAmount;

    public Payment(TpccTxnExecEngine eng) {
        super(eng);
        sm = eng.storeMgr;

        warehouseId = randomNumber(sm.startLocalWhseId, sm.endLocalWhseId,
                TPCCLoader.gen);
        customerDistrictId = randomNumber(1, configDistPerWhse, TPCCLoader.gen);

        if (TPCCLoader.gen.nextDouble() > eng.distrPayment) {
            customerWarehouseId = warehouseId;
            customerId = getLocalCustomerID(TPCCLoader.gen);
        }
        else { // for distributed txn
            if (TPCCLoader.gen.nextDouble() < eng.locality) {
                customerWarehouseId = getNeighborRemoteWhseId(warehouseId);
                customerId = getDistrCustomerID(TPCCLoader.gen);
            }
            else { // for distributed txn without locality
                switch (eng.remoteAccessPattern) {
                case TpccTxnExecEngine.REMOTE_RAND_ACCESS:
                    customerWarehouseId = getRandomRemoteWhseId();
                    break;

                case TpccTxnExecEngine.REMOTE_BINEI_ACCESS:
                    customerWarehouseId = getBiNeighborRemoteWhseId(warehouseId);
                    break;

                case TpccTxnExecEngine.REMOTE_NEI_ACCESS:
                    customerWarehouseId = getNeighborRemoteWhseId(warehouseId);
                    break;

                default: // access local warehouse
                    System.err.println("invalid remote access pattern: "
                            + eng.remoteAccessPattern);
                    customerWarehouseId = warehouseId;
                    break;
                }

                customerId = getCustomerID(TPCCLoader.gen);
            }
        }

        paymentAmount = (float) (randomNumber(100, 5000000, TPCCLoader.gen) / 100.0);

        initTxn(txnId);
    }

    public void run() {
        ++runCount;
        boolean gotOwner = false;
        startTxn(txnId);

        Warehouse warehouse = (Warehouse) sm.readTuple(warehouseId,
                TABLENAME_WAREHOUSE, warehouseKey(warehouseId));
        warehouse.w_ytd = warehouse.w_ytd + paymentAmount;
        sm.updateTuple(warehouseId, TABLENAME_WAREHOUSE,
                warehouseKey(warehouseId), warehouse);
        District district = (District) sm.readTuple(warehouseId,
                TABLENAME_DISTRICT, districtKey(customerDistrictId));
        district.d_ytd = district.d_ytd + paymentAmount;
        sm.updateTuple(warehouseId, TABLENAME_DISTRICT,
                districtKey(customerDistrictId), district);

        gotOwner = waitForOwner(
                getKeyWTTableWTparition(TABLENAME_CUSTOMER,
                        customerKey(customerId, customerDistrictId),
                        Long.valueOf(customerWarehouseId)), txnId);
        if (!gotOwner) {
            if (eng.restartAborted) {
                abortAndRestartTxn(txnId, this);
            }
            else {
                abortTxn(txnId);
            }
            return;
        }
        Customer customer = (Customer) sm
                .readTuple(customerWarehouseId, TABLENAME_CUSTOMER,
                        customerKey(customerId, customerDistrictId));

        customer.c_ytd_payment = customer.c_ytd_payment + paymentAmount;
        customer.c_balance = customer.c_balance - paymentAmount;
        sm.updateTuple(customerWarehouseId, TABLENAME_CUSTOMER,
                customerKey(customerId, customerDistrictId), customer);
        History history = new History();
        history.h_c_id = customerId;
        history.h_c_d_id = customerDistrictId;
        history.h_c_w_id = customerWarehouseId;
        history.h_d_id = customerDistrictId;
        history.h_w_id = warehouseId;
        // sm.insertTuple(warehouseId, TABLENAME_HISTORY,
        // historyKey(customerId, customerDistrictId), history);

        // dummy insert
        sm.insertTuple(warehouseId, TABLENAME_HISTORY,
                historyKey(customerId, 1), history);

        commitTxn(txnId, runCount);
    }
}