package sg.edu.nus.ownertxn.tpcc;

import java.text.SimpleDateFormat;

public final class TPCCConfig
{
    public final static String[] nameTokens = { "BAR", "OUGHT", "ABLE", "PRI",
            "PRES", "ESE", "ANTI", "CALLY", "ATION", "EING" };

    public final static SimpleDateFormat dateFormat = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss");

    public final static int configCommitCount = 1000; // commit every n records
    public final static int configItemCount = 100000; // tpc-c std = 100,000
    public final static int configDistPerWhse = 10; // tpc-c std = 10
    public final static int configCustPerDist = 3000; // tpc-c std = 3,000
}
