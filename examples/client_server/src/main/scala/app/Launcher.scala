package app

object Launcher extends qlib.app.LauncherBase {

  override val args = new Arguments

  def getExecutor = {
    if (args.isServer) 
    	Some(new OwnerTxnServer(args.slaveHosts, args.getConf)) 
    else 
    	Some(OwnerTxnClient)
  }
}