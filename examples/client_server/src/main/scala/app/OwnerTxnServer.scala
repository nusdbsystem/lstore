package app

import java.io.{ BufferedReader, InputStreamReader }
import java.net.{ InetAddress, ServerSocket }

import sg.edu.nus.ownertxn.TxnSvr
import sg.edu.nus.ownertxn.StorageMgr.setCustomizedStorageMgr
import sg.edu.nus.ownertxn.tpcc.TpccStorageMgr
import sg.edu.nus.ownertxn.TxnSvrProfiler.{ resetProfiler, getStat }
import sg.edu.nus.ownertxn.tpcc.TpccTxnExecPlugin
import sg.edu.nus.ownertxn.tpcc.txn.NewOrder
import scala.collection.JavaConverters._

import qlib.Logger._

class OwnerTxnServer(slaveHosts: Array[String], conf: Map[String, String])
  extends qlib.app.ExecutionBase {

  private[this] var roundCount = 0

  def run() {
    initOwnerTxnPlugin()

    val sSocket = new ServerSocket(10017)

    while (true) {
      val cSocket = sSocket.accept
      val netIn = new BufferedReader(new InputStreamReader(cSocket.getInputStream))

      val startSignal = netIn.readLine
      require(startSignal.trim.toLowerCase == "start")
      resetProfiler()
      roundCount += 1

      var txnTypeId = netIn.readLine
      import TpccTxnExecPlugin.TxnType._
      while (txnTypeId != null) {
        tpccTxnExecPlugin addTxn (txnTypeId match {
          case "0" => NEW_ORDER
          case "1" => PAYMENT
          case "2" => DELIVERY
          case "3" => ORDER_STATUS
          case "4" => STOCK_LEVEL
          case _ => INVALID
        })
        txnTypeId = netIn.readLine
      }

      netIn.close
      cSocket.close

      outputStat()
    }

    sSocket.close
  }

  private[this] var tpccTxnExecPlugin: TpccTxnExecPlugin = null

  def initOwnerTxnPlugin() {
    val myHostname = InetAddress.getLocalHost.getHostName
    val myId = slaveHosts.indexOf(myHostname)
    require(myId >= 0, s"Invalid host: $myHostname")

    setCustomizedStorageMgr(new TpccStorageMgr(myId, conf.asJava))

    val txnSvr = TxnSvr(slaveHosts, conf)

    tpccTxnExecPlugin = new TpccTxnExecPlugin(txnSvr)
    txnSvr setTxnExecEngine tpccTxnExecPlugin

    txnSvr.run

    while (!tpccTxnExecPlugin.isLoadingDone()) Thread.sleep(100)
  }

  private def outputStat() {
    val stat = getStat

    val numTxnCommit = stat("numTxnCommit").toDouble
    val numNewOrderCommit = NewOrder.getCommitCount
    val numTxnAbort = stat("numTxnAbort").toLong
    val accResponseMillis = stat("accResponseMillis").toLong
    val accLatencyNanos = stat("accLatencyNanos").toLong
    val execSec = stat("execSec").toDouble

    val throughput = numTxnCommit / execSec
    val response = accResponseMillis / numTxnCommit
    val latency = accLatencyNanos / numTxnCommit

    val record = s"[Round $roundCount]" +
      s" thrupt: ${throughput.toInt} txns/s" +
      s", rsp: ${response.toInt} ms" +
      s", latency: ${latency.toLong} ns" +
      s", commit: ${numTxnCommit.toLong} (NewOrder: $numNewOrderCommit)" +
      s", abort: ${numTxnAbort}"

    println("----------------------------------------------------")
    println(record)
    println("----------------------------------------------------")
  }
}