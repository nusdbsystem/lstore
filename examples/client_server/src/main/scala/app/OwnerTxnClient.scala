package app

import java.io.{ PrintWriter, BufferedReader, InputStreamReader }
import java.net.Socket

object OwnerTxnClient extends qlib.app.ExecutionBase {

  def run() {
    val serverHostname = "127.0.0.1"
    val socket = new Socket(serverHostname, 10017)
    val netOut = new PrintWriter(socket.getOutputStream, true)

    netOut.println("start")

    val stdIn = new BufferedReader(new InputStreamReader(System.in))
    print("Txn Type ID: ")

    var txnTypeId = stdIn.readLine
    while (txnTypeId != null) {
      netOut.println(txnTypeId)
      
      print("Txn Type ID: ")
      txnTypeId = stdIn.readLine
    }

    netOut.close
    stdIn.close
    socket.close
  }
}