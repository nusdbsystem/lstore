package app

import java.nio.file.{ Paths, Files }
import scala.{ Option => _ }
import org.kohsuke.args4j.Option
import sg.edu.nus.ownertxn.tpcc.TpccTxnExecEngine.REMOTE_RAND_ACCESS

class Arguments extends qlib.app.ArgumentsBase {

  @Option(name = "-s", aliases = Array("--server"),
    usage = "launch as a server")
  var isServer = false

  @Option(name = "-c", aliases = Array("--conf"), metaVar = "<path>",
    usage = "configuration file of server hosts [required]")
  var confFile = ""

  @Option(name = "--port", metaVar = "<num>",
    usage = "port number [def:5150]")
  var port = 5150

  @Option(name = "-w", aliases = Array("--num-warehouses"), metaVar = "<num>",
    usage = "number of warehouses per partition [def:1]")
  var numWarehouses = 1

  @Option(name = "-distr", metaVar = "<num>",
    usage = "fraction of distributed data (general) [def:0.1]")
  var distr = 0.1

  @Option(name = "--in-situ", metaVar = "<num>",
    usage = "percentage of in-situ data processing (general) [def:0.8]")
  var inSituDataPercent = 0.8

  @Option(name = "-l", aliases = Array("--locality"), metaVar = "<num>",
    usage = "probability of distributed data request with locality [def:0.9]")
  var locality = 0.9

  @Option(name = "-p", aliases = Array("--parallel"), metaVar = "<num>",
    usage = "parallelism of the transaction execution engine [def:1]")
  var parallel = 1

  @Option(name = "--master-port", metaVar = "<num>",
    usage = "master prot number [def:5151]")
  var masterPort = 5151

  @Option(name = "--with-log", usage = "enable transaction logging [def:false]")
  var withLog = false

  @Option(name = "-lp", aliases = Array("--log-penalty"), metaVar = "<num>",
    hidden = true, usage = "logging penalty (ms) to simulate transaction logging [def:2]")
  var logPenaltyMillis = 2;

  @Option(name = "-ap", aliases = Array("--abort-penalty"), metaVar = "<num>",
    hidden = true, usage = "abort penalty (us) to simulate rollback [def:5000]")
  var abortPenaltyUs = 5000

  @Option(name = "-mp", aliases = Array("--migrate-percent"), metaVar = "<num>",
    usage = "migration percentage [def: 0]")
  var migratePercent = 0D

  @Option(name = "--report-period", metaVar = "<num>", hidden = true,
    usage = "period (sec) of reporting the performance [def:1]")
  var reportPeriod = 1

  override def sanityCheckArgs() {
    if (isServer) {
      require(confFile != "", "configuration files must be given")
      require(Files.exists(Paths.get(confFile)), s"file not found: $confFile")
      require(port >= 1024, "port numbers 0 to 1023 are reserved by system")
      require(distr >= 0 && distr <= 1)
      require(0 < inSituDataPercent && inSituDataPercent < 1)
      require(0 <= locality && locality <= 1)
      require(parallel > 0, "parallelism must be positive")
      require(numWarehouses > 0, "number of warehouses must be positive")
      require(0 <= migratePercent && migratePercent <= 1)
      require(reportPeriod > 0)
    }
  }

  var masterHost = ""
  var slaveHosts = Array[String]()

  override def deriveArgs() {
    if (isServer) {
      val in = scala.io.Source.fromFile(confFile, "UTF-8")

      val hosts = in.getLines
        .map { _.trim }
        .filterNot { line => line.startsWith("#") || line.isEmpty }
        .toArray

      in.close()

      require(hosts.size > 2, "there must be as least 1 master and 2 slaves")
      masterHost = hosts.head
      slaveHosts = hosts.tail
    }
  }

  def getConf(): Map[String, String] = {
    Map("masterHost" -> s"${masterHost}",
      "masterPort" -> s"${masterPort}",
      "numPartitions" -> s"${slaveHosts.size}",
      "numWarehouses" -> s"${numWarehouses}",
      "port" -> s"${port}",
      "hz" -> "1",
      "time" -> "0",
      "distr" -> s"${distr}",
      "inSituDataPercent" -> s"${inSituDataPercent}",
      "locality" -> s"${locality}",
      "parallel" -> s"${parallel}",
      "withLog" -> s"${withLog}",
      "logPenaltyMillis" -> s"${logPenaltyMillis}",
      "abortPenaltyUs" -> s"${abortPenaltyUs}",
      "migratePercent" -> "0",
      "remoteAccessPattern" -> s"${REMOTE_RAND_ACCESS}",
      "benchmark" -> "",
      "reportPeriod" -> s"${reportPeriod}")
  }
}