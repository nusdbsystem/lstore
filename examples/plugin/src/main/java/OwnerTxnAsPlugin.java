import java.util.HashMap;
import java.util.Map;

import qlib.RichRandom;
import sg.edu.nus.ownertxn.TxnSvr;
import static sg.edu.nus.ownertxn.StorageMgr.setCustomizedStorageMgr;
import sg.edu.nus.ownertxn.tpcc.TpccStorageMgr;
import sg.edu.nus.ownertxn.tpcc.TpccTxnExecPlugin;
import static sg.edu.nus.ownertxn.tpcc.TpccTxnExecEngine.REMOTE_RAND_ACCESS;

public class OwnerTxnAsPlugin
{
    public static TpccTxnExecPlugin tpccTxnExecPlugin = null;

    public static void init() throws Exception {
        String[] slaveHosts = { "awan-1-44-0.awan.comp.nus.edu.sg",
                "awan-1-45-0.awan.comp.nus.edu.sg",
                "awan-2-44-0.awan.comp.nus.edu.sg",
                "awan-2-45-0.awan.comp.nus.edu.sg" };
        String hostname = java.net.InetAddress.getLocalHost().getHostName();
        int myId = java.util.Arrays.asList(slaveHosts).indexOf(hostname);
        assert (myId >= 0);

        Map<String, String> conf = new HashMap<String, String>();
        conf.put("masterHost", "awan-0-47-0.awan.comp.nus.edu.sg");
        conf.put("masterPort", "5151");
        conf.put("numPartitions", String.valueOf(slaveHosts.length));
        conf.put("numWarehouses", "3");
        conf.put("port", "5150");
        conf.put("hz", "1");
        conf.put("time", "30");
        conf.put("distr", "0.1");
        conf.put("parallel", "1");
        conf.put("withLog", "false");
        conf.put("logPenaltyMillis", "2");
        conf.put("abortPenaltyUs", "5000");
        conf.put("migratePercent", "0");
        conf.put("remoteAccessPattern", String.valueOf(REMOTE_RAND_ACCESS));
        conf.put("benchmark", "");
        conf.put("reportPeriod", "1");

        setCustomizedStorageMgr(new TpccStorageMgr(myId, conf));

        TxnSvr txnSvr = new TxnSvr(slaveHosts, conf);

        tpccTxnExecPlugin = new TpccTxnExecPlugin(txnSvr);
        txnSvr.setTxnExecEngine(tpccTxnExecPlugin);

        txnSvr.run();

        while (!tpccTxnExecPlugin.isLoadingDone()) {
            Thread.sleep(100);
        }
        Thread.sleep(1000 * (slaveHosts.length - 1));
    }

    public static void run() {
        RichRandom rand = new RichRandom();

        for (int i = 0; i < 10; ++i) {
            tpccTxnExecPlugin.addTxn(rand.nextInt(2));
        }
    }

    public static void main(String[] args) throws Exception {
        init();
        run();
    }
}
