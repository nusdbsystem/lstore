import java.util.Map;

import sg.edu.nus.ownertxn.StorageMgr;
import sg.edu.nus.ownertxn.Tuple;

public class DummyStorageMgr extends StorageMgr
{
    public DummyStorageMgr(int myId, Map<String, String> conf) {
        super(myId, conf);
    }

    public int getPartitionId(Long key) {
        return 0;
    }

    public boolean hasTuple(Long key) {
        return data.get(0).get("TABLENAME").containsKey(0);
    }

    public void insertTuple(Long key, Tuple tuple) {
        data.get(0).get("TABLENAME").put(0, tuple);
    }

    public void deleteTuple(Long key) {
        data.get(0).get("TABLENAME").remove(0);
    }

    public Tuple readTuple(Long key) {
        return data.get(0).get("TABLENAME").get(0);
    }

    public void updateTuple(Long key, Tuple tuple) {
        data.get(0).get("TABLENAME").put(0, tuple);
    }

}