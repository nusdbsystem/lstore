import AssemblyKeys._

assemblySettings

name := "odtp-plugin"

version := "0.1"

scalaVersion := "2.11.6"

libraryDependencies ++= Seq()

scalacOptions := Seq("-deprecation", "-feature")

javacOptions := Seq("-Xlint:unchecked")
