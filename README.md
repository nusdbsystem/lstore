# Ownership-based Distributed Transaction Processing

We design and build a prototype of ownership-based distributed transaction 
processing (ODTP) system. 

## Building ODTP

Generate the excutable jar by using

    sbt assembly

## Running ODTP

Run the excutable jar as regular usage. For example: 

    java -jar odtp-assembly-0.1.jar -h

This will print usage help as following. 

    OPTIONS: 
     -g (--txn-gen)          : run this as a transaction generator
     -h (-?, --help)         : print this help message
     -c (--conf) <path>      : configuration file of server hosts [required]
     -port (--port-no) <num> : port number [def:5150]

To start a transaction server, run the following command: 

    java -jar odtp-assembly-0.1.jar -c slaves

where `slaves` lists the hostnames of transaction servers. Please refer to 
`conf/slaves.example` for example. 

To start a transaction generator, run the following command: 

    java -jar odtp-assembly-0.1.jar -c conf/slaves -g

Note that the port number used by the transaction generator is one-increment 
of the stated port number used by the transaction servers. 
